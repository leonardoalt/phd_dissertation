%\section{Proof-Sensitive Interpolation}
\section{Labeling Functions for LIS}
\label{Sec:PSI}
%\subsection{Fixed vs Flexible interpolation systems}
\label{subsec:old}

This section studies the algorithms based on the labeled interpolation system
in (i) an experimental and (ii) an analytic setting.  Our main objective is to
provide motivation and a basis for developing and understanding labeling
functions that construct interpolants having desirable properties.
%
In particular, we will concentrate on three syntactic properties of the
interpolants: the number of distinct variables; the number of literal
occurrences; and the internal size of the interpolant.
%
In most of the discussion in this section we will ignore the two classical
optimizations on structural sharing and constraint simplification.
While both are critically important for practicality of interpolation,
our experimentation shows that they mostly have similar effect on all
the interpolation algorithms we studied, and therefore they can be
considered orthogonally (see Sec.~\ref{Sec:EffectsOfSimplification}).
%
The exception is that {\em the non-uniform labeling functions allow a more
efficient optimization compared to the uniform labeling functions} through
constraint simplification. We recall from Def.~\ref{def:uniform} that a uniform
labeling function consistently leads all the occurrences of a variable in a
formula with the same label.
%

\subsection{Analysing Labeling Functions Experimentally}
We start this section by presenting an experimental work that motivated
the following detailed theoretical analysis on labeling functions.
We recall Example~\ref{ex:1} from Chapter~\ref{section:preliminaries_bool},
where we prove the unsatisfiability of the formula $\phi$:
\begin{equation}
\phi = (x_1 \lor x_2) \wedge (\neg x_2 \lor x_4) \wedge (\neg x_2
\lor \neg x_3 \lor \neg x_4) \wedge (x_1 \lor x_3) \wedge (\neg x_1).
\end{equation}
Please see Fig.~\ref{Fig:proofitpp1} for the refutation proof.
If we partition the formula $\phi$ such that
$A = (x_1 \lor x_2)$ and $B =  (\neg x_2 \lor x_4) \wedge (\neg x_2
\lor \neg x_3 \lor \neg x_4) \wedge (x_1 \lor x_3) \wedge (\neg x_1)$,
and interpolate using \McM and \McP, we have that the generated
interpolants are, respectively, $Itp_{\McM} = x_1 \lor x_2$ and
$Itp_{\McP} = (((\neg x_1 \wedge x_2) \lor x_1) \wedge x_2) \lor x_1$.
If we change the partitioning such that $A' = B$ and $B' = A$, and 
interpolate over $A'$ using the same refutation proof, we have that
$Itp'_{\McM} = (((x_1 \lor \neg x_2) \wedge \neg x_1) \lor \neg x_2) \wedge \neg x_1$
and $Itp'_{\McP} = \neg(x_1 \lor x_2)$.
It is straightforward to see that neither \McM or \McP was able to generate
small interpolants after a simple change of partitioning.

Generating small interpolants is not a trivial task. Attempts on it include, for
example, proof compression and interpolant simplification, heavy procedures
that may lead to large overheads. 
%
Crafting labeling functions from LIS is a light way to generate interpolants
with a specific property, as suggested by~\cite{DSilva2010} in the
interpolation system $D_{min}$.
%
In our research investigation regarding the problem of interpolants not being
effective for program verification, we experimented with $\Dmin$ and the
abilities of LIS.
%
\Dmin labels the literals by copying its class ($a$ if it is in a clause from A
or $b$ otherwise) and is proven to generate interpolants with the least number of
distinct variables.

Table~\ref{table:dmin} shows results of our preliminary experiments with the
tool \funfrog using the interpolation algorithms \McM , \Pud, \McP and \Dmin.
\funfrog was used to verify safety properties in C programs that contain a
complex function call tree structure, with assertions on various levels of the
tree. Since \funfrog uses interpolation to create, store and reuse summaries
for the program functions, the choice of interpolation algorithm may impact the
number of refinements used in the process of verifying a certain property,
which finally impacts the overall verification performance.
%
Row \emph{Time(s)} shows the total verification time that \funfrog spent
to verify all the benchmarks using each labeling function, where the following
row represents how much worse \funfrog was using each labeling function
compared to the best of them (0).  The row \emph{Avg size} represents the
average number of Boolean connectives in the interpolants generated using each
of the labeling functions, where the following row shows how much larger were
the interpolants generated using each labeling function compared to the one
that led to the smallest average (0).
%
Surprisingly, \Dmin behaved poorly and led to the worst verification time in
\funfrog.

\begin{table}[b]
\centering
\begin{tabular}{lrrrrrrr}
\toprule
\multicolumn{5}{c}{\system{FunFrog}} \\
& $\McM$ & $\Pud$ & $\McP$ & \Dmin \\
\cmidrule{1-1} \cmidrule(l){2-5}
\textbf{Time (s)} & 2333& 3047 & 3207 & 3811 \\
\textbf{increase \%} & 0 & 23 & 27 & 38 \\
\cmidrule{1-1} \cmidrule(l){2-5}
\textbf{Avg size} & 48101& 79089& 86831 & 119306 \\
\textbf{increase \%} & 0 & 39 & 44 & 59 \\
\bottomrule
\end{tabular}
\caption{\funfrog experiments results using previous interpolation systems}
\label{table:dmin}
\end{table}

Based on this preliminary evidence, we conducted a thorough experimental
analysis on the properties of labeling functions \McM, \Pud and \McP.  We used
the three labeling functions as the interpolation algorithm in two
interpolation-based applications: (i) \funfrog, an incremental model checker,
and (ii) \evolcheck, an upgrade checker. Both approaches use Craig interpolants
to compute function summaries in the same manner as presented in
Chapter~\ref{subsec:fsum}, such that they are stored and reused.
%
For \evolcheck, only \McM and \Pud can be used, since \McP does not guarantee
soundness of tree interpolants~\cite{Rollini2012}, a necessary property for
upgrade checking.
%
We used the
model checkers to verify a set of C benchmarks characterized by a non-trivial
call tree structure. These benchmarks contain assertions in
different levels of the call-tree, which makes them particularly suitable
for summarization-based verification.
%
Tables~\ref{table:periplo_ff} and~\ref{table:periplo_ev} show, respectively,
the performance results for \funfrog and \evolcheck, where \emph{\#Refinements}
represents the number of function summaries that had to be discarded and
recomputed, Avg$|I|$ is the average number of Boolean connectives in
the interpolants, and \emph{Time(s)} is the total time spent to verify all the
benchmarks.

\begin{table}
\centering
\caption{Performance results of \funfrog when using various labeling functions.}
\label{table:periplo_ff}
\begin{tabular}{|l|c|c|c|}
\hline
 & \McM & \Pud & \McP\\
 \hline
 \#Refinements & 290 & 298 & 308\\
  \hline
 Avg $|I|$ & 38886.62 & 39372.07 & 72994.08\\
  \hline
 Time(s) & 4568.08 & 4929.93 & 6805.81\\
 \hline
\end{tabular}
\end{table}

\begin{table}
\centering
\caption{Performance results of \evolcheck when using various labeling functions.}
\label{table:periplo_ev}
\begin{tabular}{|l|c|c|}
\hline
 & \McM & \Pud \\
 \hline
 \#Refinements & 65 & 63 \\
  \hline
 Avg $|I|$ & 334554.64 & 377903.11 \\
  \hline
 Time(s) & 4322.57 & 4402.00 \\
 \hline
\end{tabular}
\end{table}

From Tables~\ref{table:periplo_ff} and~\ref{table:periplo_ev} we can observe
the following. The strength of the interpolants have an impact on the
convergence of the model checker. We can see that stronger interpolants lead to
a smaller number of refinements for \funfrog, and weaker interpolants lead to a
smaller number of refinements for \evolcheck. The other information we can
extract from the experiments is that interpolants with a small number of
Boolean connectives lead to a smaller verification time, as we can see from
both tools.  However, it is not clear how different labeling functions behave
with respect to the size of the interpolants they generate.

The next section
presents a theoretical analysis on the properties of labeling functions and how
to derive small interpolants from LIS and refutation proofs simply by using a
different labeling function.


\subsection{Analysing Labeling Functions Theoretically}
An interesting special case in LIS-based interpolation algorithms is when
the labeling can be used to reduce the number of distinct variables in
the final interpolant.  To make this explicit we define the concepts of
a $p$-pure resolution step and a $p$-annihilable interpolation instance.
\begin{definition}
\label{Definition:PAnnihilable}
Given an interpolation instance $(R, A, B)$, a variable $p \in var(A) 
\cup var(B)$ and a labeling function $L$,
a resolution step in $R$ is {\em $p$-pure} if at most one of the
antecedents contain $p$, or both antecedents $C, D$ contain $p$ but
$L(p,C) = L(p,D) = a$ or $L(p,C) = L(p,D) = b$.
%
An interpolation instance $(R, A, B)$ is {\em $p$-annihilable} if there
is a non-uniform labeling function $L$ such that $L(p,C) = a$ if $C \in
A$, $L(p, C) = b$ if $C \in B$, and all the resolution steps are
$p$-pure.
\end{definition}

The following theorem shows the value of $p$-annihilable
interpolation instances in constructing small interpolants.
\begin{theorem}
\label{Theorem:EqVariables}
Let $(R, A, B)$ be an interpolation instance, $p \in \var(A) \cap
\var(B)$, and $I$ an interpolant obtained from $(R, A, B)$ by means of a
LIS-based algorithm.  If $p \not\in \var(I)$, then $(R, A, B)$ is
$p$-annihilable.
\end{theorem}
\begin{proof}
Assume that $(R, A, B)$ is not $p$-annihilable, $p \in
\var(A)\cap\var(B)$, but there is a labeling $L$ which results in a
LIS-based interpolation algorithm that constructs an interpolant not
containing $p$.
%
The labeling function cannot have $L(p,C) = b$ if $C \in A$ or $L(p,C) =
a$ if $C \in B$ because $p$ would appear in the partial interpolants
associated with the sources by Eq.~\eqref{Eq:SourceItps}.
%
No clause $C$ in $R$ can have $L(p,C) = ab$ since all literals in the
refutation need to be used as a pivot on the path to the empty clause, and
having an occurrence of $p$ labeled $ab$ in an antecedent clause would
result in introducing the literal $p$ to the partial interpolant
associated with the resolvent by Eq.~\eqref{Eq:ResolventItps} when used
as a pivot.
%
Every resolution step in the refutation $R$ needs to be $p$-pure, since if
the antecedents contain occurrences $(p,C)$ and $(p,D)$ such that
$L(p,C) \not= L(p,D)$ either the label of the occurrence of $p$ in the
resolvent clause will be $ab$, violating the condition that no clause
can have $L(p,C) = ab$ above, or, if $p$ is pivot on the resolution step,
the variable is immediately inserted to the partial interpolant by
Eq.~\eqref{Eq:ResolventItps}.
\end{proof}

While it is relatively easy to artificially construct an interpolation
instance that is $p$-annihilable, they seem to be rare in
practice (see Section~\ref{Sec:EffectsOfSimplification}).
Hence, while instances that are $p$-annihilable would result
in small interpolants, it has little practical significance at least in
the benchmarks available to us.
%
However, we have the following practically useful result which shows the
benefits of labeling functions producing $p$-pure resolution steps in
computing interpolants with low number of connectives.

\begin{theorem}
\label{Theorem:SmallInterpolant}
%
Let $(R,A,B)$ be an interpolation instance. Given a
labeling function $L$ such that the resolution steps in $R$ are $p$-pure
for all $p \in \var(A\land B)$, and a labeling function $L'$ such that at
least one resolution step in $R$ is not $p$-pure for some $p \in
\var(A \land B)$, we have $\IntSize{\Itp{L}} \le \IntSize{\Itp{L'}}$.
\end{theorem}
\begin{proof}
For a given refutation $R$, the number of partial interpolants will be
the same for any LIS-based interpolation algorithm.
%
By Eq.~\eqref{Eq:ResolventItps} each resolution step will introduce one
connective if both occurrences in the antecedents are labeled $a$ or $b$
and three connectives otherwise.  The latter can only occur if the
labeling algorithm results in a resolution step that is not $p$-pure
for some $p$.
\end{proof}
Clearly, $p$-pure steps are guaranteed with uniform labeling functions.
Therefore we have the following corollary:
\begin{corollary}
\label{Corollary:SmallInterpolant2}
Uniform labeling functions result in interpolants with smaller internal
size compared to non-uniform labeling functions.
\end{corollary}

\subsection{Proof-Sensitive Interpolation}
The main result of this chapter is the development of a labeling function
that is uniform, therefore producing small interpolants by
Corollary~\ref{Corollary:SmallInterpolant2}, and results in the smallest
number of variable occurrences among all uniform labeling functions.
%
This {\em proof-sensitive labeling function} works by considering the
refutation $R$ when assigning labels to the occurrences of the shared
variables.

\begin{definition}
\label{Definition:PS}
Let $R$ be a resolution refutation for $A \wedge B$ where $A$ and $B$
consist of the source clauses, $f_A(p) = |\{(p, C) \mid C \in A\}|$ be the
number of times the variable $p$ occurs in $A$, and $f_B(p) = |\{(p, C)
\mid C \in B\}|$ the number the variable $p$ occurs in $B$.
%\begin{definition}
%\label{def:1}
The proof-sensitive labeling function $L_{\PS}$ is defined as
\begin{equation}
\label{Eq:LabelingPS}
L_{\PS}(p,C) =
\begin{cases}
  a & \textrm{if } f_A(p) \ge f_B(p) \\
  b & \textrm{if } f_A(p) < f_B(p).
\end{cases}
\end{equation}
\end{definition}
%
Note that since $L_{\PS}$ is uniform, it is independent of the clause
$C$.  
%GF: equation~\eqref{Eq:FreqSets} was defined in the scope of the proof
% now it is moved upper since it is also used in~\eqref{Eq:PSw} and~\eqref{Eq:PSs}
Let $\Sh_A$ be the set of
the shared variables occurring at least as often in clauses of $A$ as in
$B$ and $\Sh_B$ the set of shared variables occurring more often in $B$ than in $A$:
%
\begin{equation}
\label{Eq:FreqSets}
\begin{array}{ll}
    \Sh_A = \{p \in \var(A) \cap \var(B) \mid f_A(p) \ge f_B(p)\} &
    \textrm{ and } \\
    \Sh_B = \{p \in \var(A) \cap \var(B) \mid f_A(p) < f_B(p)\}
\end{array}
\end{equation}
%
Theorem~\ref{Theorem:PS} states the optimality with respect to
variable occurrences of the algorithm $\Itpalg{\PS}$ among uniform labeling
functions.

%\end{definition}
%
\begin{theorem}
\label{Theorem:PS}
For a fixed interpolation instance $(R,A,B)$,
%
the interpolation algorithm $\Itpalg{\PS}$ will introduce the smallest
number of variable occurrences in the partial interpolants associated
with the source clauses of $R$ among all uniform interpolation
algorithms.
\end{theorem}

\begin{proof}
%
The interpolation algorithm $\Itpalg{\PS}$ is a uniform algorithm
labeling shared variables either as $a$ or $b$.  Hence, the shared
variables labeled $a$ will appear in the partial interpolants of the
source clauses from $B$ of $R$, and the shared variables labeled $b$
will appear in the partial interpolants of the source clauses from $A$
of $R$.
%
The sum of the number of variable occurrences in the partial interpolants associated
with the source clauses by $\Itpalg{\PS}$ is
\begin{equation}
%\label{Eq:NumOccsInPS}
\nonumber
n_{\PS} = \sum_{v \in Sh_B}f_A(v) + \sum_{v \in Sh_A}f_B(v).
\end{equation}

We will show that swapping uniformly the label of any of the shared
variables will result in an increase in the number of variable
occurrences in the partial interpolants associated with the source
clauses of $R$ compared to $n_{\PS}$.
%
Let $v$ be a variable in $\Sh_A$.  By~\eqref{Eq:LabelingPS}
and~\eqref{Eq:FreqSets}, the label of $v$ in $\PS$ will be $a$.
Switching the label to $b$ results in the size
$
n' = n_{\PS} - f_B(v) + f_A(v).
$
Since $v$ was in $\Sh_A$ we know that $f_A(v) \ge f_B(v)$
by~\eqref{Eq:FreqSets}, and therefore $-f_B(v) + f_A(v) \ge 0$ and $n'
\ge n_{\PS}$.
%
An (almost) symmetrical argument shows that swapping the label for a
variable $v \in \Sh_B$ to $a$ results in $n' > n_{\PS}$.
\iffalse
Now assume $v \in \Sh_B$.  The label of $v$ in $\PS$ will be $b$.  When
the label is switched to $a$, the resulting source occurrences will be
$$
n' = n_{\PS} - f_A(v) + f_B(v).
$$
Again since $v$ is in $\Sh_B$ we know that $f_A(v) < f_B(v)$, hence
$0 < -f_A(v) + f_B(v)$ and $n' > n_{\PS}$.
%
\fi
Hence, swapping uniformly the labeling of $\PS$ for any shared variable
will result in an interpolant having at least as many variable
occurrences in the leaves.  Assuming no simplifications, the result holds
for the final interpolant.
\end{proof}

\begin{figure}[tb]
\centering
\includegraphics[width=\linewidth]{figures/proofitpp3}
\caption{Interpolants obtained by $\Itpalg{\PS}$.}
\label{Fig:proofitpp3}
\end{figure}

\begin{example}
\label{ex:3}
Fig.~\ref{Fig:proofitpp3} shows the interpolants that $\Itpalg{\PS}$
would deliver if applied to the same refutation $R$ of $\phi$ and
partitionings $A\land B$ and $A' \land B'$ given in Example~\ref{ex:1}.
Notice that $\Itpalg{\PS}$ adapts the labeling to the best one depending
on the refutation and partitions, and gives small interpolants for both
cases.
\end{example}

From Theorems~\ref{Theorem:EqVariables}, ~\ref{Theorem:SmallInterpolant}
and~\ref{Theorem:PS} we immediately have the following:
\begin{corollary}
\label{Corollary:SmallInterpolant}
For not $p$-annihilable interpolation instances, for any variable $p$, the
proof-sensitive labeling function will result in interpolants that have the
smallest internal size, the least number of distinct variables compared to any
other labeling function from LIS, since only $p$-annihilable interpolation
instances are capable of variable elimination, and least variable occurrences
in the source partial interpolants.
\end{corollary}

Because of the way $L_{\PS}$ labels the variable occurrences, we cannot
beforehand determine the strength of $\Itpalg{\PS}$ relative to, e.g., the
algorithms $\Itpalg{\McM}, \Itpalg{\Pud},$ and $\Itpalg{\McP}$.
Although it is often not necessary that interpolants have a particular
strength, in some applications this has an impact on performance or even
soundness~\cite{Rollini2013}.
%
To be able to apply the idea in applications requiring specific interpolant
strength, for example tree interpolation,
%for example sequence interpolants~\cite{VizelG:FMCAD09}, 
a weak and a strong version of the proof-sensitive interpolation algorithm are
also introduced,
$\Itpalg{\PS_w}$ and $\Itpalg{\PS_s}$.  The corresponding labeling
functions $L_{\PS_w}$ and $L_{\PS_s}$ are defined as
%
\begin{equation}
\label{Eq:PSw}
L_{\PS_w}(p,C) = \begin{cases}
    a & \textrm{if $p$ is not shared and $C \in A$ or $p \in Sh_A$}\\
    b & \textrm{if $p$ is not shared and $C \in B$}\\
    ab & \textrm{if $p \in Sh_B$}
\end{cases}
\end{equation}
%
\begin{equation}
\label{Eq:PSs}
L_{\PS_s}(p,C) =
\begin{cases}
    a & \textrm{if $p$ is not shared and $C \in A$}\\
    b & \textrm{if $p$ is not shared and $C \in B$, or $p \in \Sh_B$}\\
    ab & \textrm{if $p \in \Sh_A$}
\end{cases}
\end{equation}

Finally, it is fairly straightforward to see based on the definition of
the labeling functions that the strength of the interpolants is
partially ordered as shown in the diagram below.
%
%\begin{figure}[tb]
\begin{center}
  \includegraphics[scale=0.7]{figures/hierarchy-labeling}
  \vspace{-0.5cm}
\end{center}
%\end{figure}

%The novel interpolation algorithms $\Itpalg{\PS}$, $\Itpalg{\PS_w}$ and $\Itpalg{\PS_s}$
%are compared experimentally with \McM, \Pud and \McP in Chapter~\ref{chapter:experiments},
%and are shown to provide consistent improvement to model checkers.

