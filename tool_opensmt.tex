\section{OpenSMT2}
\label{section:opensmt2}

We implemented the previous and novel interpolation algorithms for
propositional logic from Chapter~\ref{chapter:interpolation_bool}, the
\euf-interpolation system from Chapter~\ref{chapter:euf_interpolation} and the
\lra-interpolation system from Chapter~\ref{chapter:lra_interpolation} in
\opensmt. This section describes the architecture and implementation details of
\opensmt which can be found at \url{http://verify.inf.usi.ch/opensmt}.
\opensmt was used in the evaluation experiments for
Chapters~\ref{chapter:interpolation_bool}, ~\ref{chapter:euf_interpolation}
and~\ref{chapter:lra_interpolation}, and the results can be found in those
chapters.

\subsection{Basic functionalities}

\opensmt is build on the foundations of the previous \osmt generations, while
providing more efficient data structures and native support to \smtlib version
2.  Besides, \opensmt offers a wide range of new features, such as parallel
solving, incremental solving, interpolation for different theories, and SAT
proof compression.  Fig.~\ref{figure:opensmt_high} shows a high level overview
of the architecture of \opensmt.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{figures/opensmt_overview}
\caption{Overview of the architecture of \opensmt.}
\label{figure:opensmt_high}
\end{figure}

\opensmt supports receiving a problem either from an \smtlib file or from an
interface as a library. The problem is transformed into an SMT formula $\phi$
which is the simplified into a formula $\phi_s$, using simplifications from
both the Boolean and theory levels.  The simplified formula $\phi_s$ is then
converted into CNF resulting in the formula $\phi_{CNF}$. The SAT solver then
receives $\phi_{CNF}$ and starts the search. When there is a satisfying
assignment found by the SAT solver, the corresponding theory solver has to
certify that this is indeed a satisfying assignment. In case this assignment is
actually unsatisfiable, the theory solver returns a new clause $c$ and the
problem is updated to $\phi_{CNF} := \phi_{CNF} \wedge c$. The search terminates
when either the SAT solver says that $\phi_{CNF}$ is unsatisfiable or the
theory solver accepts an assignment as SAT.

\subsection{Modularity}

\opensmt is an open source SMT solver written in C++ that has as one of its
goals modularity and easy approachability by newcomers. 
In an ideal case, one would be able to create a solver for a new theory
without even touching the existing code, only by plugging in new files.

A theory $\tau$ in \opensmt consists of three elements: a logic $L_\tau$ that
represents the language of $\tau$; a solver $S_\tau$ containing the solving
algorithms; and a theory $T_\tau$ that connects the logic and the solver. There
are four abstract classes that have to be derived when implementing a new
solver: Theory, Logic, TSolver and TSolverHandler.  Table~\ref{table:opensmt}
describes the necessary methods that have to be implemented for each class when
supporting a new theory.

\begin{table}[tb]
\caption{Abstract methods that must be overridden to implement new theories.}
\label{table:opensmt}
\footnotesize
\begin{tabular}{p{.25\textwidth}p{.75\textwidth}}
\toprule
{\bf Method} & {\bf Description} \\
\cmidrule(r){1-1} \cmidrule{2-2}
\multicolumn{2}{c}{\bf Theory }\\
\func{simplify} & Entry point for theory specific
simplifications. \\
\cmidrule(r){1-1} \cmidrule{2-2}
\multicolumn{2}{c}{\bf Logic }\\
\func{mkConst} & Create logic-specific constants.\\
\func{isUFEquality} & Check whether a given equality is uninterpreted.\\
\func{isTheoryEquality} & Check whether a given equality is from a
theory. \\
\func{insertTerm} & Insert a theory term. \\
\func{retrieveSubstitutions} & Get the substitutions based on the logic.
\\
\cmidrule(r){1-1} \cmidrule{2-2}
\multicolumn{2}{c}{\bf TSolverHandler }\\
\func{assertLit\_special} & Assert literals in the simplification
phase. \\
\cmidrule(r){1-1} \cmidrule{2-2}
\multicolumn{2}{c}{\bf TSolver }\\
\func{assertLit} & Assert a theory literal. \\
\func{pushBacktrackPoint}, \mbox{\func{popBacktrackPoint}} &
Incrementally add and remove asserted theory literals. \\
\func{check} & Check theory consistency of the asserted literals. \\
\func{getValue} & obtain a value of a theory term once a model has
been found. \\
\func{computeModel} & compute a concrete model for the theory terms
once the theory solver finds a model consistent.  \\
\func{getConflict} & return a compact explanation of the
theory-inconsistency in the form of theory literals. \\
\func{getDeduction} & get theory literals implied under
the current assignment. \\
\func{declareTerm} & inform the theory solvers about a theory literal.\\
\bottomrule
\end{tabular}
\end{table}

\subsection{Interpolation Modules}
Fig.~\ref{figure:opensmt} shows the overall architecture of \opensmt from a
new perspective, highlighting the interpolation modules.
%
When the given problem is UNSAT, it is possible to retrieve interpolants.
%
The application must tell \opensmt what should be the partitioning $(A,B)$
of the problem such that the interpolant is an over-approximation of $A$.
%
If this information is not given \opensmt assumes the first asserted formula
as $A$ and the rest as $B$.

It is also possible for the application to make extra requirements to the
interpolation module.The application can request proof reduction and the
interpolation algorithm for each theory.

The proof reduction module implements the following proof reduction techniques:
(i) RecyclePivotsWithIntersection (RPI) from~\cite{Bar-Ilan2009,Fontaine2011};
(ii) LowerUnits (LU) from~\cite{Fontaine2011}; (iii) a structural hashing
approach (SH) similar to~\cite{Cotton2010}; (iv) and the local rewritting rules
from~\cite{Rollini2011,Rollini2014,BruttomessoRST10}.

The LIS framework~\cite{DSilva2010} is implemented for propositional logic,
with the built-in interpolation algorithms from the literature
\McM~\cite{McMillan2005}, $P$ \cite{Pudlak97}, \McP \cite{DSilva2010}, and
the novel interpolation algorithms presented in
Chapter~\ref{chapter:interpolation_bool} \PS, \PSw and \PSs.  If no interpolation
algorithm is chosen, \McM is used. The application can also provide its own
labeling function to be used by LIS.

The EUF-interpolation system from Chapter~\ref{chapter:euf_interpolation} is
implemented for EUF, together with the build-in interpolation algorithms
\Itps, \Itpw and \Itpr from Chapter~\ref{chapter:euf_interpolation}, where
\Itps is equivalent to the algorithm from~\cite{Fuchs2009}. If no
interpolation algorithm is chosen, \Itps is used.  The application can also
provide its own labeling function for the congruence graph.

The LRA-interpolation system from Chapter~\ref{chapter:lra_interpolation} is
implemented for LRA, and accepts the normalized strength factors from
Chapter~\ref{chapter:lra_interpolation}, where 0 is equivalent to the algorithm
from~\cite{McMillan2005}. If no strength factor is given, 0 is chosen.

After this information is given, \opensmt starts by explicitly building the SAT
proof of unsatisfiability of the given problem. The Boolean interpolation
module might need to collect statistics from the proof before the interpolation
process starts, in case the Proof-Sensitive interpolation algorithms are used.
After applying a topological sorting, the graph is traversed bottom up applying
the interpolation rules from LIS using the specified propositional
interpolation algorithm to annotate each node with its partial interpolant.
For the theory leaves, the specific theory interpolation system is used with
the specified labeling function (for EUF) or strength factor (for LRA).
%
When the interpolant is constructed it is finally returned to the application.

\begin{figure}[tb]
\centering
\includegraphics[scale=0.5]{figures/opensmt}
\caption{Overall verification/interpolation framework.}
\label{figure:opensmt}
\end{figure}

