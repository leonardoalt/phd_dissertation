Reviewer: Philipp Ruemmer

p32/18: what is described here are not tree interpolants; mention why the tree
interpolation property is not important here?
Added: explanation on tree interpolation

p37/23: labeling has to be followed theory interpolation to preserve soundness
(last line). Explain in more detail
Added: explanation on the strength agreement between propositional
and theory interpolation algorithms

p56/42: "M_w does not satisfy a necessary property for update checking." give
more details
Added: Reason why M_w does not satisfy tree interpolation

p60/46, Cor 4.1: formulated ambiguously, clarify. p is not bound/defined.
Explain why least number of distinct variables is achieved. Smallest compared
to what?
Added: Fixed Corollary 4.1.

p62/48: "only interpolation algorithms that are at least as strong as P have
tree interpolation" can you elaborate; does this also apply to sequence
interpolants?
Added: Explanation on the question

p86/72: It is a bit unclear how the definitions for duality-based EUF
interpolation are applied in a concrete case. Add an example for this, maybe
extending one of the examples that are already there
Added: Extension of example

p94/80: explain in more detail why PS + X does not provide the smallest
interpolants
Added: Explanation on the strength agreement between propositional and theory
interpolation algorothims

p95/81: explain in more detail why the combination of strong and weak
interpolation systems leads to sub-optimal results ("strength hypothesis")
Added: Same case as the previous question

p97/83: why no comparison of runtime for the different interpolation methods?
this should at least be explained in text
Added: Table showing this comparison

p111/97: give a better explanation of the difference between "+ Spurious
Overhead" and "+ Full Overhead", the current text is unclear
Added: More text containing this exaplanation.

Template chapter:
This chapter should be a sub-section of the EUF chapter. Theorem 5 should not
be a "theorem", maybe explicitly call it "theorem template", or something
similar?
Added: Changed "Theorem" to "Theorem template", and merged this chapter with
the EUF chapter (in the beginning).

p112/98: why no comparison of different LRA interpolants (different strength)
at this point?
Added: That is shown in Table 7.2

p122/108: would the experience with stronger/weaker interpolants for
FunFrog/eVolCheck carry over to EUF/LRA interpolants?
Added: short discussion on the topic

* there should be at least a short section on planned future work
Added: short section on future work inside Chapter 8: Conclusions.

===============================================================================

Reviewer: Jan Kofron

1) At several places, the references are missing
Added: fixed those

2) I did not like the citing style.
Unfortunately this is the style in USI's template.

3) As to the LRA strength factor, is there any theoretical hypothesis (I mean intuition in addition to
stronger/weaker) as how to set it for a particular application?
Added: Short discussion on the choice of the LRA strength factor.

4) Do you envision other logics that would be useful to implement for software verification inside
OpenSMT2 next to LRA and EUF?
Added: Text in the future work mentions theories that would be nice to work on as future work.

===============================================================================

Reviewer: Fernando Pedone

1) It would be good to clarify the extent to which the choice to focus on LIS
limits the contributions of this chapter.
Added: Discussion on the topic

2) Interpolation System Template:
The main contribution is described in only two pages and there's no summary and
future work. I couldn't find a proof for Theorem 5.
Added: This chapter has been restructured and merged into the EUF chapter, and
Theorem 5 (which is not actually a theorem) has been renamed as a Theorem
Template.

3) The structure of the thesis is unusual. Why Chapters 5 and 6 are not
presented as a single chapter? Chapter 5 is thin and incomplete, but Chapter 6
evoques the framework introduced in Chapter 5.
Add ed: The thesis has been slightly restructured and these two mentioned
chapters have been merged.

===============================================================================

Reviewer: Robert Soule

1) The style used for citations is confusing.
Unfortunately this comes from USI's document class.

2) Chapter 5 seems out-of-place. The main material in Chapter 5 seems to be the
presentation of the template algorithm. I did not understand why this was not
simply a section in Chapter 6.
A dded: These chapters have been merged and now the old Chapter 5 became a
Section of the old Chapter 6.

