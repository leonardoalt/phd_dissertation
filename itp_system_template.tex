\subsection{Duality-based Interpolation and Strength}
\label{section:duality_template}
Let $T$ be the graph proving the unsatisfiability of $A\wedge B$.
The interpolation procedure annotates each node $n$ of $T$ with a \emph{partial
interpolant} $I_n$ such that
\begin{itemize}
\item[(a)] $A \models I_n$;
\item[(b)] $B,I_n \models n$;
\item[(c)] The non-logical symbols in $I_n$ are common to $A$ and $B$.
\end{itemize}
%
A dag-like interpolation algorithm consists of two procedures:
\emph{(i)} $\Itp_L^A$ for computing partial interpolants for leaf nodes;
and \emph{(ii)} $\Itp_C^A$ for computing a partial interpolant for an
inner node as a combination of its children.
Fig.~\ref{figure:small_example} ({\em bottom left}) illustrates this schematically.
%
\iffalse
\begin{figure}[tb]
\centering
\includegraphics[scale=0.5]{figures/generic_itp_xfig}
\caption{A dag with $k$ leaves $n_1,n_2,\ldots,n_k$,
proving $n_{r}$.
%
The procedures $\Itp_L^A$ and $\Itp_C^A$ compute partial interpolants
for leaves and the inner node, respectively.}
\label{figure:generic_itp}
\end{figure}
\fi

\iffalse
such that the partial interpolant is an interpolant for the associated
node in the sense specific to the proof system, and the partial
interpolant of the root node is the requested final interpolant.
%
%Amongst those procedures, are, for example, the
%Labeled Interpolation Systems for propositional logic \cite{DSilva2010},
%interpolation for the theories of Equalities and Uninterpreted Functions and
%Linear Real Arithmetic \cite{McMillan2005}, and interpolation for Nonlinear
%Real Arithmetic \cite{Gao2016}. 
%
Let $A,B \models P$, and $T$ the graph representing the proof of
unsatisfiability.  A valid partial interpolant $I_n$ for a node $n \in
T$ satisfies the following:
\begin{itemize}
\item[(a)] $A \rightarrow I_n$;
\item[(b)] $B,I_n \models n$;
\item[(c)] The non-logical symbols in $I_n$ are common to $A$ and $B$.
\end{itemize}
\fi

The interpolation system template allows constructing several
strength-controlled interpolants through the notion of \emph{duality}.

Let $\Itp^A$ be an interpolant for $A$ computed with a given algorithm.
Its \emph{dual} is the negation of the interpolant $\Itp^B$ computed for
$B$ by the same algorithm.
%
Since $\Itp^A$ is an interpolant, it satisfies the following criteria:
%
\emph{(i)} $A \rightarrow \Itp^A$;
\emph{(ii)} $\Itp^A \rightarrow \neg B$; and
\emph{(iii)} $var(\Itp^A) \subseteq var(A) \cap var(B)$.
%
Similarly, $\Itp^B$ satisfies
%
\emph{(iv)} $B \rightarrow \Itp^B$;
\emph{(v)} $\Itp^B \rightarrow \neg A$; and
\emph{(vi)} $var(\Itp^B) \subseteq var(A) \cap var(B)$.
%
From \emph{(v)} we can see that $A \rightarrow \neg \Itp^B$, and from
\emph{(iv)} that $\neg \Itp^B \rightarrow \neg B$.  Since \emph{(iii)}
and \emph{(vi)} are equal conditions, also $\neg \Itp^B$ is an
interpolant for $A$.

We generalize this idea to partial interpolants by specifying $\Itp_L^B$
and $\Itp_C^B$ for computing the dual partial interpolants for leaf and
non-leaf nodes respectively.
The control over strength of the interpolants then follows if the strength
relation of a partial interpolant and its dual can be established for
both $\Itp_L^A$ and $\Itp_C^A$:

\noindent
\paragraph{\textbf{Theorem template.}}
%\begin{theorem}
\label{theorem:template}
Let $\tau$ be a theory using a proof system that yields a refutation proof that
has a dag-like structure.  Let $\Itp_L^A$ and $\Itp_L^B$ be, respectively, an
algorithm to compute partial interpolants for leaf nodes and its dual.  Let
$\Itp_C^A$ and $\Itp_C^B$ be, respectively, an algorithm to compute partial
interpolants for non-leaf nodes and its dual.  If (i) for every leaf node $n$,
$\Itp_L^A(n) \rightarrow \Itp_L^B(n)$ and (ii) for every non-leaf node $m$,
$\Itp_C^A(m) \rightarrow \Itp_C^B(m)$, then the strength of the final
interpolant can be controlled by a labeling function $L : N \rightarrow \{s, w\}$,
such that a dual interpolant is used if the node's label is $w$, and the
conventional interpolant is used if the label is $s$.
In particular, given a strength operator $\sqsupseteq$ such that $s
\sqsupseteq s$, $s \sqsupseteq w$ and $w \sqsupseteq w$, we also have
that a labeling functions $L$ results in a \emph{stronger} interpolant
than a labeling function $L'$ (\emph{weaker}) if for every node $n$,
$L(n) \sqsupseteq L'(n)$.
%the $\tau$-interpolation system is an instance of the interpolation
%system template for theory $\tau$, using $\Itp_L^A$ and $\Itp_C^A$ for
%conventional interpolants and $\Itp_L^B$ and $Itp_C^B$ for dual
%interpolants.
%\end{theorem}

To instantiate the interpolation system template for a theory $\tau$,
conditions (i) and (ii) above need to be shown for $\tau$ and its proof
system.
%The conditions of the theorem above have to be proved for every instance of the
%interpolation system template.
In Sec.~\ref{section:euf_itp_system} we establish this relation for EUF,
and in Chapter~\ref{chapter:lra_interpolation} we use the notion of duality
of interpolants to create the LRA-interpolation system.

\iffalse
The maximum number of different interpolants available from a fixed dag
is therefore exponential in the size of the dag.
\fi
%
%In order to control when to compute the standard partial interpolant or
%its dual, we use \emph{labeling functions}.  Given a proof graph $T$
%with nodes $N$, a \emph{labeling function} is a function $L : N
%\rightarrow \{s, w\}$.
%
%When a node's label is $s$ the algorithm computes $\Itp^A$, and when a
%node's label is $w$ the algorithm computes the dual $\neg \Itp^B$.
%
%As a consequence of Theorem~\ref{theorem:template},
%a labeling function $L$ is guaranteed to generate logically stronger interpolants
%than a labeling function $L'$ if, for every node $n$, $L(n) \preceq L'(n)$,
%where $s \preceq s$, $s \preceq w$ and $w \preceq w$.
%The labeling function $L$ is then said to be \emph{stronger} than $L'$, which
%is in turn \emph{weaker}.

Algorithm~\ref{algorithm:generic_itp} describes on a high level the {\em
interpolation system template} for constructing interpolants for a
dag-like interpolation algorithms as the partial interpolant of the root
node based on the dual interpolants.  The template can be instantiated
to a $(\tau,L)$-interpolation algorithm by specifying the procedures
%
$\Itp_L^A, \Itp_L^B, \Itp_C^A,$ and $\Itp_C^B$
%
for the theory $\tau$, and by providing the labeling function $L$ for
the nodes.
\iffalse
\func{ComputeLeafItp}, \func{ComputeLeafItpDual},
\func{ComputeInterpolant}, \func{CombineChildrenItps}, and
\func{CombineChildrenItpsDual}
\fi
%

\iffalse
\begin{algorithm}
\caption{Interpolation System Template}
\label{algorithm:generic_itp}
\begin{algorithmic}[1]
\Procedure{ComputeInterpolant}{$\vars{node}$}
\State Children's partial interpolants $\vars{C} \leftarrow \{\}$
\For{each child $\vars{c}$ of $\vars{node}$}
\If{$\vars{c}$ is a leaf}
\If{\func{L}($\vars{c}$) = $s$}
\State{$\vars{C}~\leftarrow~\vars{C}~\cup~\Itp_L^A(\vars{c})$} %\func{ComputeLeafItp($\vars{c}$)}$}
\Else
\State{$\vars{C}~\leftarrow~\vars{C}~\cup~\Itp_L^B(\vars{c})$} %\func{ComputeLeafItpDual($\vars{c}$)}$}
\EndIf
\Else
\State{$\vars{C}~\leftarrow~\vars{C}~\cup~$\func{ComputeInterpolant}($\vars{c}$)}
\EndIf
\EndFor
\If{\func{L}($\vars{node}$) = $s$}
\Return $\Itp_C^A(\vars{C})$ %\func{CombineChildrenItps}($\vars{C}$)
\Else
\quad \Return $\Itp_C^B(\vars{C})$ %\func{CombineChildrenItpsDual}($\vars{C}$)
\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}
\fi

\begin{algorithm}[tb]
\caption{Interpolation System Template}
\label{algorithm:generic_itp}
\begin{algorithmic}[1]
\Procedure{ComputeInterpolant}{$\vars{node}$}
\If{$\vars{node}$ is a leaf}
\If{\func{L}($\vars{node}$) = $s$}
\quad \Return{$\Itp_L^A(\vars{c})$} %\func{ComputeLeafItp($\vars{c}$)}$}
\Else
\quad \Return{$\Itp_L^B(\vars{c})$} %\func{ComputeLeafItp($\vars{c}$)}$}
\EndIf
\EndIf
\State Children's partial interpolants $\vars{C} \leftarrow \{\}$
\For{each child $\vars{c}$ of $\vars{node}$}
\State{$\vars{C}~\leftarrow~\vars{C}~\cup~$\func{ComputeInterpolant}($\vars{c}$)}
\EndFor
\If{\func{L}($\vars{node}$) = $s$}
\Return $\Itp_C^A(\vars{C})$ %\func{CombineChildrenItps}($\vars{C}$)
\Else
\quad \Return $\Itp_C^B(\vars{C})$ %\func{CombineChildrenItpsDual}($\vars{C}$)
\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

Sec.~\ref{section:euf_itp_system} describes the EUF-interpolation system, an instance of the
interpolation system template.
