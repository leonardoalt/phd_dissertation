\section{Craig Interpolation}
\label{section:craig_interpolants}
An important component in many tasks related to symbolic model checking
is to divide an unsatisfiable formula into two parts, $A$ and $B$, and
compute a {\em Craig interpolant} $I$, defined over symbols appearing
both in $A$ and $B$, such that $A$ implies $I$ and $I \land
B$ is still unsatisfiable~\cite{Craig57}.
%
Assuming that $A \land B$ is a symbolic encoding
of the system $P$, the interpolant $I$ can be seen as an
over-approximation of the part of the program described in $A$ that is
sufficiently detailed to guarantee unsatisfiability with the problem
description in $B$.
Let $I'$ be another interpolant for $A$. If $I \rightarrow I'$, we say
that $I$ is \emph{stronger} than $I'$, which in turn is \emph{weaker} than $I$.
%
This type of problems shows up naturally in various verification approaches.
For instance, if $A$ describes a set of states and $B$ encodes an example of error-free behavior,
 a suitable interpolant $I$ can
be used to construct a safe inductive invariant for the states in
$A$~\cite{McMillan03}.  Similarly, if $A$ consists of a
description of a program function $f$ and $B$ consists of the rest of
the program together with negation of an assertion, $I$ can be
interpreted as an over-approximation of the function $f$ satisfying the
assertion~\cite{SFS_HVC11}.
%
Other applications of interpolation in model checking include the
refinement phase to synthesize new predicates~\cite{HJM+04}, or to
approximate the transition relation~\cite{JM05}.
%
Model checkers supporting interpolation include
\system{CPAChecker}~\cite{BeyerK:CAV11},
\system{CBMC}~\cite{CKL04}, \funfrog~\cite{SFS_ATVA12}, and
\evolcheck{}~\cite{FSS_TACAS13}, just to name a few.

%\paragraph{Interpolation Procedures. }

Depending on the first-order theory in which $A$ and $B$ is defined,
interpolants may be effectively computed from a proof of
unsatisfiability of $A \wedge B$.  For purely propositional formulas,
the methods described in~\cite{Pudlak97} (\Pud) \cite{Kra97} and \cite{McMillan2005} (\McM)
%and \cite{DSilva2010} (\McP)
can be
used to traverse the proof obtained from a propositional satisfiability
solver and compute interpolants. Another method, presented in \cite{DSilva2010}, is the
\emph{labeled interpolation system} (LIS), a powerful and generic framework
able to compute propositional interpolants of different strength. It also
introduces \McP, the dual of \McM, and
generalizes the main previous algorithms \Pud and \McM.
%
When the theory of $A$ and $B$ is more expressive than propositional
logic, it still is often possible to resort to an SMT solver to compute
an interpolant.  In this case the resolution proof of unsatisfiability
will contain original clauses from $A \wedge B$ plus theory-lemmas,
clauses produced by a theory solver representing tautologies.
%
Given this proof, it is possible to extract an interpolant using the
method described in~\cite{YM05}, provided that the theory-solver is
able to compute an interpolant from a conjunction of literals in the
theory.

In order to use interpolants in a straightforward manner, it is
necessary that they are {\em quantifier-free}.
%
It can be shown that every recursively enumerable theory that eliminates
quantifiers is {\em quantifier-free interpolating}, and every
quantifier-free interpolating theory eliminates
quantifiers~\cite{KapurMZ:SIGSOFT06}.
%
Examples of such theories are {\em equality with uninterpreted
functions}~($\euf$)~\cite{McMillan2005,Fuchs2009}, {\em difference
logic}~($\dl$)~\cite{Cimatti2008}, {\em linear real
arithmetic}~($\lra$)~\cite{McMillan2005,Cimatti2008}, and the {\em
bit-vector} ($\bv$) theories.
%
The case for theory of {\em linear integer arithmetic} ($\lia$) is, however,
more complicated.  For example the Presburger arithmetic (in its
original formulation) does not admit quantifier-free interpolants;
however the addition of stride predicates to the language makes the
theory quantifier-free interpolating~\cite{Pug91,Brillout2011,JCG08}.
Similarly also {\em theory of arrays} ($\mathcal{A}$) needs to be extended to obtain
quantifier-free interpolants~\cite{BruttomessoGR:RTA11}.

%\paragraph{Quality of Interpolants. }
Interpolants computed from proofs are known to be often highly
redundant~\cite{CabodiLVB:DATE2013}, necessitating different
approaches for optimizing them.
%
One way of compacting propositional interpolants is through applying transformations
to the resolution refutation.  For
example,~\cite{Rollini2013,Rollini2012} compare the effect of such
compaction on interpolation algorithms on three widely used
interpolation algorithms $\McM$, $\Pud$ and
$\McP$~\cite{DSilva2010} in connection with
function-summarization-based model
checking~\cite{FSS_TACAS13,SFS_ATVA12}.
%
A similar approach is studied in~\cite{DSilva2010} combined with an
analysis on the strength of the resulting interpolant.  Different
size-based reductions are further discussed
in~\cite{CabodiLVB:DATE2013,FontaineMWP:CADE2011}.
%
While often successful, these approaches might produce a considerable
overhead in large problems.
%
An interesting analysis in~\cite{BloemMSW:HVC2014} concentrates on the effect of identifying
subsumptions in the resolution proofs.
%
A significant reduction in the size of the interpolant can be obtained
by considering only CNF-shaped interpolants~\cite{Vizel2013}.
%
A light-weight interpolant compaction can be performed by specializing
through simplifying the interpolant with a truth
assignment~\cite{JancikKRS:FMCAD2014}.

In many verification approaches using counterexamples for refinement, it is
possible to abstract an interpolant obtained from a refuted
counterexample.  For instance,~\cite{Ruemmer2013}, and \cite{AlbertiBGRS:LPAR12}
present a framework for generalizing interpolants based on templates.  A
related approach for generalizing interpolants in unbounded
model-checking through abstraction is presented in~\cite{Cabodi2006}
using incremental SAT solving.
%
It is also possible to produce interpolants without the
proof~\cite{Chockler2013}, and there is a renewed interest in
interpolation techniques used in connection with modern ways of
organizing the high-level model-checking
algorithm~\cite{McMillan2014,Cabodi2014}.

\ \\
%\paragraph{Open problems}
\noindent
\textbf{Open problems.}
While Craig Interpolation has an established track record as an
over-approximation tool in symbolic model checking, its behavior
is still not fully understood. Moreover, the model checking applications have
no control over the interpolants, their suitability, size,
and strength. This thesis aims to solve the following
open problems:
\begin{itemize}
        \item Why aren't the current interpolation algorithms able to
            generate good interpolants regardless the interpolation
            problem?
        \item How can one generate interpolants that increase the performance
            of the overall verification process?
        \item How can the applications have more control over
            the interpolants it needs?
\end{itemize}

Our goal in this dissertation is to answer those questions in a sound way, and provide
a framework that allows interpolation based model checking to move forward.

\iffalse
While Craig Interpolation has an established track record as an
over-approximation tool in symbolic model checking, its behavior
is still not fully understood. After \cite{DSilva2010} introduced 
the Labeled Interpolation Systems, the strength of interpolants
became a candidate for a good control over the interpolants.
But \cite{Rollini2013} showed that different applications
need interpolants of different strength to be efficient. Therefore
a good control of the quality of interpolants still has not been
achieved, and remains a central problem for systems that do use
interpolation to achieve better performance.
\fi
