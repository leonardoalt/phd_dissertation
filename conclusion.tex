Craig interpolants have been successfully used in symbolic model checking as
means of over-approximation in different approaches. Interpolants can be
computed from resolution proofs of unsatisfiability, and being able to generate
multiple interpolants from a fixed proof is essential in the search for
optimized interpolants.

This thesis addresses the problems of how to give control over the generation
of interpolants to the application for different theories, and how to generate
interpolants that increase the performance of the application.  The main
research contributions are summarized as follows.

\ \\
\noindent
%\paragraph{Experimental and Theoretical Analysis of Labeling Functions for Propositional Logic.}
\textbf{Experimental and Theoretical Analysis of Labeling Functions for Propositional Logic.}
The first half of Chapter~\ref{chapter:interpolation_bool} starts by presenting
preliminary experiments that motivated a theoretical study on the labeling
functions.  These experiments showed which general properties of interpolants
are important, and that the labeling function from the literature should be
improved. A thorough theoretical analysis then follows, showing which
characteristics a labeling function needs in order to generate interpolants
with a small number of Boolean connectives.

\ \\
\noindent
%\paragraph{Proof-Sensitive Labeling Functions for Propositional Logic.}
\textbf{Proof-Sensitive Labeling Functions for Propositional Logic.}
Following on the theoretical analysis of labeling functions for propositional
logic, Chapter~\ref{chapter:interpolation_bool} presents the Proof-Sensitive (\PS)
labeling function, proven to generate small interpolants in an efficient way.
Two variants of \PS, \PSw and \PSs also guarantee the strength of the created
interpolants, which might be an important requirement in different
interpolation-based applications. Our experiments show that the Proof-Sensitive
labeling functions \PS and \PSs outperform the others when used in two model
checking scenarios, incremental checking and upgrade checking, by generating
smaller interpolants.

\ \\
\noindent
%\paragraph{Controlling EUF interpolants}
\textbf{Controlling EUF interpolants}
Chapter~\ref{chapter:euf_interpolation} presents the EUF-interpolation system,
a duality-based interpolation framework capable of generating multiple
interpolants for a single EUF proof of unsatisfiability. The framework allows
control over the interpolants using labeling functions which can be compared
with respect to strength. We also show that the strongest and weakest labeling
functions within the EUF-interpolation system are also the ones that lead to
the smallest number of equalities in an EUF interpolant. Our experiments with
\smtlib benchmarks show that the framework indeed generates interpolants of
different strength.  We also integrated it with a model checker, combining
propositional and EUF interpolation. We compared different combinations of
interpolation algorithms, and showed that (i) it is important to align the
strength of the interpolation algorithms for the different theories; and (ii)
the strongest and the weakest EUF labeling functions do lead to a better
performance in the model checker.

\ \\
\noindent
%\paragraph{Controlling LRA interpolants}
\textbf{Controlling LRA interpolants}
Chapter~\ref{chapter:lra_interpolation} introduces the LRA-interpolation
system, an interpolation framework capable of generating an infinite amount of
LRA interpolants from the same proof of unsatisfiability.  The main idea of the
LRA-interpolation system is to compute a conventional interpolant, using an LRA
interpolation algorithm from the literature, and its dual. We prove that these
two interpolants are different bounds represented as inequalities, and that it
is possible to derive an interval of real numbers leading to infinite
possibilities for LRA interpolants. We show how the strength of such
interpolants can be controlled using a normalized strength factor. We
integrated the LRA-interpolation system with a model checker and our
experiments show that it is very important to be able to fine tune the strength
of interpolants to help convergence in the model checker.

\ \\
\noindent
%\paragraph{Interpolating OpenSMT2.}
\textbf{Interpolating OpenSMT2.}
We have implemented all the novel interpolation frameworks for propositional
logic, EUF and LRA in the SMT solver \opensmt, creating an interpolation
module. \opensmt is an efficient and open source SMT solver that has support to
solving propositional logic, EUF and LRA, interpolation, and parallelism.
Chapter~\ref{section:opensmt2} describes the architecture and implementation
details of \opensmt. \opensmt is available at
\url{http://verify.inf.usi.ch/opensmt}.

\ \\
\noindent
%\paragraph{HiFrog.}
\textbf{HiFrog.}
Function summarization-based incremental model checkers suffer from the
exponential explosion in the size of the formulas when translating programs
into propositional logic. We attend this need by creating \hifrog, an
incremental bounded model checker that uses interpolants as over-approximations
of function summaries, and uses the theories EUF and LRA to encode C programs.
Bit-precision is often not necessary to prove safety properties, and our
experiments show that by using these first order theories, SMT-based
verification can be much faster than only SAT-based verification.
Chapter~\ref{section:hifrog} describes the architecture and implementation
details of \hifrog which can be found at \url{http://verify.inf.usi.ch/hifrog}.

\input{planned_future_work}
