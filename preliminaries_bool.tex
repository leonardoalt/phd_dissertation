\section{Propositional Logic Preliminaries}
\label{section:preliminaries_bool}

Given a finite set of propositional variables, a {\em literal} is a
variable $p$ or its negation $\neg{p}$.  A \emph{clause} is a finite set
of literals and a formula $\phi$ in \emph{conjunctive normal form} (CNF)
is a set of clauses.  We also refer to a clause as the disjunction of
its literals and a CNF formula as the conjunction of its clauses.
A variable $p$ occurs in the clause $C$, denoted by the pair $(p,
C)$, if either $p \in C$ or $\neg p \in C$.  The set $\var(\phi)$
consists of the variables that occur in the clauses of $\phi$.
%
We assume that double negations are removed, i.e.,
$\neg \neg p$ is rewritten as $p$.
%
A \emph{truth assignment} $\ta$ assigns a Boolean value to each variable
$p$.  A clause $C$ is satisfied if $p \in C$ and $\ta(p)$ is true, or
$\neg{p} \in C$ and $\ta(p)$ is false.  The propositional satisfiability
problem (SAT) is the problem of determining whether there is a truth
assignment satisfying each clause of a CNF formula $\phi$.
%
The special constants $\true$ and $\false$ denote the empty conjunction
and the empty disjunction.  The former is satisfied by all truth
assignments and the latter is satisfied by none.
%
A formula $\phi$ {\em implies} a formula $\phi'$, denoted
$\phi \rightarrow \phi'$, if every truth assignment satisfying $\phi$
satisfies $\phi'$.  The {\em size} of a propositional formula is the
number of logical connectives it contains.  For instance the
unsatisfiable CNF formula
%
\begin{equation}
\label{Eq:Example}
\phi = (x_1 \lor x_2) \wedge (\neg x_2 \lor x_4) \wedge (\neg x_2
\lor \neg x_3 \lor \neg x_4) \wedge (x_1 \lor x_3) \wedge (\neg x_1)
\end{equation}
%
of size 14 consists of 4 variables and 5 clauses.  The occurrences of
the variable $x_4$ are $(x_4, \neg x_2 \lor x_4)$ and $(x_4, \neg x_2
\lor \neg x_3 \lor \neg x_4)$.

For two clauses $C^+$, $C^-$ such that $p\in C^+$, $\neg p \in C^-$, and
%
for no other variable $q$ both $q \in C^- \cup C^+$ and $\neg q \in C^-
\cup C^+$,
%
a \emph{resolution step} is a triple $C^+$, $C^-$, $(C^+ \cup
C^-)\setminus \{p, \neg p\}$. The first two clauses are called the
\emph{antecedents}, the latter is the \emph{resolvent} and $p$ is the
\emph{pivot} of the resolution step.  A \emph{resolution refutation} $R$ of
an unsatisfiable formula $\phi$ is a directed acyclic graph where the
nodes are clauses and the edges are directed from the antecedents to the
resolvent.  The nodes of a refutation $R$ with no incoming edge are
the clauses of $\phi$, and the rest of the clauses are resolvents
derived with a resolution step.  The unique node with no outgoing edges
is the empty clause.  The {\em source clauses} of a refutation $R$ are the
clauses of $\phi$ from which there is a path to the empty clause.
Example~\ref{example:refutation} shows a proof of unsatisfiability
for a simple formula in CNF using subsequent uses of the resolution rule.

\begin{example}
\label{example:refutation}
Let $\psi$ be a propositional CNF consisting of the conjunction of
the following set of clauses:
$\{(x_1 \vee x_2),(x_1 \vee \neg x_2),(\neg x_1)\}$.
The tree below presents the proof that $\psi$ is unsatisfiable. The leaves are the
original clauses from $\psi$, and the edges are the application of a resolution rule
to infer a new clause. When the contradiction is reached ($\bot$) we have a proof
of unsatisfiability.
    
\begin{center}
\begin{forest}
[$\bot$,grow=north
    [$\neg{x_1}$]
    [$x_1$,grow=north
        [$(x_1 \vee \neg{x_2})$]
        [$(x_1 \vee x_2)$]
    ]
]
\end{forest}
\end{center}

\end{example}


\iffalse
Given an unsatisfiable formula $A \wedge B$, a \emph{Craig interpolant} $I$
for $A$ is a formula such that $A \rightarrow I$, $I \wedge B$ is
unsatisfiable and $\var(I) \subseteq \var(A) \cap \var(B)$.  An
interpolant can be seen as an over-approximation of $A$ that is still
unsatisfiable when conjoined with $B$.  In the rest of the paper we
assume that $A$ and $B$ only consist of the source clauses of $R$.
\fi
%
\iffalse
This poses no problems since the partitioning to $A$ and $B$ can be
initially performed on $\phi$ and the clauses that do not participate in
the refutation can be dropped by graph traversal.  The resulting
interpolant is still an over-approximation, since $A$ now contains fever
clauses.
\fi

The \emph{labeled interpolation system}~\cite{DSilva2010} (LIS) is a
framework that, given propositional formulas $A$, $B$, a refutation $R$
of ${A \land B}$ and a \emph{labeling function} $L$, computes an
interpolant $I$ for $A$ based on $R$.  The refutation together with the
partitioning $A, B$ is called an {\em interpolation instance} $(R, A,
B)$.
%
The labeling function $L$ assigns a label from the set $\{a,b,ab\}$ to
every variable occurrence $(p,C)$ in the clauses of the refutation $R$.
%
A variable is {\em shared} if it occurs both in $A$ and $B$; otherwise
it is {\em local}.  For all variable occurrences $(p, C)$ in $R$,
$L(p,C) = a$ if $p$ is local to $A$ and $L(p, C) = b$ if $p$ is local to
$B$.  For occurrences of shared variables in the source clauses the
label may be chosen freely.
%
The label of a variable occurrence in a resolvent $C$ is determined by
the label of the variable in its antecedents.  For a variable occurring
in both its antecedents with different labels, the label of the new
occurrence is $ab$, and in all other cases the label is equivalent to the
label in its antecedent or both antecedents.
%

An interpolation algorithm based on LIS computes an interpolant with a
dynamic algorithm by annotating each clause of $R$ with a {\em partial
interpolant} starting from the source clauses.
%
The partial interpolant of a source clause $C$ is
\begin{equation}
\label{Eq:SourceItps}
I(C) =
\left\{
\begin{array}{ll}
    \bigvee\{l \mid l \in C \textrm{ and } L(\var(l), C) = b\} &
\textrm{ if $C \in A$, and} \\
    \bigwedge\{\neg l \mid l \in C \textrm{ and } L(\var(l), C) = a\} &
\textrm{ if $C \in B$,}
\end{array}
\right.
\end{equation}
The partial interpolant of a resolvent clause $C$ with pivot $p$ and
antecedents $C^+$ and $C^-$, where $p \in C^+$ and $\neg p \in C^-$, is
\begin{equation}
\label{Eq:ResolventItps}
I(C) = \left\{
    \begin{array}{ll}
        I(C^+) \lor I(C^-) &
         \textrm{ if $L(p,C^+) = L(p,C^-) = a$,} \\
        I(C^+) \land I(C^-) &
         \textrm{ if $L(p,C^+) = L(p,C^-) = b$, and} \\
        (I(C^+) \lor p) \land (I(C^-) \lor \neg p) &
         \textrm{ otherwise.}
    \end{array}
\right.
\end{equation}

%In some applications it is useful to consider different interpolants
%constructed from a fixed interpolation instance, but using different
%interpolation algorithms~\cite{FSS_TACAS13}.  For such cases the LIS
The LIS
framework also provides a convenient tool for analyzing whether the
interpolants generated by one interpolation algorithm always imply the
interpolants generated by another algorithm.
%
If we order the three labels so that $b \le ab \le a$, it was shown \cite{DSilva2010}
that given two labeling functions $L$ and $L'$ resulting in the
interpolants $I_L$ and $I_{L'}$ in LIS and having the property that
$L(p,C) \le L'(p,C)$ for all occurrences $(p,C)$, it is true that
$I_{L} \rightarrow I_{L'}$.  In this case we say that the interpolation
algorithm obtained from LIS using the labeling $L'$ is {\em
weaker} than the interpolation algorithm that uses the
labeling $L$ ({\em stronger}).

The interpolation algorithms $\McM$ \cite{McMillan03} and $\Pud$ \cite{Pudlak97},
considered pioneers, can be obtained as special cases of LIS by providing a
labeling function returning $b$ and $ab$, respectively. By using the labels,
LIS provides a lattice that partially orders labeling functions, with
$\McM$ being the strongest labeling function.
A labeling function that always returns $a$ is considered dual to $\McM$, and is named $\McP$
\cite{DSilva2010}, since it is the weakest labeling function in LIS.

We define here two concepts that will be useful in Chapter~\ref{chapter:interpolation_bool}:
the class of {\em uniform} labeling functions, and the {\em internal
size} of an interpolant.
%
\begin{definition}
\label{def:uniform}
A labeling function is {\em uniform} if for all pairs of clauses $C, D
\in R$ containing the variable $p$, $L(p,C) = L(p,D)$, and no occurrence
is labeled $ab$.  Any interpolation algorithm with uniform labeling
function is also called uniform.
\end{definition}

An example of non-uniform labeling function is \Dmin, presented 
in~\cite{DSilva2010b}. \Dmin labels occurrences of shared variables by
copying its class, is proven to produce interpolants
with the least number of distinct variables.
%
\begin{definition}
The {\em internal size} $\IntSize{I}$ of an interpolant $I$ is the
number of connectives in $I$ excluding the connectives contributed by
the partial interpolants associated with the source clauses.
\end{definition}

%Typically, an interpolant constructed by a LIS-based algorithm
%contains a significant amount of subformulas that are syntactically
%equivalent.  The {\em structural sharing}, i.e., maintaining a unique
%copy of the syntactically equivalent subformulas, while completely
%transparent to the satisfiability, is of critical practical importance.
%
%Similarly important for performance is the {\em constant
%simplification}, consisting of four simple rewriting rules: $\true \land
%\phi \leadsto \phi$, $\false \land \phi \leadsto \false$, $\true \lor
%\phi \leadsto \true$, and $\false \lor \phi \leadsto \phi$, where $\phi$
%is an arbitrary Boolean formula.

\begin{figure}[tb]
\centering
\includegraphics[width=\linewidth]{figures/proofitpp1}
\caption{Different interpolants obtained from the refutation using the
partitioning $P_1$.}
\label{Fig:proofitpp1}
\end{figure}

The following example illustrates the concepts discussed in this section
by showing how LIS can be used to compute interpolants with two
different uniform algorithms $\McM$ and $\McP$.
%
\begin{example}
\label{ex:1}
Consider the unsatisfiable formula $\phi = A \land B$ where $\phi$ is
from Eq.~(\ref{Eq:Example}) and $A = (x_1 \lor x_2)$ and $B = (\neg x_2
\lor x_4) \wedge (\neg x_2 \lor \neg x_3 \lor \neg x_4) \wedge (x_1 \lor
x_3) \wedge (\neg x_1)$.
%
Fig.~\ref{Fig:proofitpp1} shows a resolution refutation for $\phi$ and
the partial interpolants computed by the interpolation algorithms $\McM$
and $\McP$.
%
Each clause in the refutation is associated with a partial interpolant
$\psi$ generated by labeling $L_{\McM}$ (denoted by $[\psi]^{\McM}$) and a partial
interpolant $\psi'$ generated by labeling $L_{\McP}$ (denoted by
$[\psi']^{\McP}$).  The generated interpolants are $\Itp_{\McM} = x_1
\lor x_2$ and $\Itp_{\McP} = (((\neg x_1 \wedge x_2) \lor x_1) \wedge x_2)
\lor x_1$.
%
Now consider a different partitioning $\phi' = A'\land B'$ for the same
formula where the partitions have been swapped, that is, $A' = B$ and
$B' = A$.  Using the same refutation, we
get the interpolants
$\Itp_{\McM}' = (((x_1 \lor \neg x_2) \wedge \neg x_1) \lor \neg x_2)
\land \neg x_1 = \neg \Itp_{\McP}$
and
$\Itp_{\McP}' = \neg(x_1 \lor x_2) = \neg \Itp_{\McM}$
%
%We use both structural sharing and constant simplification in the
%example.
The internal size of $\Itp_{\McM}$ is 0, whereas the internal
size of $\Itp_{\McP}$ is 4.
\end{example}

\iffalse
The two partitionings illustrate a case where the interpolation
algorithm $\McM$, in comparison to $\McP$, produces a small interpolant for
one and a large interpolant for another interpolation instance.
Since one of the goals of this work is to develop interpolation
algorithms that consistently produce small interpolants,
the labeling function of choice for propositional logic
cannot be $L_{\McM}$ or $L_{\McP}$.
%
Note that while in this case the interpolants $\Itp_{\McM}$ and
$\Itp_{\McP}$ are equivalent, the representation of $\Itp_{\McP}$ is considerably
smaller than the representation of $\Itp_{\McM}$.  Since minimizing a
propositional formula is an NP-complete problem, producing interpolants
that are small in the first place is a very difficult and important problem.
\fi

\section{Integration of Propositional and Theory Interpolation. }
\label{Sec:Integration}
Before we introduce the preliminaries for the first order theories
studied in this thesis, we describe how theory interpolants are
used in an SMT solver.

An SMT solver takes as input a propositional formula where some atoms are
interpreted over a theory, in our case equalities over uninterpreted functions
and inequalities over rational numbers.
%
If a satisfying truth assignment for the propositional structure is
found, a theory solver is queried to determine the consistency of its
equalities.  In case of inconsistency the theory solver adds a
reason-entailing clause to the propositional structure.  The process
ends when either a theory-consistent truth assignment is found or the
propositional structure becomes unsatisfiable.

The SMT framework provides a natural integration for the theory and
propositional interpolants.  The clauses provided by the theory solver
are annotated with their theory interpolant and are used as partial
interpolants as leaves of the refutation proof.
The propositional interpolation algorithm then proceeds as normal
and uses the theory interpolants instead of computing propositional
interpolants for theory clauses.
%
The propositional interpolation algorithms control the strength of the
resulting interpolant by choosing the partition for the shared variables
through labeling functions~\cite{Alt2016}. The theory interpolation systems
presented in this thesis also use labeling functions to generate interpolants
of different strength. If this is the case for the framework in use, the
labeling given by the propositional interpolation algorithm has to be followed
by the theory interpolation algorithm to preserve interpolant soundness.

