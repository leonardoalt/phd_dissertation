Model checking is a well established technique to verify systems, exhaustively
and automatically. The state space explosion, known as the main difficulty in
model checking scalability, has been successfully approached by symbolic model
checking which represents programs using logic, usually at the propositional or
first order theories level.

Craig interpolation is one of the most successful abstraction techniques used
in symbolic methods. Interpolants can be efficiently generated from proofs of
unsatisfiability, and have been used as means of over-approximation to generate
inductive invariants, refinement predicates, and function summaries.

However, interpolation is still not fully understood. For several theories it
is only possible to generate one interpolant, giving the interpolation-based
application no chance of further optimization via interpolation. For the
theories that have interpolation systems that are able to generate different
interpolants, it is not understood what makes one interpolant better than
another, and how to generate the most suitable ones for a particular
verification task.

The goal of this thesis is to address the problems of how to generate multiple
interpolants for theories that still lack this flexibility in their
interpolation algorithms, and how to aim at good interpolants.

This thesis extends the state-of-the-art by introducing novel interpolation
frameworks for different theories. For propositional logic, this work provides
a thorough theoretical analysis showing which properties are desirable in a
labeling function for the Labeled Interpolation Systems framework (LIS).  The
Proof-Sensitive labeling function is presented, and we prove that it generates
interpolants with the smallest number of Boolean connectives in the entire LIS
framework. Two variants that aim at controlling the logical strength of
propositional interpolants while maintaining a small size are given. The new
interpolation algorithms are compared to previous ones from the literature in
different model checking settings, showing that they consistently lead to a
better overall verification performance.

The Equalities and Uninterpreted Functions (EUF)-interpolation system,
presented in this thesis, is a duality-based interpolation framework capable of
generating multiple interpolants for a single proof of unsatisfiability, and
provides control over the logical strength of the interpolants it generates
using labeling functions. The labeling functions can be theoretically compared
with respect to their strength, and we prove that two of them generate the
interpolants with the smallest number of equalities. Our experiments follow the
theory, showing that the generated interpolants indeed have different logical
strength.  We combine propositional and EUF interpolation in a model checking
setting, and show that the strength of the interpolation algorithms for
different theories has to be aligned in order to generate smaller interpolants. 

This work also introduces the Linear Real Arithmetic (LRA)-interpolation
system, an interpolation framework for LRA. The framework is able to generate
infinitely many interpolants of different logical strength using the duality of
interpolants.  The strength of the LRA interpolants can be controlled by a
normalized strength factor, which makes it straightforward for an
interpolation-based application to choose the level of strength it wants for
the interpolants.  Our experiments with the LRA-interpolation system and a
model checker show that it is very important for the application to be able to
fine tune the strength of the LRA interpolants in order to achieve optimal
performance.

The interpolation frameworks were implemented and form the interpolation module
in \opensmt, an open source efficient SMT solver. \opensmt has been integrated
to the propositional interpolation-based model checkers \funfrog and
\evolcheck, and to the first order interpolation-based model checker \hifrog.
This thesis presents real life model checking experiments using the novel
interpolation frameworks and the tools aforementioned, showing the viability
and strengths of the techniques.

