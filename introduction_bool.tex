%\subsection{Introduction}
%\label{Sec:Introduction}

In SAT-based model checking, a widely used workflow for obtaining an
interpolant for a propositional formula $A$ is to compute a proof of
unsatisfiability for the formula $\phi = A \land B$, use a variety of
standard techniques for compressing the proof (see,
e.g.,~\cite{Rollini2013}), construct the interpolant from the
compressed proof, and finally simplify the
interpolant~\cite{CabodiLVB:DATE2013}.
%
LIS is a commonly used, flexible framework for computing the interpolant from a
given proof that generalizes several interpolation algorithms parameterized by
a labeling function.
%
Given a labeling function and a proof, LIS uniquely determines the
interpolant.  However, the LIS framework allows significant flexibility
in constructing interpolants from a proof through the choice of the
labeling function.

Arguably, the suitability of an interpolant depends ultimately on the
application~\cite{Rollini2013}, but there is a wide consensus
that small interpolants lead to better overall performance in model
checking
\cite{BloemMSW:HVC2014,Vizel2013,Rollini2013}.
%
However, generating small interpolants for a given partitioning is
a non-trivial task.
%
%This paper presents, to the best of our knowledge, the first thorough,
%rigorous analysis on how labeling in the LIS framework affects the size
%of the interpolant.  The analysis is backed up by experimentation
%showing also the practical significance of the result.  We believe that
%the results reported here will help the community working on
%interpolation in designing interpolation algorithms that work well
%independent of the application.
This chapter provides a thorough and rigorous analysis on how labeling
functions in the LIS framework affect the size of propositional interpolants.
%
Based on the analysis, the {\em proof-sensitive interpolation
algorithm} $\Itpalg{\PS}$ is presented. \PS produces small interpolants by adapting
itself to the proof of unsatisfiability.  We prove under reasonable
assumptions that the resulting interpolant is always smaller than those
generated by any other LIS-based algorithms, including the widely used
algorithms $\Itpalg{\McM}$ (McMillan~\cite{McMillan03}),
$\Itpalg{\Pud}$ (Pudl{\'a}k~\cite{Pudlak97}), and $\Itpalg{\McP}$ (dual
to $\Itpalg{\McM}$~\cite{DSilva2010}).

In some applications it is important to give guarantees on the logical
strength of the interpolants.  Since the LIS framework allows us to
argue about the resulting interpolants by their logical
strength~\cite{DSilva2010}, we know that for a fixed problem $A \land B$
and a fixed proof of unsatisfiability, an interpolant constructed with
$\Itpalg{\McM}$ implies one constructed with $\Itpalg{\Pud}$ which in
turn implies one constructed with $\Itpalg{\McP}$.
%
While $\Itpalg{\PS}$ is designed to control the interpolant size, we
additionally define two variants controlling the interpolant strength:
the strong and the weak proof-sensitive algorithms computing,
respectively, interpolants that imply the ones constructed by
$\Itpalg{\Pud}$ and that are implied by the ones constructed by
$\Itpalg{\Pud}$.

We implemented the new algorithms in \opensmt, and confirm the practical
significance of the algorithms with experiments presented in
this chapter.
%  The results
%show that when using $\Itpalg{\PS}$, both the sizes of the interpolants
%and the run times when used in a model-checking framework compare
%favorably to those obtained with $\Itpalg{\McM}, \Itpalg{\Pud}$, and
%$\Itpalg{\McP}$, resulting occasionally in significant reductions.
