\section{Experimental Evaluation}
\label{section:experiments_bool}

We compared the seven labeling functions for propositional interpolation
described in this chapter 
in the context of three different model-checking tasks:
%
(\emph{i}) incremental software model checking with function
summarization using \funfrog;
%
(\emph{ii}) checking software upgrades with function summarization using
\evolcheck;  and
%
(\emph{iii}) pre-image over-approximation for hardware model checking
with \pdtrav~\cite{Cabodi2006}.
%
The wide range of experiments permits the study of the general
applicability of the new techniques.  In experiments (\emph{i}) and
(\emph{ii}) the new algorithms are implemented within the verification
process allowing us to evaluate their effect on the full verification
run.
%
Experiment (\emph{iii}) focuses on the size of the interpolant, treating
the application as a black box.
%
Unlike in the theory presented in Section~\ref{Sec:PSI}, 
all experiments use both structural sharing and
constraint simplification, since the improvements given by
these practical techniques are important.
%
Experiments ({\emph{i}) and (\emph{ii}) use a large set of benchmarks
each containing a different call-tree structure and assertions
distributed on different levels of the tree.
%
For (\emph{iii}), the benchmarks consisted of a set of 100 interpolation
problems constructed by \pdtrav.  All experiments use
\opensmt both as the interpolation engine and as the SAT solver.

\iffalse
Fig.~\ref{figure:opensmt} shows a generic verification framework
employing the
new labeling mechanism for interpolation.  Whenever the application needs an
interpolant for the problem $A \land B$, it first requests the refutation
from the SAT solver. After the refutation is generated, the application
provides the partitioning to the proof analyser, which will generate functions
$f_A$ and $f_B$ (Def.~\ref{Definition:PS}).
%flexible
The labeling engine then creates a labeling function based on the partitions
$A$ and $B$, the functions $f_A$ and $f_B$, and a possible strength
requirement from the application, and then passes it to the interpolator.
The latter will finally construct an interpolant and return it
to the application.

\begin{figure}[tb]
\centering
\includegraphics[scale=0.33]{figures/periplo}
\caption{Overall verification/interpolation framework.}
\label{Fig:Framework}
\end{figure}
\fi

Different verification tasks
may require different kinds of interpolants.  For
example,~\cite{Rollini2013} reports that the \funfrog approach
works best with strong interpolants, whereas the \evolcheck 
techniques rely on weaker interpolants that
%
%Although the approach for (\emph{ii}) works better with weak
%interpolants, it has a limitation: this approach
%requires the
%interpolation algorithm to 
have the tree-interpolation property. As shown in~\cite{Rollini2012}, only
interpolation algorithms stronger than or equisatisfiable to $\Itpalg{\Pud}$
are guaranteed to generate sound tree interpolants.  The tree interpolation
strength requirement is restricted to this technique, and does not apply to
other similar interpolation problems, such as sequence interpolants.
%
Therefore, we evaluated only $\Itpalg{\McM}$, $\Itpalg{\Pud}$ and
$\Itpalg{\PS_s}$ for (\emph{ii}), and $\Itpalg{\McM}$, $\Itpalg{\Pud}$,
$\Itpalg{\McP}$, $\Itpalg{\PS}$, $\Itpalg{\PS_w}$ and $\Itpalg{\PS_s}$
for (\emph{i}) and (\emph{iii}).
%
\Dmin~was evaluated against the other algorithms for (\emph{i}), but
couldn't be evaluated for (\emph{ii}) because it does not preserve
the tree interpolation
property. For (\emph{iii}), \Dmin~was not evaluated due to its poor
performance in (\emph{i}).

In the experiments (\emph{i}) and (\emph{ii}), the overall verification
time of the tools and average size of interpolants were analysed.
For (\emph{iii}) only the size was analysed. In all the experiments
the size of an interpolant is the number of connectives in its
DAG representation.

\iffalse
In the rest of the paper we give an overview of techniques used in our
experimentation and report our experimental data demonstrating the
performance of the new interpolation algorithms.
\fi

\input{experiments_function_summaries}

\subsection{Over-approximating pre-image for Hardware Model Checking}

\pdtrav~\cite{Cabodi2006} implements several
verification techniques including a classical approach of unbounded
model checking for hardware designs~\cite{McMillan:CAV03}.  Given a
design and a property, the approach encodes the existence of a
counterexample of a fixed length $k$ into a SAT formula and checks its
satisfiability.  If the formula is unsatisfiable, proving that no
counterexample of length $k$ exists, Craig interpolation is used to
over-approximate the set of reachable states.  If the interpolation
finds a fixpoint, the method terminates reporting safety.  Otherwise, $k$
is incremented and the process is restarted.

\paragraph{Experiments.}

For this experiment, the benchmarks consist of interpolation instances
generated by \pdtrav.  We compare the effect of applying
different interpolation algorithms on the individual steps of the
verification procedure.

\begin{table}[tb]
\caption{Average size and increase relative to the winner for interpolants generated
when interpolating over $A$ (top) and $B$ (bottom) in $A\land B$ with
\pdtrav.}
\label{Table:pdtrav_both}
\centering
\begin{tabular}{lrrrrrr}
\toprule
& \multicolumn{6}{c}{\pdtrav} \\
 & $\McM$ & $\Pud$ & $\McP$ & $\PS$ & $\PS_w$ & $\PS_s$ \\
\cmidrule{1-1} \cmidrule(l){2-7}
\textbf{Avg size} & 683233 & 724844 & 753633 & {\bf 683215} & 722605 & 685455 \\
\textbf{increase \%} & 0.003 & 6 & 10 & 0 & 6 & 0.3\\
\cmidrule{1-1} \cmidrule(l){2-7}
\textbf{Avg size} & 699880  & 694372 & 649149 & {\bf 649013} & 650973 & 692434 \\
\textbf{increase \%} & 8 & 7 & 0.02 & 0 & 0.3 & 7 \\
\bottomrule
\end{tabular}
\end{table}

Table~\ref{Table:pdtrav_both} (top) shows the average size of the
interpolants generated for all the benchmarks using each interpolation
algorithm, and the relative size compared to the smallest interpolant.
%
Also for these approaches the best results are obtained from
$\Itpalg{\McM}$, $\Itpalg{\PS}$ and $\Itpalg{\PS_s}$, with
$\Itpalg{\PS}$ being the overall winner.
%
We note that $\Itpalg{\McM}$ performs better than $\Itpalg{\McP}$ likely
due to the structure of the interpolation instances in these benchmarks:
the partition $B$ in $A \land B$ is substantially larger than the
partition $A$.  This structure favors algorithms that label many
literals as $b$, since the partial interpolants associated with the
clauses in $B$ will be empty while the number of partial interpolants
associated with the partition $A$ will be small.
%
To further study this phenomenon we interchanged the partitions,
interpolating this time over $B$ in $A\land B$ for the same
benchmarks resulting in problems where the $A$ part is large.
%
Table~\ref{Table:pdtrav_both} (bottom) shows the average
size of the interpolants generated for these benchmarks and the relative
size difference compared to the winner.
%
Here $\Itpalg{\McP}$ and $\Itpalg{\PS_w}$ perform well, while
$\Itpalg{\PS}$ remains the overall winner.

\pdtrav experiments confirm in addition that $\Itpalg{\PS}$ is
very capable in adapting to the problem, giving best results in both
cases while the others work well in only one or the other.

We conclude that the experimental results are compatible with the
analysis in Sec.~\ref{Sec:PSI}.
%
In the \funfrog and \evolcheck experiments,
$\Itpalg{\PS_s}$ outperformed the other interpolation systems with
respect to verification time and interpolant size.

%\iffalse
%% Begin of strength experiments

\subsection{Strength of PS}
Section~\ref{Sec:PSI} states that we cannot know for sure the strength of the
PS algorithm, because it uses both $a$ and $b$ labels. In order to have an idea
of what kind of interpolants $Itp_{PS}$ would deliver with respect to strength,
the interpolants used in approaches $i)$ and $ii)$ were also stored and
compared to $Itp_P$.

In such a comparison, four cases may arise:
\begin{enumerate}
\item $Itp_{PS} \rightarrow Itp_P$. Meaning that $Itp_{PS}$ is stronger than
$Itp_P$. This case did not happen for any benchmark.
\item $Itp_P \rightarrow Itp_{PS}$. Meaning that $Itp_{PS}$ is weaker
than $Itp_P$. This case happened in 35 benchmarks.
\item $Itp_{PS} \leftrightarrow Itp_P$. Meaning that $Itp_{PS}$
is as strong as $Itp_P$ (and also as weak as). This case happened in 50 benchmarks.
\item No implication. Nothing
can be said about $Itp_{PS}$'s strength. This case happened in 219 benchmarks.
\end{enumerate}

We observed that out of 50 proofs from approach \emph{i)} and 254 proofs from approach \emph{ii)}, 
in most of them $Itp_{PS}$ was equivalent or weaker than $Itp_P$.
So even though it was shown in Section \ref{Sec:PSI} that we cannot know precisely the strength of $Itp_{PS}$,
this experimentation gives us evidence that we can expect $Itp_{PS}$ to be a slightly weaker than $Itp_P$.

%% End of strenght experiments
%\fi

%

\subsection{Effects of Simplification}

\label{Sec:EffectsOfSimplification}
It is interesting to note that in our experiments the algorithm
$\Itpalg{\PS}$ was not
always the best, and the non-uniform interpolation algorithm
$\Itpalg{\PS_s}$ sometimes produced the smallest interpolant, seemingly
contradicting Corollary~\ref{Corollary:SmallInterpolant}.  A possible reason
for this anomaly could be in the small difference in how constraint
simplification interacts with the interpolant structure.
%
Assume, in Eq.~\eqref{Eq:ResolventItps}, that $I(C^+)$ or $I(C^-)$ is
either constant true or false.  As a result in the first and the second
case respectively, the resolvent interpolant size decreases by one in
Eq.~\eqref{Eq:ResolventItps}.
%
However in the third case, potentially activated only for non-uniform
algorithms, the simplification if one of the antecedents' partial
interpolants is false
decreases the interpolant size by two, resulting in partial interpolants
with smaller internal size.
%
Therefore, in some cases, the good simplification behavior of
non-uniform algorithms such as $\Itpalg{\PS_s}$ seems to result in
slightly smaller interpolants compared to $\Itpalg{\PS}$. We believe
that this is also the reason why \Pud~behaves better than \McM~and
\McP~in some cases.

We also observed that in only five
of the benchmarks a labeling function led to interpolants with less
distinct variables, the difference between the largest and the smallest
number of distinct variables being never over 3\%, suggesting that
$p$-annihilable interpolation instances are rare.
%
Finally, we measured the effect of structural sharing.
To investigate the effect of structural sharing on simplifications, we analysed
two parameters: the number of connectives in an interpolant on its pure tree
representation ($Size_{Tree}$), and the number of connectives in an interpolant
on its DAG representation ($Size_{DAG}$), which is the result of the
application of structural sharing. Thus, we believe that the ratio $Size_{Tree}
/ Size_{DAG}$ is a good way to measure the amount of simplifications due to
structural sharing.

\begin{figure}
\includegraphics[scale=0.75]{figures/algorithms_plot_simplifications_unsorted_funfrog}
\caption{Relation $Size_{Tree} / Size_{DAG}$ on \funfrog benchmarks for
different interpolation algorithms}
\label{Fig:funfrog_simp_sh} \end{figure}

Fig.~\ref{Fig:funfrog_simp_sh} shows the results of this analysis on
\funfrog benchmarks. Each vertical line represents a benchmark, and
each point on this line represents the ratio $Size_{Tree} / Size_{DAG}$ of the
interpolant generated by each of the interpolation algorithms for the first assertion
of that benchmark. The reason why only the first assertion is considered is that
from the second assertion on, summaries (that is, interpolants) are used instead of
the original code, and therefore it is not guaranteed that the refutations will be
the same when different interpolation algorithms are applied.

It is noticeable that the existence of more/less simplifications is not related to
the interpolation algorithms, since all of them have cases where many/few
simplifications happen. Therefore, there is no difference between any of the
algorithms with respect to structural sharing.

\iffalse
The results (see
Appendix~\ref{Sec:Simplifications}) show that there is no noticeable,
consistent difference between any of the algorithms, suggesting that the
theory developed in Sec.~\ref{Sec:PSI} suffices to explain the
experimental observations.
\fi
