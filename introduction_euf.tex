An important skill in constructing mathematical proofs is to identify the
aspects of the problem that are relevant.  When applied to formal reasoning
about the correctness of a software this means ignoring the parts of the system
that play no role in its correctness.  One such approach that seems to work
well in automated software verification is to employ the theory of equality of
uninterpreted functions (EUF): in some cases it suffices to assume that a given
function returns the same value when invoked with the same arguments.  This
technique is particularly useful, for example, when modeling memory or
arrays~\cite{StumpBDL:LICS2001}, and proving program
equivalence~\cite{GodlinS13}.

Generalizing a formula over the states reachable by a program is a natural
subtask when summarizing the behavior of a procedure~\cite{SFS_ATVA12}, or
computing a fixed-point of a transition
function~\cite{McMillan03,Bradley:VMCAI2011}.  These techniques are now popular
in software
model-checking~\cite{BeyerK:CAV11,GurfinkelKKN:CAV15,CimattiG:CAV12}, resulting
in a growing interest in interpolation over uninterpreted functions.

The EUF theory is also very important for theory combination. The equality
operator is usually the common symbol between different theories, and EUF plays
a central and essential role when theories are combined. Theory combination is
a non-trivial task that attracts a lot of research, since new algorithms for
solving and interpolating are necessary.

The existing interpolation systems for EUF are treated as black-boxes, and only
generate one interpolant out of a refutation proof or give very little room for
flexibility when generating interpolants. This limits the applicability of
interpolants, in the sense that it might affect the performance of
interpolation-based applications in a way that makes it impractical. Therefore
it is necessary to allow the application to choose what kind of interpolants
should be generated, such that optimal performance is achieved.

This chapter presents the EUF-interpolation system, a duality based
interpolation system that aims at giving more control over the strength and
size of EUF interpolants.  We show that the EUF-interpolation system is an
instance $(\textrm{EUF},L)$ of the interpolation system template presented in
Chapter~\ref{chapter:generic_interpolation}, based on the \euf interpolation
algorithm presented in~\cite{McMillan2005,Fuchs2009}.
The EUF-interpolation system is able to generate several different interpolants
for the same interpolation problem, and allows strength control by means of
labeling functions.
%
To simplify the analytical discussion we describe the approach in a notation
slightly different from~\cite{Fuchs2009} that, with a specific labeling
function $L$ (see Example~\ref{example:itpalg}) constructs interpolants
syntactically equivalent to~\cite{Fuchs2009}.  Once we have presented the
EUF-interpolation system, we proceed to showing the strength result.

Section~\ref{section:experiments_euf} presents experiments involving the
EUF-interpolation system and various labeling functions in a controlled setting
and in a model checker.


