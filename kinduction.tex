\section{Interpolation as Means of Over-Approximation}

The first use of Craig interpolants in model checking was the interpolation and
SAT-based model checking algorithm from~\cite{McMillan03}. The basic idea of
the algorithm is to use bounded model checking as a subroutine, and find a
closed safe set of states to prove the safety property or a counterexample that
proves that the safety property is unsafe. This section describes this
algorithm in a high level.

We would first like to clarify the difference between the two meanings that
bounded model checking might have in the context of verification. The most
common and practical use of the term bounded model checking is to characterize
model checking algorithms that, given a bound $k$, unroll program loops and
recursive calls up to $k$, and verify safety properties with the unrolled code.
The second use is to refer to a similar approach, but related specifically to
transition systems, in the sense that the bound $k$ is used as the number of
steps from the initial states. In this case, a bounded model checking (BMC)
problem consists of a set of initial state, a transition function, a set of
error states, and the bound $k$. The bounded model checker has then as a goal
to determine if an error state can be reached from some initial state after the
application of at most $k$ steps. In this section we refer to the latter
definition of bounded model checking.

Let $S$ be a transition system representing a program to be verified with
respect to a safety property. Let $I$ be the set of initial states, $T$ the
transition function, $E$ the set of error states, and $k$ an integer.  A BMC
problem that verifies the safety of $S$ up to a bound $k$ can be represented as
the formula
\begin{equation}
\label{equation:bmc}
BMC(S, k) = I \wedge \left( \bigwedge_{0 \le i < k} T(s_i, s_{i+1}) \right)
\wedge \left( \bigvee_{s_e \in E} s_e \right).
\end{equation}

If $BMC(S, k)$ is satisfiable (SAT), the states represent an assignment for the
variables that is a counterexample for $S$. On the other hand, if $BMC(S, k)$
is unsatisfiable (UNSAT), the program is safe up to $k$ and interpolants can be
used to search for an inductive invariant. Suppose $BMC(S, k)$ is UNSAT.  Then,
it can be partitioned such that $A_1 = I \wedge T(s_0, s_1)$ and
$B_1 = \left(
\bigwedge_{1 \le i < k} T(s_i, s_{i+1}) \right) \wedge \left( \bigvee_{s_e \in
E} s_e \right)$,
and an interpolant $P_1$ for this partitioning is computed.
The interpolant $P_1$ is an over-approximation of the states that are reachable
from the set of initial states in one transition step. It is also unsatisfiable
when conjoined with $B_1$, meaning that no assignment satisfying $P_1$ can reach
$E$ in $k - 1$ steps.
Let $R$ be an over-approximation of the reachable states, initialized such that
$R = I$. If $P_1 \rightarrow R$, $R$ is an inductive invariant and $S$ is safe for the
general unbounded case. Otherwise, we update $R$ such that $R = R \vee P_1$,
compute an interpolant $P_2$ for the partitioning
$A_2 = I \wedge T(s_0, s_1) \wedge T(s_1, s_2)$ and
$B_2 = \left(
\bigwedge_{2 \le i < k} T(s_i, s_{i+1}) \right) \wedge \left( \bigvee_{s_e \in
E} s_e \right)$, and check if $P_2 \rightarrow R$ to see if $R$ is an inductive
invariant. This process repeats until (i) an inductive invariant is found; (ii)
a counterexample is found; or (iii) no inductive invariant or counterexample is
found after the generation of $k + 1$ interpolants. If (iii) happens,
$k$ needs to be increased and the process continued.

The work in~\cite{McMillan03} also proves that by increasing $k$, eventually
either a counterexample or an inductive invariant is found.
