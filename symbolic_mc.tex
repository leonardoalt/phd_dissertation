\section{Symbolic Model Checking}
\label{section:symbolic_mc}
{\em Model checking}~\cite{CE81,QS82} is a highly successful
technique for verifying hardware and software systems in a fully automated
manner. The technique is based on analysing the state space of the system and
verifying that the behavior of the program does not violate its specifications.
%
A system $P$ is described as a set of states $S$, a set $I \subseteq S$
of initial states, and a transition relation $T \subseteq S \times S$
specifying how the system state evolves over time.  A possibly infinite
sequence of states $s_0, s_1, \ldots$ where $s_0 \in I$, and $(s_i,
s_{i+1}) \in T$ for all $i$ is called an {\em execution} of the system.
%
{\em Safety properties} are a practically important class of
specifications expressing that certain states describing errors in the
system behavior should not be contained in any execution.
%
Safety properties are particularly important in verification since they
can be expressed as a reachability problem making the related
computations efficient.

For most systems, the concrete state space $S$ is too large to be
expressed explicitly.  {\em Symbolic model checking} addresses this
problem by expressing the state over a set of variables and the
transition relation as a formula over these variables.  The problem of
reachability can this way be reduced to determining the satisfiability
of a logical formula, a task that has received a significant amount
of interest lately with the development of efficient SAT and SMT solvers (see,
e.g.,~\cite{CimattiGSS:TACAS13,DeMouraB:TACAS08},
\cite{BruttomessoPST:TACAS10,Rollini2013}).

SAT-based bounded model checking~\cite{BCC+99,BCC+03} (BMC) is a
powerful approach to assure safety of software up to a fixed bound
$k$.  BMC works by 1) unwinding the symbolic transition relation up to $k$ steps,
2) encoding negation of an assertion (that essentially expresses a safety property) in the state reached after $k$ steps, and 3) passing the resulting formula to a SAT solver for
determining the satisfiability.  If the BMC formula is unsatisfiable, the
program is safe.  Otherwise, the program is unsafe, and each model of
the formula corresponds to a counterexample.  BMC has been successfully
used in several contexts~\cite{ACKS02,AMP06,JD08} and forms the basis of
several techniques aiming at unbounded model checking with safe
inductive invariant generation.


%\iffalse
%
Even though symbolic encoding of the system can be logarithmic in the
size of its search space $S$, the problem of determining satisfiability
is often still overwhelming.
%
To make computing more practical in such cases the system is {\em
abstracted} by grouping several states together and expressing the
transitions over these grouped states.
%
There are several ways how abstraction can be implemented in practice.
Most of them can be reduced to generating an abstract system $\hat{P}$
from a given system $P$ in such a way that the set of executions of $S$
is a sub-set of those of $\hat{S}$.
%
Thus, any property that can be shown to hold for all executions of
the abstraction $\hat{S}$ also holds for all executions of $S$.

In {\em abstract interpretation} the target is to compute inductive, but
not necessarily safe, invariants for the program using an abstract
simulation of the program~\cite{CousotC:POPL77}.  The abstraction is
obtained by substituting concrete program with {\em abstract domains}
suitable for the properties of the analyzed program (for examples,
see~\cite{CousotC:POPL77, ClarisoC:SAS04, HalbwachsP:SIGPLAN08}).
%
While greatly simplifying the model-checking task, abstraction might
allow property violations not present in the original software.  Such
{\em spurious counterexamples} can be used to refine the abstractions
through methods such as {\em predicate
abstraction}~\cite{GrafS:CAV97} and its refinements, {\em
counter-example guided abstraction refinement}
(CEGAR)~\cite{ClarkeGJLV:CAV00}, and {\em lazy
abstraction}~\cite{HenzingerJMS:POPL02,McMillan2006}.

The capability of the refinement procedure to find a suitable
abstraction level is a critical factor in determining the practical
success of a model checker based on abstract interpretation and refinement.
With the emergence of extended SAT/SMT solvers and symbolic encoding,
approaches based on {\em Craig
interpolation}~\cite{Craig57,McMillan03} have become very
important tools for computing abstractions. However,
it has been widely acknowledged that interpolation has
been used as a black box, limiting its flexibility and
usability. Applications have no control whatsoever on the resulting
interpolants. This thesis addresses the problem of the
ability to control interpolants.

%\fi

