\section{EUF Preliminaries}
\label{section:preliminaries_euf}
The theory of equalities and uninterpreted functions extends propositional
logic by adding equality ($=$) and disequality ($\neq$) to the logical symbols,
and allowing functions and predicates as non-logical symbols.
It has the following axioms:

\begin{equation}
\label{equation:euf_reflexivity}
x = x
\end{equation}

\begin{equation}
\label{equation:euf_symmetry}
x = y \Rightarrow y = x
\end{equation}

\begin{equation}
\label{equation:euf_transitivity}
(x = y \wedge y = z) \Rightarrow x = z
\end{equation}

\begin{equation}
\label{equation:euf_congruence}
(x_1 = y_1 \wedge \ldots \wedge x_n = y_n) \Rightarrow f(x_1, \ldots, x_n) = f(y_1, \ldots, y_n)
\end{equation}

Most EUF solvers rely on the \emph{congruence closure}
algorithm~\cite{NO80,NO05} to decide the satisfiability of a set of equalities
and disequalities. The algorithm, described in Alg.~\ref{algorithm:congruence},
has as input a finite set $\vars{Eq}$ of equalities, and the set $\vars{T}$ of
subterm-closed terms over which $\vars{Eq}$ is defined.
%
The main idea of the algorithm is to compute \emph{equivalence classes}, that
is, sets of terms that are equivalent. If in the end of the computation of the
equivalent classes, there are two nodes $u$ and $v$ such that they belong to
the same equivalent class and $(u \neq v)$ is an element of the set of problem
disequalities, the problem is unsatisfiable. If no such pair exists, the
problem is satisfiable.
%
During the execution the algorithm builds an undirected {\em congruence graph}
$G$ using the set $T$ as nodes, that represents the equivalence classes. If two
nodes are connected, they are equivalent. 
%
We write $(x \sim y)$ if there is a path in $G$ connecting $x$ and $y$.

\begin{algorithm}
\caption{Congruence closure}
\label{algorithm:congruence}
\begin{algorithmic}[1]
\Procedure{CongruenceClosure}{$\vars{T},\vars{Eq}$}
\State Initialize $\vars{E} \leftarrow \emptyset$ and $\vars{G}
\leftarrow (\vars{T},\vars{E})$
\Repeat{}
  pick $x, y \in \vars{T}$ such that $(x \not\sim y)$
  \If{(a) $(x = y) \in \vars{Eq}$ or\\
    \quad\quad\quad (b) $x$ is $f(x_1, \ldots, x_k)$, $y$ is $f(y_1, \ldots, y_k)$, and\\
    \quad\quad\quad$(x_1 \sim y_1), \ldots, (x_k \sim y_k)$
  }
    \State{\quad$\vars{E} \leftarrow \vars{E} \cup \{ (x, y) \}$}
  \EndIf
\Until{$\vars{E}$ does not grow}
\iffalse
\While{pick $x, y \in \vars{T}$ such that $(x \not\sim y)$}
\If{(a) $(x = y) \in Eq$ or\\
\quad\quad\quad (b) $x$ is $f(x_1, \ldots, x_k)$, $y$ is $f(y_1, \ldots, y_k)$,
                and\\
\quad\quad\quad$(x_1 \sim y_1), \ldots, (x_k \sim y_k)$
	}
\State{\quad$\vars{E} \leftarrow \vars{E} \cup \{ (x, y) \}$}
\EndIf
\EndWhile
\If{\vars{E} grew}
\State{repeat line 3}
\EndIf
\fi
\EndProcedure
\end{algorithmic}
\end{algorithm}

%\begin{enumerate}
%    \item Initialization step. $G = (T,\emptyset)$.
%    \item Pick $x, y \in T$ such that $(x \not\sim y)$.
%        \begin{itemize}
%            \item[(a)]If $(x = y) \in E$ or
%            \item[(b)]$x$ is $f(x_1, \ldots, x_k)$, $y$ is $f(y_1, \ldots, y_k)$,
%                and\\$(x_1 \sim y_1), \ldots, (x_k \sim y_k)$
%        \end{itemize}
%        then add the edge $(x, y)$ to $G$.
%    \item Repeat line 2 as long as it is possible.
%\end{enumerate}

\iffalse
\begin{theorem}[c.f.~\cite{NO80,NO05}]
\label{theorem:closure}
For every $x, y \in T$, $E \models (x = y)$
if and only if $(x \sim y)$. Moreover, the set $E \cup \{ (x \neq y) | (x \not\sim y) \}$
is satisfiable.
\end{theorem}
\fi

\begin{theorem}[c.f.~\cite{NO80,NO05}]
\label{theorem:closure}
Let $S\supseteq{\vars{Eq}}$ be a set containing EUF equalities and
disequalities over the terms $T$.  The set $S$ is satisfiable if and
only if the congruence graph $G$ constructed by
\algcall{CongruenceClosure}($\vars{T}, \vars{Eq}$) has no path $(x\sim
y)$ such that $(x \neq y) \in S$.
\end{theorem}

\iffalse
satisfiability can be decided by analyzing
the congruence graph $G$ created by a run of the congruence closure algorithm
with $E$ containing the equalities in $S$, and $T$ containing every
term and subterm from both equalities and disequalities from $S$.
If $(x \not\sim y)$ holds in $G$ for every disequality $(x \neq y)$ in $S$, it is
satisfiable. In case there is a disequality $(x \neq y)$ in $S$ such that
$(x \sim y)$ holds in $G$, it is unsatisfiable.
\fi

%Let $L = L_= \cup L_{\neq}$ be a set of EUF literals, where $L_=$ contains only
%equalities, and $L_{\neq}$ contains only disequalities. The satisfiability
%of the set $L$ can be checked by running the congruence closure algorithm
%with $E = L_=$ and $T$ containing every term and subterm from $L$.
%Theorem~\ref{theorem:closure} states that $L$ is satisfiable if and only if
%$(u \not\sim v)$ holds in the graph $G$ created by the congruence closure algorithm
%for every disequality $(u \neq v) \in L_{\neq}$. On the other hand, $L$ is unsatisfiable
%if and only if $(u \sim v)$ in $G$ for some disequality $(u \neq v) \in L_{\neq}$.

During the creation of $G$, an edge $(x, y)$ is added only if $(x \sim y)$ does
not hold, which ensures that $G$ is acyclic. Therefore,
for any pair of terms $x$ and $y$ such that $(x \sim y)$ holds in $G$, there is
a unique \emph{path} $\overline{xy}$ connecting these terms. Empty paths are
represented by $\overline{xx}$.
%
Example~\ref{example:euf_run} shows in detail how Alg.~\ref{algorithm:congruence} works.

\begin{example}
\label{example:euf_run}
Let $Eq = \{x = f(z), y = f(w), z = w\}$.  We have that $T = \{x, y, z, w,
f(x), f(y), f(z), f(w)\}$ is the set of nodes of the congruence graph, which in
the beginning of the algorithm contains no edges.
Fig.~\ref{figure:congruence_steps} shows a graphical representation of the
steps of the algorithm. We can pick the pairs $(x,y)$, $(x,z)$, $(x,w)$,
$(x,f(x))$ and $(x,f(y))$, but none of the conditions from the algorithm is
satisfied for those pairs. Let us then pick $(x,f(z))$. Condition (a) of the
algorithm is satisfied, so this pair is added as an edge in the congruence
graph (Fig.~\ref{figure:congruence2}). Let us now pick $(y,f(w))$. Condition (a) is also satisfied for this
pair, therefore it becomes an edge (Fig.~\ref{figure:congruence3}). Now the next and only pair we can pick that
changes the graph is $(z = w)$. Condition (a) is satisfied and it becomes an
edge (Fig.~\ref{figure:congruence4}). This edge enables the use of pair $(f(z),f(w))$, which satisfies
condition (b), resulting in a new edge. Now no pair can be picked, and
the algorithm terminates (Fig.~\ref{figure:congruence5}). We can see that $Eq$ is satisfiable and the algorithm
proved that the set $\{x, f(z), f(w), y\}$ is one of the equivalence classes.
Notice that if $(x \neq y) \in Eq$, it would be unsatisfiable, since
the algorithm proved that $(x = y)$ is true, which would contradict
an original disequality.
\end{example}


\begin{figure*}
    \centering

    \subfloat[Initial state of the algorithm, with an empty set of edges.]{\includegraphics[width=0.4\linewidth]{figures/congruence1}\label{figure:congruence1}}
    \quad\quad\quad\quad
    \subfloat[Introduction of the edge $(x,f(z))$]{\includegraphics[width=0.4\linewidth]{figures/congruence2}\label{figure:congruence2}}\\
    \subfloat[Introduction of the edge $(y,f(w))$]{\includegraphics[width=0.4\linewidth]{figures/congruence3}\label{figure:congruence3}}
    \quad\quad\quad\quad
    \subfloat[Introduction of the edge $(z,w)$]{\includegraphics[width=0.4\linewidth]{figures/congruence4}\label{figure:congruence4}}\\
    \subfloat[Final congruence graph]{\includegraphics[width=0.4\linewidth]{figures/congruence5}\label{figure:congruence5}}

    \caption{Computation of the congruence graph}
    \label{figure:congruence_steps}
\end{figure*}

For an arbitrary path $\pi$, we use the notation $\feq{\pi}$ to
represent the equality of the terms that $\pi$ connects. If, for
example, $\pi$ connects $x$ and $y$, then $\feq{\pi} := (x = y)$. We
also extend this notation over sets of paths $P$ so that $\feq{P} :=
\bigwedge_{\sigma \in P}\feq{\sigma}$.

%\begin{definition}[Congruence Graph \cite{Fuchs2009}]
%A \emph{congruence graph} is a graph $G$ obtained by running the congruence
%closure algorithm. The restriction $(u \not\sim v)$ in Step 2 guarantees 
%that every congruence graph is acyclic. Therefore, a \emph{path} uniquely
%connects $u$ and $v$ if $(u \sim v)$ holds in $G$, represented by $\overline{uv}$.
%Empty paths are represented by $\overline{uu}$.
%For an arbitrary path $\pi$, we use the notation $\feq{\pi}$ to represent the
%equality of the terms that $\pi$ connects. If, for example, $\pi$ connects $t$
%and $t'$, then $\feq{\pi} := (t = t')$. We also extend this notation to be used
%over sets of paths. Let $S$ be a set of paths. Then $\feq{S} :=
%\bigwedge_{\sigma \in S}\feq{\sigma}$.
%\end{definition}

An edge may be added to a congruence graph $G$ because of two different
reasons in Alg.~\ref{algorithm:congruence} at line~7. Edges added
because of Condition~$(a)$ are called \emph{basic}, while edges added
because of Condition~$(b)$ are called \emph{derived}.  Let $e$ be a derived
edge $(f(x_1, \ldots, x_k), f(y_1, \ldots, y_k))$.  The $k$ {\em parent
paths} of $e$ are $\overline{x_1y_1}, \ldots, \overline{x_ky_k}$.

%\begin{definition}[Basic and Derived edges \cite{Fuchs2009}]
%An edge in $G$ is either \emph{basic}, if it was introduced because of Condition $(a)$
%in Step 2, or \emph{derived}, if it was introduced because of Condition $(b)$.
%A derived edge $(f(u_1, \ldots, u_k), f(v_1, \ldots, v_k))$ has $k$ \emph{parent}
%paths $\overline{u_1v_1}, \ldots, \overline{u_k,v_k}$, some of which may be empty
%(but not all, otherwise this edge would not exist).
%\end{definition}

In order to generate an interpolant, the formula, or in this case,
the set of equalities, must be split into two partitions: $A \cup B$.
Terms, equalities and formulas are \emph{$a$-colorable} if all their
non-logical symbols occur in $A$, and \emph{$b$-colorable} if all their
non-logical symbols occur in $B$. They are \emph{colorable} if they are
$a$ or $b$-colorable, and \emph{$ab$-colorable} if both.  An edge $(x,
y)$ of a congruence graph has the same color as the equality $(x = y)$.
A path in a congruence graph is colorable if all its edges are
colorable, and a congruence graph is colorable if all its edges are
colorable.
%




%\begin{definition}[Colorability \cite{Fuchs2009}]
%Let $A$ and $B$ be sets of literals and let $\Sigma_A$ and $\Sigma_B$
%be the sets of non-logical symbols that occur in $A$ and $B$, respectively.
%Terms, literals, and formulas over $\Sigma_A$ are called $A$-colorable,
%those over $\Sigma_B$ are called $B$-colorable. Such expressions are called
%\emph{colorable} if they are either $A$-colorable or $B$-colorable, and
%$AB$-colorable if they are both.
%These definitions are extended to edges of congruence graphs over $A \cup B$
%so that an edge $(u, v)$ has the same colorability attributes as the equality
%$(u = v)$. A path in a congruence graph is colorable if all edges in the path are
%colorable, and a congruence graph is colorable if all its edges are colorable.
%\end{definition}

While it is possible to construct a non-colorable congruence graph, the
following lemma and its constructive proof in~\cite{Fuchs2009} state
that we may assume without loss of generality that congruence graphs are
colorable.
\begin{lemma}[c.f. \cite{Fuchs2009}]
\label{lemma:color}
If $x$ and $y$ are colorable terms and if $A, B \models (x = y)$, then there exist
a term set $T$ and a colorable congruence graph over the equalities contained
in $A \cup B$ and $T$ in which $(x \sim y)$.
\end{lemma}

Let $A$ and $B$ be two sets of equalities and disequalities. A
\emph{coloring} of a congruence graph $G = (E, T)$ created by a run of
the congruence closure algorithm over the equalities and terms of $A
\cup B$ is a function $C : E \rightarrow \{a, b\}$, that is, $C$ assigns
a color $a$ or $b$ to each edge, considering two restrictions:
%
\emph{(i)} basic edges $e$ must be colored with $a$ if $e \in A$ and
with $b$ if $e \in B$; and
%
\emph{(ii)} if an edge has color $c$, both its endpoints must be
$c$-colorable.  $ab$-colorable derived edges can be colored arbitrarily.
%
%Notice that colors and labels are different concepts.  Unlike labels $a, b, ab$
%presented in Section~\ref{section:preliminaries_bool} and labels $s, w$ presented in
%Chapter~\ref{chapter:generic_interpolation}, colors $a, b$ are EUF specific and
%tell if a node or edge in a congruence graph belongs only to $A$, only to $B$,
%or is common between them, whereas labels are used while deciding to whether to
%use the standard or the dual interpolant.

We denote a congruence graph $G$ colored with a function $C$ by $G^C$.
A path is called an \emph{$a$-path} if all its edges are colored $a$,
and a \emph{$b$-path} if all its edges are colored $b$.
%
A \emph{factor} of a path in $G^C$ is a maximal subpath such that all
its edges have the same color. Notice that every path is uniquely
represented as a concatenation of the consecutive factors of opposite
colors.

\begin{example}
\label{example:preliminaries}
Let $A := \{ (v_1 = f(y_1)), (f(y_2) = v_2), (y_1 = t_1), (t_2 = y_2), (s_1 = f(r_1)), (f(r_2) = s_2), (r_1 = u_1), (u_2 = r_2) \}$
and $B := \{ (x_1 = v_1), (v_2 = x_2), (t_1 = f(z_1)), (f(z_2) = t_2), (z_1 =
s_1), (r_1 = r_2), (s_2 = z_2), (u_1 = u_2), (x_1 \neq x_2) \}$.
Fig.~\ref{figure:example} shows a colored congruence graph $G^C$ built
while proving the unsatisfiability of $A$ and $B$ with
Alg.~\ref{algorithm:congruence}.
%
%The curvy edges with the labels \emph{s} or \emph{w} in $G^C$ are not relevant
%for this example and are used later in
%Section~\ref{Sec:InterpolationSystemForEUF}.
%
The congruence graph $G^C$ demonstrates the joint unsatisfiability of $A$ and
$B$, since it proves $(x_1 = x_2)$ and $(x_1 \neq x_2)$ is an original term.
Edges are represented by rectangles, and dotted arrows point to the parents of
derived edges.  Black rectangles and circles represent $a$-colorable nodes
(terms) and $a$-colored edges, white edges and circles represent $b$-colorable
nodes and $b$-colored edges by, and half filled circles represent
$ab$-colorable nodes.
%
In the first (top) path of $G^C$, we can see that basic edges (original equalities from $A \cup B$)
are used to prove $(r_1 = r_2)$. This fact is used to infer $(f(r_1) = f(r_2))$,
which is in turn used as a derived edge in the path below, proving $(z_1 = z_2)$.
The equality $(f(z_1) = f(z_2))$ is then inferred and used to prove $(y_1 = y_2)$
in the path below. In the last
(bottom) path of $G^C$, the derived edge representing $(f(y_1) = f(y_2))$ is created and finally
$(x_1 = x_2)$ is proved.

\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/example}
\caption{Congruence graph $G^C$ that proves the unsatisfiability of $A \cup B$}
\label{figure:example}
\end{figure}

\end{example}

We discuss interpolant generation from congruence graphs in Chapter~\ref{chapter:euf_interpolation}.
