\section{Proofs for Chapter~\ref{chapter:euf_interpolation}}
\label{appendix:proofs_euf}
%\begin{theorem}
\textbf{Theorem~\ref{theorem:correctness}.}
\emph{
Given two sets of equalities and disequalities $A$ and $B$ such that $A \cup B$
is unsatisfiable, $G^C$ a colored congruence graph containing a path $\pi :=
\opath{xy}$ such that $(x \neq y) \in A \cup B$ and a labeling function $L$,
Eq.~(\ref{equation:Itp}) computes a valid interpolant for $A$ using $L$ over
$G^C$.
}
%\end{theorem}
\begin{proof}
In order to analyze Eq.~(\ref{equation:Itp}) we have to analyze
Eq.~(\ref{equation:Iprime}), Eq.~(\ref{equation:Sprime}),
Eq.~(\ref{equation:lisI}) and Eq.~(\ref{equation:lisS}).

\emph{(i) $I'_A(\pi)$}\\
Eq.~(\ref{equation:Iprime}) is the same presented in \cite{Fuchs2009}, and
computes interpolants for $A$ when the disequality $(x \neq y)$ is in $A$.

\emph{(ii) $\neg I'_B(\pi)$}\\
Eq.~(\ref{equation:Sprime}) is the dual of Eq.~(\ref{equation:Iprime}), and
clearly computes interpolants for $B$ when the disequality is in $B$. We can
transform it into an interpolant for $A$ by negating it, as it is done in
Eq.~(\ref{equation:Itp}).

\emph{(iii) $I_A(\pi)$}\\
Eq.~(\ref{equation:lisI}) behaves similarly to the interpolation procedure
presented in \cite{Fuchs2009}, with the addition of dual interpolants
and labeling functions. First $J_I$ computes the individual contribution
of the $A$-factors that prove $\pi$ in $G^C$ ($\A(\pi)$) to the interpolant,
and then conjoins it with the interpolants of their $B$-premise sets ($\BA(\pi)$).

\emph{(iv) $\neg I_B(\pi)$}\\
Eq.~(\ref{equation:lisS}) is the dual of Eq.~(\ref{equation:lisI}) and computes
an interpolant for $B$, which can be transformed into an interpolant for $A$ by
negating it.

\iffalse
With respect to Eqs.~(\ref{equation:lisI}) and~(\ref{equation:lisS}), we have two cases:
\begin{paragraph}{Case 1} the used labeling function simply returns $s$
for any given path.\\
In this case, 
the resulting interpolants are equivalent to the ones given in \cite{Fuchs2009}
(referred as $I$) when the used labeling function simply returns $s$ for
any given path, which disables the use of dual interpolants.
%
The change of notation from $I$ to $I_A$ is merely a tool to make the proofs more readable.
%
As $I$ does, $I_A$ also computes $J_A$ for the $A$-factors that prove $\pi$.
%
The only difference is that $I$ is called over both $A$ and $B$-factors, and
decides what to do based on whether the argument consists of multiple factors,
a single $A$-factor or a single $B$-factor, whereas $I_A$'s argument is either a
path potentially containing multiple factors (fist call) or a single $B$-factor
that is part of the $B$-premise set previously used.
%
%The new notation for $I_A'$ is simply an adaptation to the new notation of $I$.
%
Notice that regardless the notation both procedures generate the same
interpolant for a given problem.
%
Eq.~(\ref{equation:lisS}) is analogous to Eq.~(\ref{equation:lisI}),
and computes interpolants for $B$.
\end{paragraph}

\begin{paragraph}{Case 2} the labeling function is non trivial.\\
In~\cite{Fuchs2009}, it is shown that combining valid partial interpolants with
a simple conjunction is a valid procedure. We reuse this combination procedure
here, and together with the duality idea presented in Section~\ref{subsection:generic_itp}
we have showed that the equations above fit
the template given in Algorithm~\ref{algorithm:generic_itp} making use
of non trivial labeling functions.
\end{paragraph}
\fi
\end{proof}

\begin{definition}
\label{def:relevant}
Let $G$ be a congruence graph, and
$\sigma$ an arbitrary factor from $G$. We say that
$\sigma$ is \emph{relevant} to $\omega$ if
either $J_A(\sigma)$ or $J_B(\sigma)$ is called during
the computation of $I_A(\omega)$ or $I_B(\omega)$.
\end{definition}    

\noindent
\textbf{Lemma~\ref{lemma:str1}.}
\emph{
Let $G^C$ be a congruence graph with coloring $C$, and $\omega$ a factor from $G$.
Then $I_A(\omega) \wedge I_B(\omega) \rightarrow \feq{\omega}$.
}
\begin{proof}
Let $R^{\omega}$ be the set of factors relevant (Def.~\ref{def:relevant})
to $\omega$ in a congruence graph, $R^{\omega}_A$ the subset of
$R^{\omega}$ containing only $A$-factors and $R^{\omega}_B$ the subset of
$R^{\omega}$ containing only $B$-factors.
%
%By definition of relevant factors, $L_s$, $L_w$, Eq.~(\ref{equation:lisI}), and
%Eq.~(\ref{equation:lisS}), we have that $I_A(\omega) = \bigwedge_{r \in
%R^{\omega}_A}J_A(r)$ and $I_B(\omega) = \bigwedge_{r \in R^{\omega}_B}J_B(r)$.

From Eq.~(\ref{equation:lisI}) we can clearly see that
\begin{equation}
\label{equation:RIinf}
R^{\omega}_A := \A(\omega) \cup \A(\BA(\omega)) \cup \ldots \cup
\A((\BA)^k(\omega)) \cup \ldots,
\end{equation}
and from Eq.~(\ref{equation:lisS}) that
\begin{equation}
\label{equation:RSinf}
R^{\omega}_B := \B(\omega) \cup \B(\AB(\omega)) \cup \ldots \cup
\B((\AB)^k(\omega)) \cup \ldots.
\end{equation}

Because congruence graphs are acyclic and finite, for any $\omega$
there exists an integer $n$ such that $\A((\BA)^n(\omega)) = \emptyset$
and $\B((\AB)^n(\omega)) = \emptyset$,
which allows us to rewrite the previous equations as
\begin{equation}
\label{equation:RI}
R^{\omega}_A := \A(\omega) \cup \A(\BA(\omega)) \cup \ldots \cup
\A((\BA)^n(\omega)),
\end{equation}
\begin{equation}
\label{equation:RS}
R^{\omega}_B := \B(\omega) \cup \B(\AB(\omega)) \cup \ldots \cup
\B((\AB)^n(\omega)).
\end{equation}

If $\omega$ is an $A$-factor, we have that $\A(\omega) = \{\omega\}$
and therefore we can write $\B(\omega) \equiv \BA(\omega)$. Using that
we can infer that $\A((\BA)^{n+1}(\omega)) = \emptyset$ (by the definition
of $n$), and we can change
Eq.~(\ref{equation:RS}) to
\begin{equation}
\label{equation:RSchanged}
R^{\omega}_B := \BA(\omega) \cup (\BA)^2(\omega)) \cup \ldots \cup
(\BA)^{n+1}(\omega).
\end{equation}
We follow the proof assuming that $\omega$ is an $A$-factor,
using Eq.~(\ref{equation:RI}) and Eq.~(\ref{equation:RSchanged})
to represent $R_A^\omega$ and $R_B^\omega$ respectively,
and then argue that the proof is symmetrical for the case
where $\omega$ is a $B$-factor.

%Notice that $(\BA)^n(\omega)$ (from $R^{\omega}_S$)
%could already have an empty premise set, in which case
%$\A((\BA)^n(\omega))$ would not be necessary.
%We decide to use $\A((\BA)^n(\omega))$ as the first
%to have an empty premise set (meaning that $(\BA)^{n+1}(\omega) = \emptyset$)
%for simplicity reasons,
%and it is still valid because $\B(\emptyset) = \emptyset$.

From Eq.~(\ref{equation:RI}) and Eq.~(\ref{equation:RSchanged}), we can then see that
\begin{equation}
\begin{aligned}
R^{\omega} :=
%R^{\omega}_A \cup R^{\omega}_B =
\A(\omega)
\cup \BA(\omega) \cup \A(\BA(\omega))
\cup \ldots \cup
\A((\BA)^n(\omega)) \cup (\BA)^{n+1}(\omega)
\end{aligned}
\end{equation}
and therefore
\begin{equation}
I_A(\omega) \wedge I_B(\omega) = 
( \bigwedge_{\sigma \in R^{\omega}_A}J_A(\sigma) )
\wedge
( \bigwedge_{\sigma \in R^{\omega}_B}J_B(\sigma) ).
\end{equation}
When $J_A$ and $J_B$ are computed over a set
that has an empty premise set, the result is
not a conjunction of implications, but a conjunction of equalities.
In this case,
$J_B((\BA)^{n+1}(\omega)) = \feq{(\BA)^{n+1}(\omega)}$
because $\A((\BA)^{n+1}(\omega)) = \emptyset$.
Thus, after applying the functions $J_A$ and $J_B$ we have that
\begin{equation}
\label{equation:ISeq}
\begin{array}{ll}
I_A(\omega) \wedge I_B(\omega) =
 &
\left(
 \bigwedge_{i=0}^n
 \bigwedge_{\sigma \in \A((\BA)^i(\omega))}
         ( \JI{\sigma} )
\right) \\
%\wedge \feq{\A((\BA)^n(\omega))} \wedge \\
 &
 \wedge
\left(
\bigwedge_{i=0}^{n}
\bigwedge_{\sigma \in (\BA)^i(\omega)}
        ( \JS{\sigma} )
\right)\\ &
\wedge \feq{(\BA)^{n+1}(\omega)}.
\end{array}
\end{equation}
We know that formulas of the form
$((a_1 \rightarrow b_1) \wedge \ldots \wedge (a_n \rightarrow b_n))
\rightarrow
( (\bigwedge_{i \in 1..n}a_i) \rightarrow (\bigwedge_{i \in 1..n}b_i) )$
are tautologies. Using that and Eq.~(\ref{equation:ISeq}), we can then show that
\begin{equation}
\label{equation:ISimply}
\begin{array}{ll}
I_A(\omega) \wedge I_B(\omega) & \rightarrow
    \left( \bigwedge_{i=0}^n ( \feq{(\BA)^{i+1}(\omega)} \rightarrow
    \feq{\A((\BA)^i(\omega))} ) \right) \\
%    ( \feq{\BA(\omega)} \rightarrow \feq{\A(\omega)} )
%\wedge \feq{\A((\BA)^n(\omega))} \wedge \\
& \wedge
\left( \bigwedge_{i=0}^{n} ( \feq{\A((\BA)^i(\omega))} \rightarrow
\feq{(\BA)^i(\omega)} ) \right) \\&
\wedge \feq{(\BA)^{n+1}(\omega)}
\end{array}
\end{equation}

Because $\feq{(\BA)^{n+1}(\omega)}$ has no
implicant, it has to be true in the formula, starting a chain that
satisfies the antecedents of all the implications in Eq.~(\ref{equation:ISimply}),
since $(\BA)^{n+1}(\omega)$ is the premise set
of $\A((\BA)^n(\omega))$, which is the premise set of $(\BA)^n(\omega)$
and so on.
Because of that, we can simplify the formula to
\begin{equation}
I_A(\omega) \wedge I_B(\omega) \rightarrow\\
\left( \bigwedge_{i=0}^{n} \feq{\A((\BA)^i(\omega))} \right)
\wedge
\left( \bigwedge_{i=0}^{n+1} \feq{(\BA)^i(\omega)} \right).
\end{equation}

Therefore, we have that
\begin{equation}
\label{equation:imply}
\forall r \in R^{\omega} . (I_A(\omega) \wedge
I_B(\omega)) \rightarrow \feq{r}.
\end{equation}

For the case where $\omega$ is a $B$-factor, we have that
$\B(\omega) = \{\omega\}$, which implies that $\AB(\omega) \equiv \A(\omega)$.
Using that, we can infer that $\B((\AB)^{n+1}(\omega)) = \emptyset$
and we can also change Eq.~(\ref{equation:RI}) to
\begin{equation}
\label{equation:RIchanged}
    R^{\omega}_A := \AB(\omega) \cup (\AB)^2(\omega) \cup \ldots \cup
(\AB)^n(\omega).
\end{equation}
Using Eq.~(\ref{equation:RIchanged}) and Eq.~(\ref{equation:RS}) to
represent $R_A^\omega$ and $R_B^\omega$ respectively, the same reasoning
is followed and we can show that Eq.~(\ref{equation:imply}) holds
also if $\omega$ is a $B$-factor.

If $\omega$ is an $A$-factor, then $\A(\omega) = \{\omega\}$
and $J_A$ is called for $\omega$ in the first iteration of
Eq.~(\ref{equation:lisI}). On the other hand, if $\omega$ is
a $B$-factor, then $\B(\omega) = \{\omega\}$ and
$J_B$ is called for $\omega$ in the first iteration of
Eq.~(\ref{equation:lisS}).
%In particular $\omega \in
%R^{\omega}$, since $\omega$ is an $A$-factor by the assumption of
%Lemma~\ref{lemma:factor} and therefore $\A(\omega) = \{\omega\}$
%implying $J_I$ is called for $\omega$ in the first iteration of
%Eq.~\ref{equation:I}. 
This shows that $\omega$ is a relevant factor
and by Eq.~(\ref{equation:imply}) we conclude the proof.
% the case where
%$\omega$ is an A-factor.  The proof is symmetrical to the case
%when $\omega$ is a $B$-factor.
%$\qed$
\end{proof}

\noindent
\textbf{Lemma~\ref{lemma:Ifactors}.}
\emph{
Let $\pi$ be an arbitrary path in the congruence graph, and $\facts(\pi)$ the
set of all factors in $\pi$.  Then
$I_A(\pi) = \bigwedge_{\sigma \in \facts(\pi)}I_A(\sigma)$ and
$I_B(\pi) = \bigwedge_{\sigma \in \facts(\pi)}I_B(\sigma)$.
}
\begin{proof}
By the definition of $\A$ in Eq.~(\ref{equation:A}), we know that
$\A(\pi) = \bigcup_{\sigma \in \facts(\pi)}\A(\sigma)$.
By the definition of $I_A$, we can see that $J_A$ is computed
individually for each element of $\A(\pi)$ in $I_A(\pi)$,
and $I_A$ is called recursively for the $B$-premise sets
of each individual element of $\A(\pi)$. Therefore,
$\bigwedge_{\sigma \in \facts(\pi)}I_A(\sigma) = 
\bigwedge_{\sigma \in \A(\pi)}I_A(\sigma)$, which has the same effect of $I_A(\pi)$.
The result is analogous for $I_B(\pi)$.
\end{proof}

\noindent
\textbf{Lemma~\ref{lemma:multiple}.}
\emph{
Lemma~\ref{lemma:str1} holds when $\omega$ is a path containing multiple factors.
}
\begin{proof}
Let $\omega$ be a path built by multiple factors and
$\facts(\omega)$ the set containing these factors.
By Lemma~\ref{lemma:Ifactors} we know that
$I_A(\omega) = \bigwedge_{\sigma \in \facts(\omega)}I_A(\sigma)$ and
$I_B(\omega) = \bigwedge_{\sigma \in \facts(\omega)}I_B(\sigma)$;
by Lemma~\ref{lemma:str1} we know that
$\forall \sigma \in \facts(\omega) . (I_A(\sigma) \wedge I_B(\sigma)) \rightarrow \feq{\sigma}$.
Because the elements of $\facts(\omega)$ are factors linking nodes to prove $\omega$,
we know that $(\bigwedge_{\sigma \in \facts(\omega)}\feq{\sigma}) \rightarrow \feq{\omega}$.
Therefore we have that $(I_A(\omega) \wedge I_B(\omega)) \rightarrow \feq{\omega}$.
\end{proof}

\noindent
\textbf{Theorem~\ref{theorem:str1}.}
\emph{
For fixed $A, B, G^C,$ and $\opath{xy}$, for the corresponding interpolants
defined in Eq.~(\ref{equation:Itp}) it holds that
$\Itp(A,B,G^C,\opath{xy},L_s)
\rightarrow \Itp(A,B,G^C,\opath{xy},L_w)$.
}
\begin{proof}
We only consider the case where $(x \not= y) \in B$ and note that the
case where $(x \not= y) \in A$ is completely symmetrical.
%
Let $\pi := \opath{xy}$ and $\psi = I_A(\pi) \wedge I_B'(\pi)$.  By
Eq.~(\ref{equation:Sprime}) we have that
\begin{equation}
\label{psi}
\begin{aligned}
\psi =
I_B(\theta_A) \wedge
%\left(
\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_B(\sigma)
%\right)
%    \bigwedge \{ S( \{ \sigma \} ) | \sigma \in \AU{\pi_1}{\pi_2} \}
\wedge
\left( \feq{\AU{\pi_1}{\pi_2}} \rightarrow \nfeq{\theta_A} \right)
\wedge
%( \bigwedge_{\sigma \in \A(S_\pi)}J_I(\sigma) ) \wedge
%I(\BA(\{\pi\}))
I_A(\pi),
\end{aligned}
\end{equation}
where $\pi$ is decomposed as $\pi_1\theta_A\pi_2$,
and $\theta_A$ is the largest subpath of $\pi$ with
$A$-colorable endpoints.
In order to show that $I_A(\pi) \rightarrow \neg I_B'(\pi)$,
we prove that $\psi \rightarrow \bot$, which leads to
the theorem.
In $I_B'(\pi)$, $\pi$ is split into $\pi_1 \theta_A \pi_2$.
From the definition of $\theta_A$, we know that $\pi_1$
and $\pi_2$ are $B$-factors. Therefore, using Lemma~\ref{lemma:Ifactors},
we can say that
$I_A(\pi) = I_A(\pi_1) \wedge I_A(\theta_A) \wedge I_A(\pi_2)$.
Because $\pi_1$ and $\pi_2$ are subpaths of $\pi$,
we know that
$\A(\pi_1), \A(\pi_2) \subseteq \A(\pi)$.
Using Lemma~\ref{lemma:Ifactors},
we have that $(I_A(\pi_1) \wedge I_A(\pi_2)) =
\bigwedge_{\sigma \in \A(\pi_1)}I_A(\sigma) \wedge
\bigwedge_{\sigma \in \A(\pi_2)}I_A(\sigma) $.

We can now see that both
$\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_B(\sigma)$
and
$\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_A(\sigma)$
are contained in $\psi$. From Lemma~\ref{lemma:str1}
we know that
\emph{(i)} $\psi \rightarrow \feq{\AU{\pi_1}{\pi_2}} $.
%
$I_A(\theta_A)$ and $I_B(\theta_A)$ are also contained in $\psi$,
therefore from Lemma~\ref{lemma:str1} we have that
\emph{(ii)} $\psi \rightarrow \feq{\theta_A}$.
%
From \emph{(i)} and \emph{(ii)} we see that
$\psi \rightarrow (\feq{\theta_A} \wedge \nfeq{\theta_A})$.
\end{proof}

\noindent
\textbf{Theorem~\ref{theorem:lis}.}
\emph{
Let $\sqsupseteq$ be a label strength operator such that $s \sqsupseteq
s$, $w \sqsupseteq w$ and $s \sqsupseteq w$.
%
Let $P = (A,B,G^C,\opath{xy},L)$ and $P' = (A,B,G^C,\opath{xy},L')$ be
two interpolation problems where $L$ and $L'$ are two labeling functions
such that $L(\sigma) \sqsupseteq L'(\sigma)$ for all the factors $\sigma$
of $G^C$.  Then $\Itp(P) \rightarrow \Itp(P')$.
}
\begin{proof}
If $(x \neq y) \in B$, we can either use $I_A$ or $\neg I_B$ to create an
interpolant for $A$. On the other hand, if $(x \neq y) \in A$, we can 
use either $I_A'$ or $\neg I_B$. Since only Eq.~(\ref{equation:lisI}) and
Eq.~(\ref{equation:lisS}) use labeling functions, we analyze only
those equations in this proof.

From Eq~(\ref{equation:lisI}) we can see that when a factor $\sigma$ 
has label $a$ a weakening step is applied, using $\neg I_B'(\sigma)$ instead of
$I_A(\sigma)$. Let $\Itp$ be the interpolant generated without weakening,
and $\Itp'$ the interpolant generated having applied the weakening step.
We know that $I_A(\sigma) \subseteq \Itp$ and $\neg I_B'(\sigma) \subseteq \Itp'$.
From Theorem~\ref{theorem:str1} we know that $I_A(\sigma) \rightarrow \neg I_B'(\sigma)$,
therefore we have that $\Itp \rightarrow \Itp'$.
%If no weakening swap is applied,
%$I$ from Eq.~(\ref{equation:lisI}) is equivalent to $I$ from \cite{Fuchs2009}.

Following the same reasoning, from Eq.~(\ref{equation:lisS}) we can see that
when a factor $\sigma$ has label $b$ a strengthening step is applied,
using $\neg I_A'(\sigma)$ instead of $I_B(\sigma)$. Let $\Itp$ be the interpolant generated
without strengthening, and $\Itp'$ the interpolant generated having applied this strengthening step.
We know that $\neg I_B(\sigma) \subseteq \Itp$ and $\neg \neg
I_A'(\sigma) \subseteq \Itp'$.
From Theorem~\ref{theorem:str1} we have that $I_A'(\sigma) \rightarrow \neg I_B(\sigma)$.
Therefore we have that $\Itp' \rightarrow \Itp$.
\end{proof}

\noindent
\textbf{Theorem~\ref{theorem:size}.}
\emph{
Let $P = (A,B,G^C,\pi,L)$.  If $\pi \in B$, $L = L_s$ leads to the
interpolant $Itp(P)$ that contains the smallest number of equalities, and if
$\pi \in A$, $L = L_w$ leads to the interpolant $Itp(P)$ that contains the
smallest number of equalities.
}
\begin{proof}
Consider the labeling function $L_s$ and the computation of $I_A(\pi)$. The usage of this
labeling function makes the formula be entirely computed by $I_A$, never
using $I_B$. Now let $I_A(\delta)$ be some arbitrary subcomputation of
$I_A(\pi)$. First, $\bigwedge_{\sigma \in \A(\delta)} J_A(\sigma)$ is computed.
From Eq.~\ref{equation:JA} we know that this formula contains the equality
$\feq{\sigma_A}$ for every $\sigma_A \in \A(\delta)$ and the equality $\feq{\sigma_B}$ for every $\sigma_B \in
\BA(\delta)$.
Suppose then that a factor $\gamma$ from $\BA(\delta)$ has label $w$, which
results in the computation of $\neg I_B'(\gamma)$. We know that $\gamma$ is
a $B$-factor because it came from $\BA(\delta)$, therefore \emph{(i)} $\theta_A = \gamma$;
and \emph{(ii)} $\B(\gamma) = \{\gamma\}$. From \emph{(i)} we have that
$I_B'(\gamma)$ computes $I_B(\gamma)$, and from \emph{(ii)} we have that $\B(\gamma) = \{\gamma\}$
and $J_B(\gamma)$ is then
computed. This reintroduces $\feq{\gamma}$ in the interpolant (as the implicated part
of $J_B(\gamma)$), since $I_A(\delta)$
already introduced it in the implicant part of some implication in
$\bigwedge_{\sigma \in \A(\delta)} J_A(\sigma)$. Notice that if $\gamma$ had label $s$ this
equality would not be reintroduced. The reasoning is symmetrical for the case where
$L_w$ is used for the computation of $I_B$.
Therefore we have that $I_A(\pi)$ and $I_B(\pi)$ contain exactly the same equalities,
with the difference being the side of the implication (implicant or implicated) that
an equality appears in (because of Eq.~\ref{equation:JA} and Eq.~\ref{equation:JB}).
We can also see that any other labeling function $L$ will introduce at least one
equality in the interpolant more than once (when it changes from $I_A$ to $\neg I_B'$
or from $I_B$ to $\neg I_A'$).

If $\pi \in B$, an interpolant can be computed by either $I_A(\pi)$ or $\neg I_B'(\pi)$.
The interpolant $I_A(\pi)$ has less equalities because it does not introduce the conflict in
the interpolant, as $\neg I_B'(\pi)$ does with the term $(\feq{\A(\pi_1) \cup \A(\pi_2)} \rightarrow \neg \feq{\theta_A})$.
Therefore, $L_s$ leads to the interpolant with the least number of equalities.

If $\pi \in A$, an interpolant can be computed by either $\neg I_B(\pi)$ or $I_A'(\pi)$.
Symmetrically, the interpolant $\neg I_B(\pi)$ has less equalities because it does not introduce the conflict in
the interpolant, as $I_A'(\pi)$ does with the term $(\feq{\B(\pi_1) \cup \B(\pi_2)} \rightarrow \neg \feq{\theta_B})$.
Therefore, $L_w$ leads to the interpolant with the least number of equalities.
\end{proof}
