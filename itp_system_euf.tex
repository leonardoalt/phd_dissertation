\section{The EUF-Interpolation System}
\label{section:euf_itp_system}
An EUF interpolation problem is a 5-Tuple $P = (A,B,G^C,\pi,L)$ where $A$ and $B$
are two sets of equalities and disequalities such that $A \cup B$ is
unsatisfiable, $G^C$ is a congruence graph with coloring $C$, $\pi$ is a path
$\overline{xy}$ in $G$, such that the disequality $(x \neq y)$ exists in $A
\cup B$, and $L$ is a labeling function. Our goal is to compute an interpolant
for $A$.
%
The two constant labeling functions $L_s(\pi) := s$ and $L_w(\pi) := w$
will be useful in the analysis.
%
We omit $A$, $B$, $G^C$ and $L$ when they are clear from the context,
referring to the interpolation problem as $\Itp(\pi)$.

The interpolation algorithms in~\cite{McMillan2005} and~\cite{Fuchs2009}
essentially compute an interpolant by collecting the $A$-factors that
prove $(x = y)$ in $G^C$.  To maintain the unsatisfiability with the $B$
part of the problem, the $A$ factors will then be implied by their
$B$-{\em premise set}.
%
A {\em premise set} for a color is the set of equalities of the opposite
color justifying the existence of a parent edge.  As stated in
Chapter~\ref{chapter:generic_interpolation}, it is also possible to compute a dual
interpolant for $A$ as the negation of an interpolant for $B$.
%
More technically, the $B$-premise set $\B$ for a path $\pi$ is
\begin{equation}
\label{equation:B}
    \B(\pi) := \begin{cases}
        \bigcup \{ \B(\sigma) | \sigma\text{ is a factor of }\pi \}, \text{if $\pi$ has $\ge$ 2 factors; }\\
        \{\pi\}, \text{if $\pi$ is a $B$-path; and}\\
        \bigcup \{ \B(\sigma) | \sigma\text{ is a parent of an edge of }\pi \},\\ \quad \text{if $\pi$ is an $A$-path.}
    \end{cases}
\end{equation}
%
To compute the dual interpolant we will need similarly to collect the
$B$-factors that prove $(x = y)$ in $G^C$, implied by their $A$-premise
set.  The $A$-premise set $\A$ for a path $\pi$ is defined as
%
\begin{equation}
\label{equation:A}
    \A(\pi) := \begin{cases}
        \bigcup \{ \A(\sigma) | \sigma\text{ is a factor of }\pi \}, \text{if $\pi$ has $\ge$ 2 factors;}\\
        \{\pi\}, \text{if $\pi$ is an $A$-path; and}\\
        \bigcup \{ \A(\sigma) | \sigma\text{ is a parent of an edge of }\pi \},\\ \quad \text{if $\pi$ is a $B$-path.}
    \end{cases}
\end{equation}

We extend the notation of $\A$ and $\B$ over a set $S$ of paths with a `syntactic
sugar'
%
$\A(S) := \bigcup_{\sigma \in S}\A(\sigma)$ and
%
$\B(S) := \bigcup_{\sigma \in S}\B(\sigma)$.
%
For any two operators $\OP, \OP'$ whose range contains this domain we
define the composite operator $\OP\OP'(\sigma) := \OP(\OP'(\sigma))$;
and define recursively $\OP^0(\sigma) := \sigma$, and $\OP^n :=
\OP(\OP^{n-1})$.

The functions $J_A$ and $J_B$ give, respectively, the contribution of an
individual $A$-factor and an individual $B$-factor to the interpolants.
\begin{equation}
\label{equation:JA}
    J_A(\pi) := \JI{\pi}
\end{equation}
\begin{equation}
\label{equation:JB}
    J_B(\pi) := \JS{\pi}
\end{equation}

Let $S$ be a set of paths. The notation $S|_c$ represents the subset of
$S$ containing the paths $\sigma$ such that $L(\sigma) = c$.
%
The number of paths in an interpolation problem is exponential in the
size of the problem, resulting in potential challenges in defining the
labeling function.  However this is not an issue since the interpolation
algorithm presented in this section almost always handles only factors
and therefore it suffices to label the linear number of
factors. The only path that is not necessarily a factor that needs to be
labeled is the path in the congruence graph that
contradicts an original disequality.
%
The $(\textrm{EUF}, L)$-algorithm for computing the EUF interpolant over
$A$ for a path $\opath{xy}$, $\Itp(P)$, where $P = (A,B,G^C,\pi,L)$, is
defined using four sub-procedures $I_A, I'_A, I_B$, and $I'_B$ that are
invoked depending on which partition the conflict $x\not=y$ lies and
what color the path has as:
\begin{equation}
\label{equation:Itp}
\Itp(P) :=
\begin{cases}
    I_A(\opath{xy}) & \text{if $(x \neq y) \in B$ and $L(\opath{xy}) = s$}, \\
    I_A'(\opath{xy}) & \text{if $(x \neq y) \in A$ and $L(\opath{xy}) = s$}, \\
    \neg I_B(\opath{xy}) & \text{if $(x \neq y) \in A$ and $L(\opath{xy}) = w$, and} \\
    \neg I_B'(\opath{xy}) & \text{if $(x \neq y) \in B$ and $L(\opath{xy}) = w$}.
\end{cases}
\end{equation}
%
The sub-procedures for $I_A$ and $I_B$ are defined as
\begin{equation}
\label{equation:lisI}
\begin{aligned}
    I_A(\pi) :=
        ( \bigwedge_{\sigma \in \A(\pi)}J_A(\sigma) ) \wedge
        ( \bigwedge_{\sigma \in \BA(\pi)|_s}I_A(\sigma) ) \wedge
        ( \bigwedge_{\sigma \in \BA(\pi)|_w}\neg I_B'(\sigma) )
\end{aligned}
\end{equation}
%
\begin{equation}
\label{equation:lisS}
\begin{aligned}
    I_B(\pi) :=
        ( \bigwedge_{\sigma \in \B(\pi)}J_B(\sigma) ) \wedge
        ( \bigwedge_{\sigma \in \AB(\pi)|_s}I_B(\sigma) ) \wedge
        ( \bigwedge_{\sigma \in \AB(\pi)|_w}\neg I_A'(\sigma) ).
\end{aligned}
\end{equation}
For the cases where the conflict $x\not= y \in A$ and $L(\opath{xy}) =
s$ and $x\not= y \in B$ and $L(\opath{xy}) = w$ the path $\opath{xy} =
\pi$ needs to be
%
decomposed for computing the partial interpolant as $\pi_1\theta_B\pi_2$
or $\pi_1\theta_A\pi_2$, where $\theta_C$ is the longest subpath of
$\pi$ with $c$-colorable endpoints.  Hence, $I_A'$ and $I_B'$ are
\begin{equation}
\label{equation:Iprime}
\begin{aligned}
    I_A'(\pi) := I_A(\theta_B) \wedge
    ( \bigwedge_{\sigma \in \BU{\pi_1}{\pi_2}}I_A(\sigma))
%    \bigwedge \{ I( \{ \sigma \} ) | \sigma \in \BU{\pi_1}{\pi_2} \}
\wedge
( \feq{\BU{\pi_1}{\pi_2}} \rightarrow \nfeq{\theta_B} ).
\end{aligned}
\end{equation}
\begin{equation}
\label{equation:Sprime}
\begin{aligned}
    I_B'(\pi) := I_B(\theta_A) \wedge
    ( \bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_B(\sigma) )
%    \bigwedge \{ S( \{ \sigma \} ) | \sigma \in \AU{\pi_1}{\pi_2} \}
\wedge
( \feq{\AU{\pi_1}{\pi_2}} \rightarrow \nfeq{\theta_A} ).
\end{aligned}
\end{equation}

\begin{theorem}
\label{theorem:correctness}
Given two sets of equalities and disequalities $A$ and $B$ such that $A
\cup B$ is unsatisfiable, a colored congruence graph $G^C$ containing a
path $\pi := \opath{xy}$ such that $(x \neq y) \in A \cup B$, and a
labeling function $L$, Eq.~(\ref{equation:Itp}) computes a valid
interpolant for $A$ using $L$ over $G^C$.
\end{theorem}
\begin{proof}
In order to analyze Eq.~(\ref{equation:Itp}) we have to analyze
Eq.~(\ref{equation:Iprime}), Eq.~(\ref{equation:Sprime}),
Eq.~(\ref{equation:lisI}) and Eq.~(\ref{equation:lisS}).

\emph{(i) $I'_A(\pi)$}\\
Eq.~(\ref{equation:Iprime}) is the same presented in \cite{Fuchs2009}, and
computes interpolants for $A$ when the disequality $(x \neq y)$ is in $A$.

\emph{(ii) $\neg I'_B(\pi)$}\\
Eq.~(\ref{equation:Sprime}) is the dual of Eq.~(\ref{equation:Iprime}), and
clearly computes interpolants for $B$ when the disequality is in $B$. We can
transform it into an interpolant for $A$ by negating it, as it is done in
Eq.~(\ref{equation:Itp}).

\emph{(iii) $I_A(\pi)$}\\
Eq.~(\ref{equation:lisI}) behaves similarly to the interpolation procedure
presented in \cite{Fuchs2009}, with the addition of dual interpolants
and labeling functions. First $J_I$ computes the individual contribution
of the $A$-factors that prove $\pi$ in $G^C$ ($\A(\pi)$) to the interpolant,
and then conjoins it with the interpolants of their $B$-premise sets ($\BA(\pi)$).

\emph{(iv) $\neg I_B(\pi)$}\\
Eq.~(\ref{equation:lisS}) is the dual of Eq.~(\ref{equation:lisI}) and computes
an interpolant for $B$, which can be transformed into an interpolant for $A$ by
negating it.

\iffalse
With respect to Eqs.~(\ref{equation:lisI}) and~(\ref{equation:lisS}), we have two cases:
\begin{paragraph}{Case 1} the used labeling function simply returns $s$
for any given path.\\
In this case, 
the resulting interpolants are equivalent to the ones given in \cite{Fuchs2009}
(referred as $I$) when the used labeling function simply returns $s$ for
any given path, which disables the use of dual interpolants.
%
The change of notation from $I$ to $I_A$ is merely a tool to make the proofs more readable.
%
As $I$ does, $I_A$ also computes $J_A$ for the $A$-factors that prove $\pi$.
%
The only difference is that $I$ is called over both $A$ and $B$-factors, and
decides what to do based on whether the argument consists of multiple factors,
a single $A$-factor or a single $B$-factor, whereas $I_A$'s argument is either a
path potentially containing multiple factors (fist call) or a single $B$-factor
that is part of the $B$-premise set previously used.
%
%The new notation for $I_A'$ is simply an adaptation to the new notation of $I$.
%
Notice that regardless the notation both procedures generate the same
interpolant for a given problem.
%
Eq.~(\ref{equation:lisS}) is analogous to Eq.~(\ref{equation:lisI}),
and computes interpolants for $B$.
\end{paragraph}

\begin{paragraph}{Case 2} the labeling function is non trivial.\\
In~\cite{Fuchs2009}, it is shown that combining valid partial interpolants with
a simple conjunction is a valid procedure. We reuse this combination procedure
here, and together with the duality idea presented in Section~\ref{subsection:generic_itp}
we have showed that the equations above fit
the template given in Algorithm~\ref{algorithm:generic_itp} making use
of non trivial labeling functions.
\end{paragraph}
\fi
\end{proof}


The EUF-interpolation system of Eq.~(\ref{equation:Itp}) is an instance
of the interpolation system template.  The congruence graph represents
the dag-like structure using factors as dag nodes and a labeling
function as presented in Chapter~\ref{chapter:generic_interpolation}.  Using
Eq.~(\ref{equation:lisI}) and Eq.~(\ref{equation:lisS}), we have that
$\Itp_L^A := J_A$ and $\Itp_L^B := J_B$, since leaf nodes represent
factors in a congruence graph that have empty premise sets.
%
Finally we have that $\Itp_C^A := I_A$ (or $I_A'$ if $A$ contains
the disequality that the congruence graph contradicts)
%\func{CombineChildrenItps} \equiv I_A$
and $\Itp_C^B := I_B$ (or $I_B'$ if $B$ contains the disequality
that the congruence graph contradicts).
%$\func{CombineChildrenItpsDual} \equiv I_B$.

Notice that in Eq.~\ref{equation:Itp}, Eq.~\ref{equation:lisI}
and Eq.~\ref{equation:Iprime}, if $L_s$ is used as the labeling function, only
$I_A$ and $I_A'$ are used. This makes the algorithm collect only $A$-factors
and their premises for the interpolant, which is the same approach as in
\cite{Fuchs2009}.  This shows that the interpolation algorithm
from~\cite{Fuchs2009} is an instance of the EUF-interpolation system
represented by the labeling function $L_s$.
%
The following example shows how Eq.~(\ref{equation:Itp}) can be used to compute
the interpolants from~\cite{Fuchs2009} using $L_s$.
\begin{example}
\label{example:itpalg}
Let $A := \{ (x_1 = f(x_2)), (f(x_3) = x_4), (x_4 = f(x_5)), (f(x_6) =
x_7) \}$ and $B := \{ (x_2 = x_3), (x_5 = x_6), (x_1 \neq x_7) \}$.
Fig.~\ref{figure:small_example} shows a possible congruence graph $G^C$
that proves the joint unsatisfiability of $A$ and $B$ (by proving $(x_1 = x_7)$
such that $(x_1 \neq x_7) \in A \cup B$) and its tree
representation, with each node annotated by its partial interpolant.
Notice that the labeling function $L_s$ used in $G^C$ labels all the factors
as $s$.
From Eq.~(\ref{equation:Itp}) we have that
$Itp(\opath{x_1x_7}) = I_A(\opath{x_1x_7})$, because $L_s(\opath{x_1x_7}) = s$
and $(x_1 \neq x_7) \in B$.
%
The call to $I_A(\opath{x_1x_7})$ is represented by the root node in the tree
in Fig.~\ref{figure:small_example}.  First we compute $\A(\opath{x_1x_7}) =
\{\opath{x_1x_7}\}$ and $\BA(\opath{x_1x_7}) =
\{\opath{x_2x_3},\opath{x_5x_6}\}$.  Then from Eq.~(\ref{equation:lisI}) we
have that $I_A(\opath{x_1x_7}) = J_A(\opath{x_1x_7}) \wedge I_A(\opath{x_2x_3})
\wedge I_A(\opath{x_5x_6})$.  The calls to $I_A(\opath{x_2x_3})$ and
$I_A(\opath{x_5x_6})$ are represented by the edges from the root to the leaf nodes in the tree in
Fig.~\ref{figure:small_example}.  We then proceed computing
$\A(\opath{x_2x_3}) = \emptyset$ and $\BA(\opath{x_2x_3}) = \emptyset$ which
lead to $I_A(\opath{x_2x_3}) = \top$; and $\A(\opath{x_5x_6}) = \emptyset$ and
$\BA(\opath{x_5x_6}) = \emptyset$ which lead to $I_A(\opath{x_5x_6}) = \top$
(the partial interpolants of the leaf nodes).  Finally we have that
$I_A(\opath{x_1x_7}) = ((x_2 = x_3) \wedge (x_5 = x_6)) \rightarrow (x_1 =
x_7)$ is the partial interpolant of the root node, representing the final
interpolant for $A$.
\end{example}

\begin{figure}[tb]
\centering
\includegraphics[scale=0.5]{figures/small_example_xfig}
\includegraphics[scale=0.5]{figures/generic_itp_xfig}
\includegraphics[scale=0.5]{figures/small_example_tree_xfig}
\caption{Computing partial interpolants for the EUF-interpolation
system.  The bottom left dag illustrates the computation in terms of the
dag-like interpolation algorithm.}
\label{figure:small_example}
\end{figure}



\subsection{The Strength}

Let $P = (A,B,G^C,\pi,L_s)$ and $P' = (A,B,G^C,\pi,L_w)$ be two
interpolation problems differing only in the labeling function.  We will
show in Theorem~\ref{theorem:str1} that $\Itp(P) \rightarrow \Itp(P')$, and
then in Example~\ref{example:labeling} that there are cases where the
strength relation is strict in the sense that there are models that
satisfy $\Itp(P')$ but do not satisfy $\Itp(P)$.
Theorem~\ref{theorem:str1} needs Lemma~\ref{lemma:multiple} which in
turn is a generalization of Lemma~\ref{lemma:str1}.
%
We then show our main result on EUF in Theorem~\ref{theorem:lis} that
the new interpolation procedure for EUF presented here meets all the
requirements described in Chapter~\ref{chapter:generic_interpolation}, that is, we
provide a way to compare the strength of interpolants based on the
labeling functions used.

\begin{definition}
\label{def:relevant}
Let $G$ be a congruence graph, and
$\sigma$ an arbitrary factor from $G$. We say that
$\sigma$ is \emph{relevant} to $\omega$ if
either $J_A(\sigma)$ or $J_B(\sigma)$ is called during
the computation of $I_A(\omega)$ or $I_B(\omega)$.
\end{definition}    

\begin{lemma}
\label{lemma:str1}
Let $G^C$ be a congruence graph with coloring $C$, and $\omega$ a factor from $G$.
Then $I_A(\omega) \wedge I_B(\omega) \rightarrow \feq{\omega}$.
\end{lemma}
\begin{proof}
Let $R^{\omega}$ be the set of factors relevant (Def.~\ref{def:relevant})
to $\omega$ in a congruence graph, $R^{\omega}_A$ the subset of
$R^{\omega}$ containing only $A$-factors and $R^{\omega}_B$ the subset of
$R^{\omega}$ containing only $B$-factors.
%
%By definition of relevant factors, $L_s$, $L_w$, Eq.~(\ref{equation:lisI}), and
%Eq.~(\ref{equation:lisS}), we have that $I_A(\omega) = \bigwedge_{r \in
%R^{\omega}_A}J_A(r)$ and $I_B(\omega) = \bigwedge_{r \in R^{\omega}_B}J_B(r)$.

From Eq.~(\ref{equation:lisI}) we can clearly see that
\begin{equation}
\label{equation:RIinf}
R^{\omega}_A := \A(\omega) \cup \A(\BA(\omega)) \cup \ldots \cup
\A((\BA)^k(\omega)) \cup \ldots,
\end{equation}
and from Eq.~(\ref{equation:lisS}) that
\begin{equation}
\label{equation:RSinf}
R^{\omega}_B := \B(\omega) \cup \B(\AB(\omega)) \cup \ldots \cup
\B((\AB)^k(\omega)) \cup \ldots.
\end{equation}

Because congruence graphs are acyclic and finite, for any $\omega$
there exists an integer $n$ such that $\A((\BA)^n(\omega)) = \emptyset$
and $\B((\AB)^n(\omega)) = \emptyset$,
which allows us to rewrite the previous equations as
\begin{equation}
\label{equation:RI}
R^{\omega}_A := \A(\omega) \cup \A(\BA(\omega)) \cup \ldots \cup
\A((\BA)^n(\omega)),
\end{equation}
\begin{equation}
\label{equation:RS}
R^{\omega}_B := \B(\omega) \cup \B(\AB(\omega)) \cup \ldots \cup
\B((\AB)^n(\omega)).
\end{equation}

If $\omega$ is an $A$-factor, we have that $\A(\omega) = \{\omega\}$
and therefore we can write $\B(\omega) \equiv \BA(\omega)$. Using that
we can infer that $\A((\BA)^{n+1}(\omega)) = \emptyset$ (by the definition
of $n$), and we can change
Eq.~(\ref{equation:RS}) to
\begin{equation}
\label{equation:RSchanged}
R^{\omega}_B := \BA(\omega) \cup (\BA)^2(\omega)) \cup \ldots \cup
(\BA)^{n+1}(\omega).
\end{equation}
We follow the proof assuming that $\omega$ is an $A$-factor,
using Eq.~(\ref{equation:RI}) and Eq.~(\ref{equation:RSchanged})
to represent $R_A^\omega$ and $R_B^\omega$ respectively,
and then argue that the proof is symmetrical for the case
where $\omega$ is a $B$-factor.

%Notice that $(\BA)^n(\omega)$ (from $R^{\omega}_S$)
%could already have an empty premise set, in which case
%$\A((\BA)^n(\omega))$ would not be necessary.
%We decide to use $\A((\BA)^n(\omega))$ as the first
%to have an empty premise set (meaning that $(\BA)^{n+1}(\omega) = \emptyset$)
%for simplicity reasons,
%and it is still valid because $\B(\emptyset) = \emptyset$.

From Eq.~(\ref{equation:RI}) and Eq.~(\ref{equation:RSchanged}), we can then see that
\begin{equation}
\begin{aligned}
R^{\omega} :=
%R^{\omega}_A \cup R^{\omega}_B =
\A(\omega)
\cup \BA(\omega) \cup \A(\BA(\omega))
\cup \ldots \cup
\A((\BA)^n(\omega)) \cup (\BA)^{n+1}(\omega)
\end{aligned}
\end{equation}
and therefore
\begin{equation}
I_A(\omega) \wedge I_B(\omega) = 
( \bigwedge_{\sigma \in R^{\omega}_A}J_A(\sigma) )
\wedge
( \bigwedge_{\sigma \in R^{\omega}_B}J_B(\sigma) ).
\end{equation}
When $J_A$ and $J_B$ are computed over a set
that has an empty premise set, the result is
not a conjunction of implications, but a conjunction of equalities.
In this case,
$J_B((\BA)^{n+1}(\omega)) = \feq{(\BA)^{n+1}(\omega)}$
because $\A((\BA)^{n+1}(\omega)) = \emptyset$.
Thus, after applying the functions $J_A$ and $J_B$ we have that
\begin{equation}
\label{equation:ISeq}
\begin{array}{ll}
I_A(\omega) \wedge I_B(\omega) =
 &
\left(
 \bigwedge_{i=0}^n
 \bigwedge_{\sigma \in \A((\BA)^i(\omega))}
         ( \JI{\sigma} )
\right) \\
%\wedge \feq{\A((\BA)^n(\omega))} \wedge \\
 &
 \wedge
\left(
\bigwedge_{i=0}^{n}
\bigwedge_{\sigma \in (\BA)^i(\omega)}
        ( \JS{\sigma} )
\right)\\ &
\wedge \feq{(\BA)^{n+1}(\omega)}.
\end{array}
\end{equation}
We know that formulas of the form
$((a_1 \rightarrow b_1) \wedge \ldots \wedge (a_n \rightarrow b_n))
\rightarrow
( (\bigwedge_{i \in 1..n}a_i) \rightarrow (\bigwedge_{i \in 1..n}b_i) )$
are tautologies. Using that and Eq.~(\ref{equation:ISeq}), we can then show that
\begin{equation}
\label{equation:ISimply}
\begin{array}{ll}
I_A(\omega) \wedge I_B(\omega) & \rightarrow
    \left( \bigwedge_{i=0}^n ( \feq{(\BA)^{i+1}(\omega)} \rightarrow
    \feq{\A((\BA)^i(\omega))} ) \right) \\
%    ( \feq{\BA(\omega)} \rightarrow \feq{\A(\omega)} )
%\wedge \feq{\A((\BA)^n(\omega))} \wedge \\
& \wedge
\left( \bigwedge_{i=0}^{n} ( \feq{\A((\BA)^i(\omega))} \rightarrow
\feq{(\BA)^i(\omega)} ) \right) \\&
\wedge \feq{(\BA)^{n+1}(\omega)}
\end{array}
\end{equation}

Because $\feq{(\BA)^{n+1}(\omega)}$ has no
implicant, it has to be true in the formula, starting a chain that
satisfies the antecedents of all the implications in Eq.~(\ref{equation:ISimply}),
since $(\BA)^{n+1}(\omega)$ is the premise set
of $\A((\BA)^n(\omega))$, which is the premise set of $(\BA)^n(\omega)$
and so on.
Because of that, we can simplify the formula to
\begin{equation}
I_A(\omega) \wedge I_B(\omega) \rightarrow\\
\left( \bigwedge_{i=0}^{n} \feq{\A((\BA)^i(\omega))} \right)
\wedge
\left( \bigwedge_{i=0}^{n+1} \feq{(\BA)^i(\omega)} \right).
\end{equation}

Therefore, we have that
\begin{equation}
\label{equation:imply}
\forall r \in R^{\omega} . (I_A(\omega) \wedge
I_B(\omega)) \rightarrow \feq{r}.
\end{equation}

For the case where $\omega$ is a $B$-factor, we have that
$\B(\omega) = \{\omega\}$, which implies that $\AB(\omega) \equiv \A(\omega)$.
Using that, we can infer that $\B((\AB)^{n+1}(\omega)) = \emptyset$
and we can also change Eq.~(\ref{equation:RI}) to
\begin{equation}
\label{equation:RIchanged}
    R^{\omega}_A := \AB(\omega) \cup (\AB)^2(\omega) \cup \ldots \cup
(\AB)^n(\omega).
\end{equation}
Using Eq.~(\ref{equation:RIchanged}) and Eq.~(\ref{equation:RS}) to
represent $R_A^\omega$ and $R_B^\omega$ respectively, the same reasoning
is followed and we can show that Eq.~(\ref{equation:imply}) holds
also if $\omega$ is a $B$-factor.

If $\omega$ is an $A$-factor, then $\A(\omega) = \{\omega\}$
and $J_A$ is called for $\omega$ in the first iteration of
Eq.~(\ref{equation:lisI}). On the other hand, if $\omega$ is
a $B$-factor, then $\B(\omega) = \{\omega\}$ and
$J_B$ is called for $\omega$ in the first iteration of
Eq.~(\ref{equation:lisS}).
%In particular $\omega \in
%R^{\omega}$, since $\omega$ is an $A$-factor by the assumption of
%Lemma~\ref{lemma:factor} and therefore $\A(\omega) = \{\omega\}$
%implying $J_I$ is called for $\omega$ in the first iteration of
%Eq.~\ref{equation:I}. 
This shows that $\omega$ is a relevant factor
and by Eq.~(\ref{equation:imply}) we conclude the proof.
% the case where
%$\omega$ is an A-factor.  The proof is symmetrical to the case
%when $\omega$ is a $B$-factor.
%$\qed$
\end{proof}


\iffalse
\begin{lemma}
\label{lemma:relevants}
Let $P = (A,B,G^C,\pi,L)$. For every possible labeling function $L$, the set of
distinct equalities contained in $Itp(P)$ is exactly $\feq(R^\pi_A \cup R^\pi_B)$.
\end{lemma}
\begin{proof}
From the development of Lemma~\ref{lemma:str1} we have that $I_A(\pi) =
\bigwedge_{\sigma \in R^\pi_A} J_A(\sigma))$ when using $L_s$, and $I_B(\pi) =
\bigwedge_{\sigma \in R^\pi_B} J_B(\sigma))$ when using $L_w$.  From
Lemma~\ref{lemma:str1} we can also see that for every
$\sigma \in R^\pi_A$, either $\B(\sigma) = \emptyset$ or $\B(\sigma) \in R^\pi_B$;
and for every $\sigma \in R^\pi_B$,
either $\A(\sigma) = \emptyset$ or $\A(\sigma) \in R^\pi_A$.
Now consider an arbitrary labeling function $L$ different from $L_s$ and $L_w$.
Let $\sigma$ be a factor from $G^C$ and $I_A(\sigma)$ part of the computation
of $Itp(P)$. 
\end{proof}
\fi

\begin{lemma}
\label{lemma:Ifactors}
Let $\pi$ be an arbitrary path in the congruence graph, and $\facts(\pi)$ the
set of all factors in $\pi$.  Then
$I_A(\pi) = \bigwedge_{\sigma \in \facts(\pi)}I_A(\sigma)$ and
$I_B(\pi) = \bigwedge_{\sigma \in \facts(\pi)}I_B(\sigma)$.
\end{lemma}
\begin{proof}
By the definition of $\A$ in Eq.~(\ref{equation:A}), we know that
$\A(\pi) = \bigcup_{\sigma \in \facts(\pi)}\A(\sigma)$.
By the definition of $I_A$, we can see that $J_A$ is computed
individually for each element of $\A(\pi)$ in $I_A(\pi)$,
and $I_A$ is called recursively for the $B$-premise sets
of each individual element of $\A(\pi)$. Therefore,
$\bigwedge_{\sigma \in \facts(\pi)}I_A(\sigma) = 
\bigwedge_{\sigma \in \A(\pi)}I_A(\sigma)$, which has the same effect of $I_A(\pi)$.
The result is analogous for $I_B(\pi)$.
\end{proof}

%\begin{proof}
%See Appendix~\ref{appendix:correctness}.
%$\qed$
%\end{proof}

\begin{lemma}
\label{lemma:multiple}
Lemma~\ref{lemma:str1} holds when $\omega$ is a path containing multiple factors.
\end{lemma}
\begin{proof}
Let $\omega$ be a path built by multiple factors and
$\facts(\omega)$ the set containing these factors.
By Lemma~\ref{lemma:Ifactors} we know that
$I_A(\omega) = \bigwedge_{\sigma \in \facts(\omega)}I_A(\sigma)$ and
$I_B(\omega) = \bigwedge_{\sigma \in \facts(\omega)}I_B(\sigma)$;
by Lemma~\ref{lemma:str1} we know that
$\forall \sigma \in \facts(\omega) . (I_A(\sigma) \wedge I_B(\sigma)) \rightarrow \feq{\sigma}$.
Because the elements of $\facts(\omega)$ are factors linking nodes to prove $\omega$,
we know that $(\bigwedge_{\sigma \in \facts(\omega)}\feq{\sigma}) \rightarrow \feq{\omega}$.
Therefore we have that $(I_A(\omega) \wedge I_B(\omega)) \rightarrow \feq{\omega}$.
\end{proof}

%\begin{proof}
%See Appendix~\ref{appendix:correctness}.
%$\qed$
%\end{proof}

\begin{theorem}
\label{theorem:str1}
For fixed $A, B, G^C,$ and $\opath{xy}$, for the corresponding interpolants
defined in Eq.~(\ref{equation:Itp}) it holds that
$\Itp(A,B,G^C,\opath{xy},L_s)
\rightarrow \Itp(A,B,G^C,\opath{xy},L_w)$.
\end{theorem}
\begin{proof}
We only consider the case where $(x \not= y) \in B$ and note that the
case where $(x \not= y) \in A$ is completely symmetrical.
%
Let $\pi := \opath{xy}$ and $\psi = I_A(\pi) \wedge I_B'(\pi)$.  By
Eq.~(\ref{equation:Sprime}) we have that
\begin{equation}
\label{psi}
\begin{aligned}
\psi =
I_B(\theta_A) \wedge
%\left(
\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_B(\sigma)
%\right)
%    \bigwedge \{ S( \{ \sigma \} ) | \sigma \in \AU{\pi_1}{\pi_2} \}
\wedge
\left( \feq{\AU{\pi_1}{\pi_2}} \rightarrow \nfeq{\theta_A} \right)
\wedge
%( \bigwedge_{\sigma \in \A(S_\pi)}J_I(\sigma) ) \wedge
%I(\BA(\{\pi\}))
I_A(\pi),
\end{aligned}
\end{equation}
where $\pi$ is decomposed as $\pi_1\theta_A\pi_2$,
and $\theta_A$ is the largest subpath of $\pi$ with
$A$-colorable endpoints.
In order to show that $I_A(\pi) \rightarrow \neg I_B'(\pi)$,
we prove that $\psi \rightarrow \bot$, which leads to
the theorem.
In $I_B'(\pi)$, $\pi$ is split into $\pi_1 \theta_A \pi_2$.
From the definition of $\theta_A$, we know that $\pi_1$
and $\pi_2$ are $B$-factors. Therefore, using Lemma~\ref{lemma:Ifactors},
we can say that
$I_A(\pi) = I_A(\pi_1) \wedge I_A(\theta_A) \wedge I_A(\pi_2)$.
Because $\pi_1$ and $\pi_2$ are subpaths of $\pi$,
we know that
$\A(\pi_1), \A(\pi_2) \subseteq \A(\pi)$.
Using Lemma~\ref{lemma:Ifactors},
we have that $(I_A(\pi_1) \wedge I_A(\pi_2)) =
\bigwedge_{\sigma \in \A(\pi_1)}I_A(\sigma) \wedge
\bigwedge_{\sigma \in \A(\pi_2)}I_A(\sigma) $.

We can now see that both
$\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_B(\sigma)$
and
$\bigwedge_{\sigma \in \AU{\pi_1}{\pi_2}}I_A(\sigma)$
are contained in $\psi$. From Lemma~\ref{lemma:str1}
we know that
\emph{(i)} $\psi \rightarrow \feq{\AU{\pi_1}{\pi_2}} $.
%
$I_A(\theta_A)$ and $I_B(\theta_A)$ are also contained in $\psi$,
therefore from Lemma~\ref{lemma:str1} we have that
\emph{(ii)} $\psi \rightarrow \feq{\theta_A}$.
%
From \emph{(i)} and \emph{(ii)} we see that
$\psi \rightarrow (\feq{\theta_A} \wedge \nfeq{\theta_A})$.
\end{proof}

%\begin{proof}
%See Appendix~\ref{appendix:correctness}.
%$\qed$
%\end{proof}

Theorem~\ref{theorem:str1} proves one of the requirements of Theorem~\ref{theorem:template}, which is that $Itp_L^A \rightarrow Itp_L^B$, that is, the interpolation 
algorithm that generates conventional interpolants implies the interpolation
algorithm that generates dual interpolants. As described earlier in this section,
these two algorithms are instances of the EUF-interpolation system when using,
respectively, labeling functions $L_s$ and $L_w$.

To show that the implication is not trivial in general, we show by
example that three different labeling functions being
applied to the congruence graph from Example~\ref{example:preliminaries}
result in three interpolants which do not share models pairwise.

\begin{example}
\label{example:labeling}
Consider again the sets $A$ and $B$ and the congruence graph $G^C$
from Example~\ref{example:preliminaries} and Fig.~\ref{figure:example}.
Let $L_c$ a custom labeling function mapping
the paths to labels as
$\{
\opath{x_1x_2}:s, \opath{x_1v_1}:s, \opath{v_1v_2}:s, \opath{v_2x_2}:s,
\opath{y_1t_1}:w, \opath{t_1t_2}:w, \opath{t_2y_2}:w, \opath{z_1s_1}:w,
\opath{s_1s_2}:w, \opath{s_2z_2}:w, \opath{r_1u_1}:w, \opath{u_1u_2}:w, \opath{u_2r_2}:w
\}$.
We recall that the labeling function only needs to be defined on the
factors and the path that contradicts the original disequality in $A
\cup B$, in this case $\opath{x_1x_2}$.
%
The labels are shown over curves representing which path is being labeled. The custom
labeling function $L_c$ represents the intent of generating stronger partial interpolants
closer to $(x_1 = x_2)$, and weaker partial interpolants in the inner explanations.
%
Let $\Itp_s$ and $\Itp_w$ be, respectively, the interpolants generated
by Eq.~(\ref{equation:Itp}) using $L_s$ and $L_w$.
%
The computed interpolants are
$\Itp_s = ((t_1 = t_2) \rightarrow (v_1 = v_2)) 
\wedge ((u_1 = u_2) \rightarrow (s_1 = s_2))$ and
$\Itp_w = \neg((u_1 = u_2) \wedge ((s_1 = s_2) \rightarrow (t_1 = t_2))
\wedge \neg(v_1 = v_2))$.

To show how the EUF-interpolation system uses the duality of interpolants,
Fig.~\ref{figure:custom_label_euf} shows the tree of factors that represent the
congruence graph used in this example, labeled by $L_c$.  Notice that at every
node, the label controls if an interpolant for $A$ or $B$ (dual) should be
derived for that node.  Let $\Itp_c$ be the interpolant generated using $L_c$.
Because the disequality $(x_1 \neq x_2)$ is in $B$ and the root node, that
represents the equality $(v_1 = v_2)$, has label $s$, we have that $\Itp_c =
I_A(\opath{v_1v_2})$. The function $I_A$, at this point, computes
$J_I(\opath{v_1v_2})$ and moves to the dual interpolation algorithm for the
node that represents $(t_1 = t_2)$, since its label is $w$, by computing $\neg
I_B'(\opath{t_1t_2})$. The labels of the following nodes are also $w$, so the
dual interpolation algorithm is used again. Therefore, we have that
$\Itp_c = ((t_1 = t_2) \rightarrow (v_1 = v_2)) \wedge
\neg(((s_1 = s_2) \rightarrow (t_1 = t_2)) \wedge (u_1 = u_2)
\wedge \neg(t_1 = t_2))$. 

We also have that $\Itp_s \rightarrow \Itp_c \rightarrow
\Itp_w$, and none of them is equivalent to another.
%
%A more detailed example on the computation with $L_s$ is in
%Appendix~\ref{appendix:example}.
\iffalse
\begin{figure}
\centering
\includegraphics[scale=0.3]{figures/example_labeled}
\caption{Congruence graph labeled by $L_c$}
\label{figure:labeled_cgraph}
\end{figure}
\fi

\end{example}


\begin{figure}
\centering
\includegraphics[scale=0.3]{figures/custom_label_euf}
\caption{Tree of factors that represents the congruence graph from Example~\ref{example:labeling}.}
\label{figure:custom_label_euf}
\end{figure}

Finally our main result is presented providing a way to partially order
interpolants into a lattice based on their strength only by looking at
the labeling function used. As a result it follows that the constant
labeling functions $L_s$ and $L_w$ give, respectively, the strongest and
the weakest interpolants within this framework.
\begin{theorem}
\label{theorem:lis}
Let $\sqsupseteq$ be a label strength operator such that $s \sqsupseteq
s$, $w \sqsupseteq w$ and $s \sqsupseteq w$.
%
Let $P = (A,B,G^C,\opath{xy},L)$ and $P' = (A,B,G^C,\opath{xy},L')$ be
two interpolation problems where $L$ and $L'$ are two labeling functions
such that $L(\sigma) \sqsupseteq L'(\sigma)$ for all the factors $\sigma$
of $G^C$.  Then $\Itp(P) \rightarrow \Itp(P')$.
\end{theorem}
\begin{proof}
If $(x \neq y) \in B$, we can either use $I_A$ or $\neg I_B$ to create an
interpolant for $A$. On the other hand, if $(x \neq y) \in A$, we can 
use either $I_A'$ or $\neg I_B$. Since only Eq.~(\ref{equation:lisI}) and
Eq.~(\ref{equation:lisS}) use labeling functions, we analyze only
those equations in this proof.

From Eq~(\ref{equation:lisI}) we can see that when a factor $\sigma$ 
has label $a$ a weakening step is applied, using $\neg I_B'(\sigma)$ instead of
$I_A(\sigma)$. Let $\Itp$ be the interpolant generated without weakening,
and $\Itp'$ the interpolant generated having applied the weakening step.
We know that $I_A(\sigma) \subseteq \Itp$ and $\neg I_B'(\sigma) \subseteq \Itp'$.
From Theorem~\ref{theorem:str1} we know that $I_A(\sigma) \rightarrow \neg I_B'(\sigma)$,
therefore we have that $\Itp \rightarrow \Itp'$.
%If no weakening swap is applied,
%$I$ from Eq.~(\ref{equation:lisI}) is equivalent to $I$ from \cite{Fuchs2009}.

Following the same reasoning, from Eq.~(\ref{equation:lisS}) we can see that
when a factor $\sigma$ has label $b$ a strengthening step is applied,
using $\neg I_A'(\sigma)$ instead of $I_B(\sigma)$. Let $\Itp$ be the interpolant generated
without strengthening, and $\Itp'$ the interpolant generated having applied this strengthening step.
We know that $\neg I_B(\sigma) \subseteq \Itp$ and $\neg \neg
I_A'(\sigma) \subseteq \Itp'$.
From Theorem~\ref{theorem:str1} we have that $I_A'(\sigma) \rightarrow \neg I_B(\sigma)$.
Therefore we have that $\Itp' \rightarrow \Itp$.
\end{proof}

%\begin{proof}
%See Appendix~\ref{appendix:correctness}.
%\end{proof}

Theorem~\ref{theorem:lis} shows that by using labeling functions $s$ and $w$
and Theorem~\ref{theorem:str1}, the second and final requirement of
Theorem~\ref{theorem:template} is met, and therefore the strength of EUF
interpolants can be controlled by the labeling functions.

\subsection{Labeling Functions}
The EUF-interpolation system presented above introduces a way of computing interpolants
of different strength by labeling the factors of a congruence graph as $s$ or $w$,
depending on the required strength. Each labeling function leads to a potential
new interpolant, and creating meaningful labeling functions is a very hard task
on its own.  In this paper we restrict ourselves to the study of the two
extreme labeling functions with respect to strength, $L_s$ and $L_w$, and prove
the following important result related to the size of interpolants.

\begin{theorem}
\label{theorem:size}
Let $P = (A,B,G^C,\pi,L)$.  If $\pi \in B$, $L = L_s$ leads to the
interpolant $Itp(P)$ that contains the smallest number of equalities, and if
$\pi \in A$, $L = L_w$ leads to the interpolant $Itp(P)$ that contains the
smallest number of equalities.
\end{theorem}
\begin{proof}
Consider the labeling function $L_s$ and the computation of $I_A(\pi)$. The usage of this
labeling function makes the formula be entirely computed by $I_A$, never
using $I_B$. Now let $I_A(\delta)$ be some arbitrary subcomputation of
$I_A(\pi)$. First, $\bigwedge_{\sigma \in \A(\delta)} J_A(\sigma)$ is computed.
From Eq.~\ref{equation:JA} we know that this formula contains the equality
$\feq{\sigma_A}$ for every $\sigma_A \in \A(\delta)$ and the equality $\feq{\sigma_B}$ for every $\sigma_B \in
\BA(\delta)$.
Suppose then that a factor $\gamma$ from $\BA(\delta)$ has label $w$, which
results in the computation of $\neg I_B'(\gamma)$. We know that $\gamma$ is
a $B$-factor because it came from $\BA(\delta)$, therefore \emph{(i)} $\theta_A = \gamma$;
and \emph{(ii)} $\B(\gamma) = \{\gamma\}$. From \emph{(i)} we have that
$I_B'(\gamma)$ computes $I_B(\gamma)$, and from \emph{(ii)} we have that $\B(\gamma) = \{\gamma\}$
and $J_B(\gamma)$ is then
computed. This reintroduces $\feq{\gamma}$ in the interpolant (as the implicated part
of $J_B(\gamma)$), since $I_A(\delta)$
already introduced it in the implicant part of some implication in
$\bigwedge_{\sigma \in \A(\delta)} J_A(\sigma)$. Notice that if $\gamma$ had label $s$ this
equality would not be reintroduced. The reasoning is symmetrical for the case where
$L_w$ is used for the computation of $I_B$.
Therefore we have that $I_A(\pi)$ and $I_B(\pi)$ contain exactly the same equalities,
with the difference being the side of the implication (implicant or implicated) that
an equality appears in (because of Eq.~\ref{equation:JA} and Eq.~\ref{equation:JB}).
We can also see that any other labeling function $L$ will introduce at least one
equality in the interpolant more than once (when it changes from $I_A$ to $\neg I_B'$
or from $I_B$ to $\neg I_A'$).

If $\pi \in B$, an interpolant can be computed by either $I_A(\pi)$ or $\neg I_B'(\pi)$.
The interpolant $I_A(\pi)$ has less equalities because it does not introduce the conflict in
the interpolant, as $\neg I_B'(\pi)$ does with the term $(\feq{\A(\pi_1) \cup \A(\pi_2)} \rightarrow \neg \feq{\theta_A})$.
Therefore, $L_s$ leads to the interpolant with the least number of equalities.

If $\pi \in A$, an interpolant can be computed by either $\neg I_B(\pi)$ or $I_A'(\pi)$.
Symmetrically, the interpolant $\neg I_B(\pi)$ has less equalities because it does not introduce the conflict in
the interpolant, as $I_A'(\pi)$ does with the term $(\feq{\B(\pi_1) \cup \B(\pi_2)} \rightarrow \neg \feq{\theta_B})$.
Therefore, $L_w$ leads to the interpolant with the least number of equalities.
\end{proof}
%\begin{proof}
%See Appendix~\ref{appendix:correctness}.
%\end{proof}

