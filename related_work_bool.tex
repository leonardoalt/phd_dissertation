\section{Related work}
\label{section:related_work_bool}
This section describes interpolation algorithms related to propositional logic.
For work related to other theories please see
Sections~\ref{section:related_work_template}, ~\ref{section:related_work_euf},
and~\ref{section:related_work_lra}.

%There is a wide body of research on propositional interpolation with
%respect to size~\cite{Alt2016,Bloem2014,Vizel2013},
%strength~\cite{DSilva2010b,Rollini2012,Rollini2013}, and
%optimization~\cite{Cabodi2015,Ruemmer2013}.  This paper contributes to
%the field by addressing interpolation on SMT theories.

Different aspects of interpolation for propositional logic have been studied
since its introduction in model checking in~\cite{McMillan03}.
%
One of the most important recent results is \cite{DSilva2010b}, where LIS
(Labeled Interpolation Systems) is introduced. LIS is a framework capable of
generating multiple interpolants for a single proof of unsatisfiability.  This
is done by the application of different \emph{labeling functions} which can
also control the strength of the generated interpolants.  Two specific labeling
functions from LIS are also able to generalize the previous interpolation
algorithms from~\cite{Pudlak97} and~\cite{McMillan2005}. The authors also
present two new labeling functions: $D_{min}$, a labeling function that
generates interpolants with the smallest amount of distinct variables in the
framework, and a labeling function that behaves as the dual
of~\cite{McMillan2005}.
%
The Proof-Sensitive interpolation algorithms described in this chapter are built
on top of LIS.

A comparison between the interpolation algorithms from~\cite{Pudlak97},
~\cite{McMillan2005} and its dual when used by two different model checkers was
done in~\cite{Rollini2013}. The two applications used interpolants to compute
function summaries for incremental verification and upgrade checking. It was
shown that for one of the applications a logically stronger interpolation
algorithm led to less refinements, whereas for the other application this was
achieved by a logically weaker interpolation algorithm. This led to the conclusion
that the suitability of interpolants depends on the application.

Even though it is hard to say whether an interpolant is good or not based only
on its logical strength, there is a consensus that the size of the interpolant
might have a direct impact on the performance of the interpolation-based
application. There are different techniques that try to reduce the size of
interpolants.

Interpolants can be compacted through applying transformations to the
refutation proof. The work presented in~\cite{Rollini2012} applies
several proof compression techniques in order to reduce the size
of the interpolants generated using that proof. The approach works
well, as shown in~\cite{Rollini2013}, but compressing a proof
requires heavy computation, and this might be an impractical
overhead in many cases.

It is also possible to apply post-interpolation reduction.
In~\cite{CabodiLVB:DATE2013} circuit reduction techniques such as ODC,
BDD-based sweeping and Constant Propagation are used to reduce the size of
propositional interpolants.  Also this technique, even though often successful,
is computationally expensive and may create an overhead.

The Proof-Sensitive interpolation algorithms presented in this chapter are a
lightweight alternative to reduce the size of interpolants, since they require
little extra computation and are proved to generate small interpolants.

A significant reduction in the size of the interpolant can be obtained by
considering only CNF-shaped interpolants~\cite{Vizel2013}.  The main idea of
this technique is to first compute an approximation for the interpolant. Then,
as a second stage, inductive reasoning is applied to transform the interpolant
approximation into a sound interpolant.  However, the strength of these
interpolants is not as easily controllable as in the LIS interpolants, making
the technique harder to apply in certain model checking approaches.

%
%An interesting analysis similar to ours, presented
%in~\cite{BloemMSW:HVC2014}, concentrates on the effect of identifying
%subsumptions in the resolution proofs.
%
%
The extension of LIS presented in~\cite{JancikKRS:FMCAD2014} enables new
optimization methods for interpolation-based model checkers. By specifying a
truth assignment, the application can make a single SAT query and generate
interpolants for different interpolation problems for the same proof, reducing
the number of SAT queries and increasing its performance.
This work has an orthogonal goal compared to the Proof-Sensitive interpolation
algorithms, and can be used together.

In~\cite{Ruemmer2013} a semantic framework to explore different interpolants is
presented, with the goal of finding suitable interpolants that ensure fast
convergence for model checkers. The semantic framework is based on the notion
of interpolation abstraction. The authors show that by using interpolation
abstraction with interpolant templates they were able to increase convergence
of interpolation-based applications.
Even though the goal of most work related to interpolation is to find more
suitable interpolants, this technique is orthogonal to the Proof-Sensitive
interpolation algorithms in the sense that they use different methods to
achieve that goal, which leads to different results.

A related approach for generalizing interpolants in unbounded model-checking
through abstraction is presented in~\cite{Cabodi2006} using incremental SAT
solving.  While this direction is orthogonal to ours, we believe that the ideas
presented here and addressing the interpolation back-end would be useful in
connection with the generalization phase.

%Linear-sized interpolants can be derived also from resolution
%refutations computed by SMT solvers, for instance in the combined theory
%of linear inequalities and equalities with uninterpreted
%functions~\cite{McMillan04} and linear rational
%arithmetic~\cite{Aws2013}.
%
%These approaches have an interesting connection to ours since they also
%contain a propositional part.
%
%It is also possible to produce interpolants without a
%proof~\cite{Chockler2013}.  However, this method gives no control over
%the relative interpolant strength and reduces in the worst case to
%enumerating all models of a SAT instance.
%
%Finally, conceptually similar to our work, there is a renewed interest
%in interpolation techniques used in connection with modern ways of
%organizing the high-level model-checking
%algorithm~\cite{McMillan2014,Cabodi2014}.


