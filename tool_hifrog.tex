\section{HiFrog}
\label{section:hifrog}
The novel first order interpolation techniques presented in this thesis were
integrated in a new model checker, \hifrog.
%
\hifrog{} is an SMT-based bounded model checker for C that uses interpolants to
create function summaries and reuse them in the incremental checking way
presented in Chapter~\ref{subsec:fsum}. Besides, it supports different levels
of abstraction by using an SMT-solver and the theories EUF, LRA, and
propositional logic.
%
This section describes the architecture and implementation details of \hifrog{}.

The basic verification flow applied by \hifrog{} is the following: the C program
to be verified is given, along with previously computed or user defined
function summaries. The assertions in the C program are processed
incrementally, using the applicable function summaries. If the result of the
verification of an assertion is SAT, refinement is done. Otherwise, the
interpolator SMT solver provides interpolants for the UNSAT query, which then
are stored as function summaries.

Fig.~\ref{figure:hifrog_arch} gives an overview of the many components of
\hifrog{}. The parser consists of the goto-cc symbolic compiler. It transforms
the C code into a \emph{goto-program} such that all loops and recursive calls
are unwound up to a certain bound. The SMT encoder is called for each
assertion, and starts by creating an SSA version of the unwound program. This
guarantees that each function call has its own SSA representation. At this
point, the SMT encoder applies the chosen logic (propositional, EUF or LRA) and
a theory-specific formula is generated, along with the applicable function
summaries in that theory.  Function summaries can be either previously
generated or user defined.  The formula is then given to the SMT solver, which
determines if that assertion is safe or not.
%
If the SMT solver returns UNSAT, we have that the assertion is true, and
function summaries are extracted. Otherwise, refinement is needed. If function
summaries were used, it may be the case that they introduced spurious behaviors
into the formula. In this case, the summaries involved in this assertion are
replaced by the precise encoding of the functions they summarize, and the SMT
solver is called again.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{figures/hifrog_arch}
\caption{\hifrog's architecture overview.}
\label{figure:hifrog_arch}
\end{figure}

The different theories supported by \hifrog{} allow performance improvement and
scalability, since encoding arithmetic operations, for instance, using
bit-precise encoding can be expensive. Replacing bit-precision encoding with
linear arithmetic or uninterpreted functions can significantly reduce
verification time and still be effective, since an UNSAT result for the
theory-abstracted formula also holds for the original problem. If the
theory-abstracted formula leads to a SAT result, it may be because of spurious
behaviors introduced by the abstraction, in which case the theory has to be
refined to a more precise one.
