\section{Function Summarization for Bounded Model Checking}
\label{subsec:fsum}

In~\cite{SFS_HVC11,SFS_FMCAD12}, a set of techniques to make BMC incremental by
allowing the handling different assertions and different versions of a software
source code was introduced.  The key concept behind this approach is
\emph{function summarization}, a technique to create and use over-approximation
of the function behavior via Craig Interpolation.
%
Assume that a safety property has been shown to hold for a BMC unwinding
of a given program by showing the corresponding formula unsatisfiable.
The constructed interpolants are then
stored in a persistent storage and are available for the use 
by the model checker on demand.

This technique handles the {\em unwound static single assignment}
(USSA) approximation of the program, where loops and recursive function
calls are unwound up to a fixed limit, and each variable is only
assigned at most once.
%
The benefit of the USSA-approximation is that it allows encoding into a
suitable first order theory, as well as propositional logic,
and can be passed to an SMT solver supporting the theory, or a SAT solver.
The constructed function summaries can be used in two applications:
%
\framebox{A1} incremental substitution while checking the same program
with respect to different assertions; and
%
\framebox{A2} upgrade validation by localized checking of summaries with
respect to the old assertions.

The approach of \framebox{A1} considers given assertions one at a time.  It
computes function summaries with respect to the first assertion (if possible)
and attempts to substitute summaries for the function while checking
consecutive assertions.  If a check fails after such substitution (i.e. the
solver returns SAT), \emph{refinement} is needed: the summaries are discarded
and the check is repeated with concrete function bodies.  The approach of
\framebox{A2}, in contrast, considers a new version of the program, previously
proven safe.  The idea is to check whether the updated code is still
over-approximated by the old summaries, and if needed, also to perform
\emph{refinement}.

%Both applications \framebox{A1} and \framebox{A2} are implemented in the tools
%\funfrog~\cite{SFS_ATVA12} and \evolcheck~\cite{FSS_TACAS13} respectively.  In
%the low level, both applications use \opensmt~\cite{BPST10}, a small,
%efficient, competition-winning SMT solver has been developing at USI since
%2008.  \opensmt{} includes a highly efficient SAT solver implementation based
%on a recent version of \system{MiniSAT}~\cite{SorenssonEen:SAT2003}.
%, developed at USI since 2008, that supports a wide range of theory specific
%reasoning. 

%Propositional function summarization can be a highly efficient tool in
%BMC, but can also result in bad performance.  This might result from the
%complexity of the proofs that need to be done on the bit-level and might
%involve complex arithmetic such as multiplication.
%
%In the proposed project we will study SMT encoding to combat the
%difficulties arising from solving and interpolating over the bit-blasted
%BMC formula.

The next paragraphs discuss the details of the model checking algorithm
that uses function summaries.  In particular, Alg.~\ref{Alg1} outlines
the method for constructing function summaries (non-empty if the
program is \emph{SAFE}) in SMT-based BMC.

%We refer the reader to the Sec.~\ref{subsec:fsum} to get all necessary background.

\paragraph{BMC formula construction.}
%
The USSA approximation is encoded into a BMC formula as follows:
\vspace{-0.5em}
\begin{gather*}
\texttt{CreateFormula}(\hat{f}) \triangleq \phi_{\hat{f}} \land
\bigwedge\limits_{\hat{g} \in \hat{F}: nested(\hat{f},
\hat{g})}{}\texttt{CreateFormula}(\hat{g})
\end{gather*}

For a function call $\hat{f} \in \hat{F}$, the formula is constructed
recursively, by conjoining the subformula $\phi_{\hat{f}}$ corresponding to the
body of the function with a separate subformula for every nested function call.
%
The logical formula $\phi_{\hat{f}}$ is constructed from the USSA form
of the body of the function $f$ using the theory $T$.  %The bodies of the
%nested calls are encoded into separate logical formulas (using a
%recursive call to \texttt{CreateFormula}).

\paragraph{Summarization.} If the BMC
formula is unsatisfiable, i.e., the program is safe, the algorithm proceeds
with interpolation.  The function summaries are constructed as interpolants
from a proof of unsatisfiability of the BMC formula.  In order to generate the
interpolant, for each function call $\hat{f}$ the BMC formula is split into two
parts.
%
First, $\phi^{subtree}_{\hat{f}}$ corresponds to the subformulas representing
the function call $\hat{f}$ and all the nested functions.  Second,
$\phi^{env}_{\hat{f}}$ corresponds to the context of the call $\hat{f}$, i.e.,
to the rest of the encoded program and  possible erroneous behaviors
$\emph{error}$.
%
\vspace{-0.5em}
\begin{align*}
\phi^{subtree}_{\hat{f}} \triangleq \bigwedge\limits_{\hat{g} \in \hat{F} : subtree(\hat{f}, \hat{g})}{} \phi_{\hat{g}} &&
\phi^{env}_{\hat{f}} \triangleq \emph{error} \land \bigwedge\limits_{\hat{h} \in \hat{F} : \lnot subtree(\hat{f}, \hat{h})}{} \phi_{\hat{h}}
\vspace{-0.5em}
\end{align*}
%
Therefore, for each function call $\hat{f}$, the \texttt{Interpolate}
method splits the BMC formula into
%
$A \equiv \phi^{subtree}_{\hat{f}}$ and $B \equiv \phi^{env}_{\hat{f}}$
%
and generates an interpolant $I_{\hat{f}}$ for $(A, B)$.
%
We refer to $I_{\hat{f}}$ as a \emph{summary} of function call
$\hat{f}$.
%Such interpolant $I_{\hat{f}}$ is an interpolant summary formula for the function $f$ (in the following we will simply call it summary).

%
%The generated interpolants are associated with the function calls by a mapping%
%\footnote{%
%Here, we consider only a single summary per a function call for the sake of simplicity.
%This still means multiple summaries per a single function called multiple times.
%Our prototype implementation does not have this restriction.}
%$\sigma: \hat{F} \rightarrow \mathbb{S}$, i.e.,  $\sigma(\hat{f}) = I_{\hat{f}}$.

\begin{algorithm}
\caption{Function summarization in SMT-based BMC}
\label{Alg1}
\begin{algorithmic}[1]
\Statex
\INPUT
{USSA $P_{\nu}$ with function calls $\hat{F}$ and main function $f_{main}$}
\OUTPUT
{Verification result: \{\emph{SAFE, UNSAFE}\}, summaries $ \{I_{\hat{f}} \} $ }
\DATA
{BMC formula $\phi$}
\State{$\phi \leftarrow \texttt{CreateFormula}(\hat{f}_{main})$}
\State{$result, proof \leftarrow$ \texttt{Solve}($\phi$) \Comment{run SAT/SMT-solver}}
\If{$result = \textsc{SAT}$}
\Return \emph{UNSAFE}, $\varnothing$
\EndIf
\For{each $\hat{f} \in \hat{F}$} \Comment{extract summaries}
\State{$I_{\hat{f}} \leftarrow$ \texttt{Interpolate}($\hat{f}$)}
\EndFor
\Return \emph{SAFE}, $ \{I_{\hat{f}} \} $
\end{algorithmic}
\end{algorithm}

Various verification techniques implement the idea of function summarization
in different forms~\cite{McMillan2006,McMillan2010,Albarghouthi2012}.
%
This thesis uses three tools that implement Craig interpolation-based function
summaries as described above. The tools \funfrog, \evolcheck, and \hifrog are
used in model checking experiments with the interpolation techniques presented
in Chapters~\ref{chapter:interpolation_bool}, ~\ref{chapter:euf_interpolation}
and~\ref{chapter:lra_interpolation}. Depending on the specific technique using
interpolants as function summaries, the interpolation algorithms that are
enabled need to be restricted. For instance, the tool \evolcheck requires tree
interpolation, as provided in~\cite{Rollini2013}.

