set terminal epscairo enhanced font "12" 
set grid
set key left
set size ratio 1
#set timestamp
set output "algorithms_plot_time_evolcheck.eps"
#set title "Algorithms comparison"
set xlabel "# Benchmarks"
set ylabel "Verification time (seconds)"
set logscale y
set pointsize 0.5
set yrange [7:1500]
plot "data_evolcheck_up_sorted.csv" using 2 with linespoints lt 3 pt 4 title "P", \
"data_evolcheck_up_sorted.csv" using 4 with linespoints lt 8 pt 8 title "M_s", \
"data_evolcheck_up_sorted.csv" using 6 with linespoints lt 4 pt 3 title "PS_s"
quit

