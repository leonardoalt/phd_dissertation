set term pdf
pname = itpalg1."_vs_".itpalg2."_".param.".pdf"
set output pname
#set title pname 
set grid
set nokey
set logscale xy
set xlabel itpalg1
set ylabel itpalg2
set size ratio 1
set xrange [1000:5500000]
set yrange [1000:5500000]
set xtics rotate by -45
#set timestamp
plot x, fname u int(col1):int(col2) with points lt 3 pt 4
quit
