set terminal epscairo enhanced font "12" color
set grid
set key left
set size ratio 1
#set timestamp
set output "algorithms_plot_time_funfrog.eps"
#set title "Algorithms comparison"
set xlabel "# Benchmarks"
set ylabel "Verification time (seconds)"
set logscale y
set pointsize 0.5
plot \
"data_funfrog_allclaims_sorted.csv" using 8 with linespoints lt 5 pt 0 title "M_w", \
"data_funfrog_allclaims_sorted.csv" using 2 with linespoints lt 3 pt 4 title "P", \
"data_funfrog_allclaims_sorted.csv" using 5 with linespoints lt 1 pt 8 title "M_s", \
"data_funfrog_allclaims_sorted.csv" using 11 with linespoints lt 2 pt 2 title "PS", \
"data_funfrog_allclaims_sorted.csv" using 14 with linespoints lt 8 pt 3 title "PS_w", \
"data_funfrog_allclaims_sorted.csv" using 17 with linespoints lt 4 pt 6 title "PS_s"
quit

