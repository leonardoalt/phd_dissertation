#!/bin/bash

gnuplot -e "fname='$1'; col1='1'; col2='4'; itpalg1='strong'; itpalg2='weak'; param='bool'" scatter.gp
gnuplot -e "fname='$1'; col1='1'; col2='7'; itpalg1='strong'; itpalg2='random'; param='bool'" scatter.gp
gnuplot -e "fname='$1'; col1='4'; col2='7'; itpalg1='weak'; itpalg2='random'; param='bool'" scatter.gp

gnuplot -e "fname='$1'; col1='2'; col2='5'; itpalg1='strong'; itpalg2='weak'; param='eq'" scatter.gp
gnuplot -e "fname='$1'; col1='2'; col2='8'; itpalg1='strong'; itpalg2='random'; param='eq'" scatter.gp
gnuplot -e "fname='$1'; col1='5'; col2='8'; itpalg1='weak'; itpalg2='random'; param='eq'" scatter.gp

gnuplot -e "fname='$1'; col1='3'; col2='6'; itpalg1='strong'; itpalg2='weak'; param='uf'" scatter.gp
gnuplot -e "fname='$1'; col1='3'; col2='9'; itpalg1='strong'; itpalg2='random'; param='uf'" scatter.gp
gnuplot -e "fname='$1'; col1='6'; col2='9'; itpalg1='weak'; itpalg2='random'; param='uf'" scatter.gp


