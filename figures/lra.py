from fillplots import boundary, Plotter, plot_regions
import matplotlib.pyplot as plt

zero = boundary(0)
one = boundary(1)
two = boundary(2)
three = boundary(3)
four = boundary(4)
five = boundary(5)

r_a = [(lambda x: x*0 + 1, True), (one, True)]
r_b1 = [(lambda x: x*0 + 4,), (zero, ), (three, True)]
r_b2 = [(lambda x: x*0 + 0.5,), (three, )]
r_f = [(lambda x: x*0 + 2.5, True), (two, True)]
r_w = [(lambda x: x*0 + 4, True), (three, True)]

p = plot_regions([r_w, r_f, r_a, r_b1, r_b2], xlim=(-1, 4), ylim=(-1, 5))

p.regions[0].config.fill_args['facecolor'] = (0.262, 0.478, 0.266) #W
p.regions[1].config.fill_args['facecolor'] = (0.313, 0.725, 0.317) #F
p.regions[2].config.fill_args['facecolor'] = (0.490, 0.584, 0.847) #A

p.regions[3].config.fill_args['facecolor'] = (0.75, 0.2, 0.2) #B
p.regions[4].config.fill_args['facecolor'] = (0.75, 0.2, 0.2) #B

p.regions[1].config.fill_args['alpha'] = 0.5
p.regions[2].config.fill_args['alpha'] = 0.5

p.plot()

plt.show()
