set terminal epscairo enhanced font "12" color size 5, 3
set output "time_lra_factors.eps"
#set title ""
#set xlabel ""
#set ylabel ""

c1="grey"
c2="beige"
c3="brown"

set linetype 1 lc rgb c1
set linetype 4 lc rgb c1
set linetype 7 lc rgb c1
set linetype 10 lc rgb c1
set linetype 13 lc rgb c1
set linetype 16 lc rgb c1
set linetype 2 lc rgb c2
set linetype 5 lc rgb c2
set linetype 8 lc rgb c2
set linetype 11 lc rgb c2
set linetype 14 lc rgb c2
set linetype 17 lc rgb c2
set linetype 3 lc rgb c3
set linetype 6 lc rgb c3
set linetype 9 lc rgb c3
set linetype 12 lc rgb c3
set linetype 15 lc rgb c3
set linetype 18 lc rgb c3

set boxwidth 0.5
set style fill solid
set xtics border in scale 0,0 nomirror rotate by -45  autojustify
#set xtics border in scale 0,0 nomirror autojustify
set key inside right top vertical Right noreverse noenhanced nobox
#set yrange [ 0.00000 : 5.50000 ]
set yrange [ 1.00000 : 1.150000 ]
#set yrange [ 0.00000 : 3.00000 ]
set ytics (0, 1, 2, 3)

plot 'data_lra.dat' using 1:3:($1+1):xtic(2) with boxes linecolor variable notitle,'' u 1:4 notitle lc rgb "black", 1 notitle
