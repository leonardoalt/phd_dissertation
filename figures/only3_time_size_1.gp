#!/usr/bin/env gnuplot
set term epslatex standalone font 17 header "\\input{macros.tex}"
set output "data/only3_time_size_1.tex"
set size square
set key bottom right

#set grid
#set size ratio 1
#set timestamp
set xtics 100
set ytics
set xrange [50:615]
#set yrange [:550000]

#set title "Time vs ItpSize"
set xlabel "Time (seconds)"
set ylabel "Size (number of connectives)"

plot \
"data/data_funfrog.csv" using 2:3 with points lt 1 pt 1 ps 3 title "$\\Pud$", \
"data/data_funfrog.csv" using 5:6 with points lt 2 pt 2 ps 3 title "$\\McM$", \
"data/data_funfrog.csv" using 8:9 with points lt 3 pt 4 ps 3 title "$\\McP$"

quit
