\section{Experimental Evaluation}
\label{section:experiments_euf}
We implemented and integrated the system into the existing propositional
interpolation implementation in the \opensmt{} solver.
%
We report experiments in two different settings in the implementation:
(i) interpolating over unsatisfiable \qfuf{} benchmarks from \smtlib; and
(ii) running the approach integrated in \hifrog{}, an
interpolation-based incremental model checker for C.  The benchmarks and
the software are available at \url{verify.inf.usi.ch/euf-interpolation}.
Before describing the experiments we give a concise explanation on how
EUF and propositional interpolation are integrated.

\iffalse
\subsection{Integration of Propositional and EUF Interpolation. }
\label{Sec:Integration}
An SMT solver takes as input a propositional formula where some atoms
are interpreted as equalities or inequalities over a theory, in our
experiments equalities over uninterpreted functions.
%
If a satisfying truth assignment for the propositional structure is
found, a theory solver is queried to determine the consistency of its
equalities.  In case of inconsistency the theory solver adds a
reason-entailing clause to the propositional structure.  The process
ends when either a theory-consistent truth assignment is found or the
propositional structure becomes unsatisfiable.
%
The SMT framework provides a natural integration for the theory and
propositional interpolants.  The clauses provided by the theory solver
are annotated with their theory interpolant and are used as partial
interpolants in the propositional interpolation system (see,
e.g.,~\cite{Alt2016}).
%
Similar to EUF, the propositional interpolation algorithms control the
strength of the resulting interpolant by choosing the partition for the
shared variables through labeling~\cite{Alt2016}.  The labeling has to
be followed then by the theory interpolation algorithm to preserve
interpolant soundness.
\fi

\subsection{Interpolation over \smtlib benchmarks}
%
The motivation for this experiment is to stress-test the implementation
with respect to generating interpolants of different strength and size.
The chosen benchmarks lead to complex congruence graphs,
and therefore the interpolation problems are non-trivial.
%
Following~\cite{Fuchs2009,Cimatti2008}, we randomly split the
assertions in each benchmark to partitions $A$ and $B$.
%required by interpolation.
%
The set consists of 106 \qfuf{} instances resulting
in total over two and a half million EUF interpolants.

\paragraph{Logical strength. }
The theory interpolation algorithms use three labeling functions $L_s,
L_w$ (see Chapter.~\ref{chapter:euf_interpolation}), and $L_r$, a
labeling function that labels all components randomly as either $s$ or
$w$.  The algorithms are called, respectively, $\Itp_s, \Itp_w$, and
$\Itp_r$.
%
We use the proof-sensitive interpolation algorithm~\cite{Alt2016} in the
propositional structure.
%
This results in three final interpolants $I_s$, $I_w$ and $I_r$ for each
benchmark.

We computed the strength relationship for each theory partial
interpolant as well as the final SMT interpolants.
%
Even though the EUF interpolants are often simple, in 71\% of them it
was possible to generate at least two interpolants of different
strength, and 5.73\% resulted in all three having different strength.
%
%The theory partial interpolants are then used by the propositional interpolation
%in order to compute a final interpolant.

After solving and interpolating, we ran extra experiments to check the
strength relations of the final interpolants $I_s$, $I_w$ and $I_r$.
%
Since the final interpolants are much more complex, of the 106
benchmarks, 55 ran out of memory while computing the strength relations.
For the remaining 51, all the three final interpolants were pairwise
inequivalent, confirming that the framework is able to generate
interpolants of different strength.

%\begin{table}[]
%    \centering
%    \caption{Average percentage of partial interpolants that led to different strength}
%    \label{table:strength}
%    \begin{tabular}{|c|c|c|c|}
%        \hline
%        \textbf{\# Itps} & 1     & 2  & 3    \\ \hline
%        \textbf{Avg \%}  & 23.29 & 71 & 5.73 \\ \hline
%    \end{tabular}
%\end{table}

\paragraph{Interpolant size. }
Since the propositional and EUF interpolation algorithms are to a large
degree independent, it is natural to ask what combination of the
algorithms is most efficient.  This experiment studies the question
using the interpolant size as a measure of efficiency.
%
We use several propositional interpolation algorithms from the
literature to study the combinations, all instances of the labeled
interpolation system~\cite{DSilva2010,Alt2016} supported by \opensmt{}.
Fig.~\ref{Fig:HierarchyLabeling} shows the algorithms ordered with
respect to the logical strength of the interpolants they compute, which
resulted from the theoretical analysis presented in 
Chapter~\ref{chapter:interpolation_bool}.  The algorithms $\McM, \Pud,$ and
$\McP$ use a fixed labeling for the shared variables whereas the algorithms
$\PS, \PS_s$, and $\PS_w$ use the proof structure to optimize the labeling.
\begin{figure}[tb]
\begin{center}
\includegraphics[width=.6\textwidth]{figures/hierarchy-labeling}
\end{center}
\caption{The relative strength of the propositional interpolation
algorithms~\cite{Alt2016}}
\label{Fig:HierarchyLabeling}
\end{figure}
%
The six propositional and three EUF interpolation algorithms result in
18 combinations.  We measure the sizes of the final interpolants both in
\emph{(i)} the number of Boolean connectives (Fig.~\ref{Fig:Bool});
and \emph{(ii)} the number of EUF equalities (Fig.~\ref{Fig:Eq}).
%
Excluding the instances where we encountered memory outs we report the
results on 82 of the original 106 benchmarks.  For each benchmark, we
computed the smallest number of Boolean connectives or equalities in the
interpolant among all the configurations (\emph{best}) and the ratio
\emph{combination/best} for each possible combination, which shows us
how much worse each combination did compared to the best combination for
that benchmark.  Notice that the ratio of the best combination for a
benchmark is one and therefore no ratio can be less than one.  The bars
present the average and the crosses the median of those ratios among
all the benchmarks for each combination.

\begin{figure}[tb]
\includegraphics{figures/size_bool}
\caption{Comparison between interpolation combinations with respect to
the number of Boolean connectives in the final interpolant}
%\caption{The combination $M_w+\Itp_w$
%generates interpolants with number of Boolean connectives that is on average
%1.2 times larger than the smallest (bars), outperforming all the other configurations.
%Also, its median (cross) is the lowest possible (1), which means that this combination
%led to the interpolant with the smallest amount of Boolean connectives for
%at least half of the benchmarks.}
\label{Fig:Bool}
\end{figure}

In Fig.~\ref{Fig:Bool} the combination $\McP$ + $\Itp_w$ gives the
smallest number of Boolean connectives, and $\McM$ + $\Itp_s$ appears in
the second place.  The median of $\McP$ + $\Itp_w$ is 1, which means
that it was responsible for the smallest number of connectives in at
least half of the benchmarks, and its average of 1.2 shows that even
when this was not the case, the combination was still close to the
optimum.
%
On the losing side, we make two observations. The EUF interpolation
algorithm $\Itp_r$ leads to a larger number of Boolean connectives, and
the propositional interpolation algorithm $\Pud$ leads to larger
interpolants.

Interestingly the combinations $\PS+\Itp_s$ and $\PS_s+\Itp_s$
have low medians and average, which are good, but not the best.
%
This seemingly contradicts our earlier observation in~\cite{Alt2016} that
$\PS$ and $\PS_s$ consistently lead to small number of connectives in
the interpolant.
%
Based on the experiments the likely reason is the soundness restriction in
integration (see Sec.~\ref{Sec:Integration}). This restriction occurs when
using a propositional and a theory interpolation system that use labeling
functions to control strength. If the propositional interpolation algorithm
labels a certain term aiming at a specific strength, the theory interpolation
algorithm has to follow this label (see Sec.~\ref{Sec:Integration}).  If the
strength aimed by the propositional interpolation algorithm is the opposite of
the theory interpolation algorithm, redundant equalities are added by the
EUF-interpolation system.
%
We can see our hypothesis in the experiments, since the results get gradually
worse when the propositional and the EUF interpolation algorithms disagree more
on the labeling (strength), best being $\PS_s+\Itp_s$ and the worst
$\PS_w+\Itp_s$. 

%
\iffalse
When the propositional and the theory
interpolation algorithms agree on the strength of the interpolant, the
theory interpolation algorithm does not suffer from it, because it was
its choice anyway. The combinations $\McM$ + $\Itp_s$ and $\McP$ + $\Itp_w$
are the extreme cases of \emph{strength agreement}, and the combinations
$\PS_s$ + $\Itp_s$ and $\PS_w$ + $\Itp_w$ agree on a lesser extreme way. An
extreme strength agreement brings advantages to the size of the
interpolants, whereas the lack of agreement brings negative effects, as
we can see in the next experiment.
\fi

We can observe the same trend in Fig.~\ref{Fig:Eq} in the number of EUF
equalities.
A strong propositional interpolation algorithm ($\McM$, $\PS_s$) combined
with $\Itp_s$ leads to smaller interpolants compared to their combination with
$\Itp_w$; and a weak propositional interpolation algorithm ($\McP$, $\PS_w$)
combined with $\Itp_w$ leads to smaller interpolants compared to their
combination with $\Itp_s$.  Also notice that $\PS$, a propositional interpolation
algorithm that tends to balance the distribution of variables~\cite{Alt2016},
leads to very similar results when combined with $\Itp_s$ and $\Itp_w$.
%As
%expected, $\Itp_r$ leads to a large number of equalities regardless the
%propositional interpolation algorithm, since it leads to many redundant
%equalities.
%switching
%from $I_A$ to $\neg I_B'$ in Eq.~\ref{equation:lisI} and from $I_B$ to $\neg I_A'$
%in Eq.~\ref{equation:lisS}, and as shown in Theorem~\ref{theorem:size}, this causes
%an increase on the number of equalities in the final interpolant.

\begin{figure}[tb]
\includegraphics{figures/size_eq}
\caption{Comparison between interpolation combinations with respect to
the number of equalities in the final interpolant}
%\caption{The combinations $M_w+Itp_w$ and $PS_w+Itp_w$ generate interpolants
%    with number of equalities that is on average 1.2 times worse than the best (bars),
%    outperforming all the other configurations. Also, their median (cross) is the
%lowest (1), showing that they generate the interpolant with the smallest
%number of equalities for at least half of the benchmarks.}
\label{Fig:Eq}
\end{figure}

\subsection{Interpolation-Based Incremental Verification}
We integrated the EUF-interpolation system with the incremental model
checker \hifrog{} as part of \opensmt{}, and used it to verify a set of C
benchmarks from SV-COMP (\url{https://sv-comp.sosy-lab.org/}) and other
sources.

%\hifrog{} uses interpolants to construct function summaries.  These
%summaries are then stored and used instead of the precise encoding of a
%function to incrementally verify a sequence of safety properties.  If a
%summary is not suitable it is refined, resulting in a new summary.
%
We use both purely propositional logic and \qfuf{} to model the
programs.  Table~\ref{table:hifrog} shows the verification time for
\hifrog{} with propositional logic in the column {\em Bool Ver. Time};
and \qfuf{} in the columns marked {\em EUF Verification Time}.

Unlike the bit-precise propositional model, the \qfuf{} model provides
an over-approximation of the program behavior.  If \hifrog{} reports that
a safety property is true under \qfuf{} it is also true for the
propositional model.
%
However, if a property is reported false, it may indicate either a real
or a spurious counterexample introduced by the EUF abstraction.  In case
of false properties the model checker can for instance consult the
propositional encoding to get the correct result.
%
We report run times for three variations of the model checker.  Column
{\em EUF only} reports the time used only by the EUF check.  Column {\em
+Spurious Overhead} reports the time when \hifrog{} is allowed to query
the spuriousness of the counter-example from an oracle and only needs to
consult the propositional encoding if the answer is yes.  Column {\em
+Full Overhead} reports the time when \hifrog{} needs to resort to the
propositional encoding always in case of a failure to verify.
%
Notably the use of EUF as an abstraction technique usually speeds up the
solving even in the case of the full overhead.

We also report the effect of interpolation algorithm strength to the
number of required refinements for the four combinations $\McM+\Itp_s$,
$\McM+\Itp_w$, $\McP+\Itp_s$ and $\McP+\Itp_w$ in the last four columns.
%
We note that the number of summary refinements varies sometimes
considerably over the combinations, demonstrating the advantage of the
flexibility our framework provides for the EUF-interpolation.
%
Table~\ref{table:euf_time_itps} shows the comparison between combinations of
interpolation algorithms with respect to the overall verification time spent by
\hifrog. We can see that no specific combination is the overall winner, and
that the EUF-interpolation system is necessary to achieve general optimal
performance.

\begin{sidewaystable}
    \centering
    \caption{Verification results of a set of C benchmarks.}
    \label{table:hifrog}
    \resizebox{\columnwidth}{!}{%
    \begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|}
        \cline{2-13}
                                                  &
\multirow{2}{*}{\textbf{\#Asrt}} & \multicolumn{3}{c|}{\textbf{EUF
Results}}
& \multirow{2}{*}{\textbf{\begin{tabular}[c]{@{}c@{}}Bool\\ Ver.\\ Time
(s)\end{tabular}}} & \multicolumn{3}{c|}{\textbf{EUF Verification Time
(s)}}
& \multicolumn{4}{c|}{\textbf{Summary Refinements}}                                        \\ \cline{3-5} \cline{7-13} 
                                                  &
& \textbf{\begin{tabular}[c]{@{}c@{}}Correct
\end{tabular}} & \textbf{SAT} &
\textbf{\begin{tabular}[c]{@{}c@{}}Spurious\\ SAT\end{tabular}} &
& \textbf{\begin{tabular}[c]{@{}c@{}}EUF\\ Only\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}+Spurious\\ Overhead\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}+Full\\ Overhead\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}$\McM$+\\$\Itp_s$\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}$\McM$+\\$\Itp_w$\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}$\McP$+\\$\Itp_s$\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}$\McP$+\\$\Itp_w$\end{tabular}} \\ \hline
        \multicolumn{1}{|c|}{\textbf{floppy1}}   & 18 & 15 & 3 & 3 & 69.598  & 8.296  & 34.739  & 34.739  & 28672 & 27648 & 24320 & 32256 \\ \hline
        \multicolumn{1}{|c|}{\textbf{floppy2}}   & 21 & 18 & 3 & 3 & 192.079 & 46.65  & 122.544 & 122.544 & 37120 & 41216 & 37632 & 40704 \\ \hline
        \multicolumn{1}{|c|}{\textbf{kbfiltr1}}  & 10 & 10 & 0 & 0 & 4.123   & 1.304  & 1.304   & 1.304   & 4864  & 4864  & 4864  & 4864  \\ \hline
        \multicolumn{1}{|c|}{\textbf{diskperf1}} & 14 & 11 & 3 & 3 & 193.695 & 29.731 & 67.78   & 67.78   & 45568 & 44544 & 47104 & 48384 \\ \hline
        \multicolumn{1}{|c|}{\textbf{floppy3}}   & 19 & 16 & 4 & 3 & 76.212  & 9.583  & 36.421  & 43.737  & 28928 & 34304 & 26624 & 29952 \\ \hline
        \multicolumn{1}{|c|}{\textbf{kbfiltr2}}  & 13 & 13 & 0 & 0 & 10.23   & 3.107  & 3.107   & 3.107   & 4864  & 4864  & 4864  & 4864  \\ \hline
        \multicolumn{1}{|c|}{\textbf{floppy4}}   & 22 & 19 & 4 & 3 & 207.261 & 52.564 & 127.869 & 144.073 & 41472 & 43008 & 40704 & 43776 \\ \hline
        \multicolumn{1}{|c|}{\textbf{kbfiltr3}}  & 14 & 14 & 1 & 0 & 18.664  & 5.649  & 5.649   & 14.563  & 10240 & 10496 & 10240 & 10496 \\ \hline
        \multicolumn{1}{|c|}{\textbf{tcas\_asrt}} & 162                                    & 149                                                                      & 145          & 13                                                              & 86.032                                                                                                    & 16.753                                                      & 21.648                                                                    & 99.95                                                                 & 59648                & 60160                & 59648                & 60160                \\ \hline
        \multicolumn{1}{|c|}{\textbf{cafe}}       & 115                                    & 100                                                                      & 100          & 15                                                              & 19.164                                                                                                    & 4.273                                                       & 5.77                                                                      & 14.665                                                                & 6656                 & 6656                 & 6656                 & 6656                 \\ \hline
        \multicolumn{1}{|c|}{\textbf{s3}}         & 131                                    & 123                                                                      & 112          & 8                                                               & 1.499                                                                                                     & 1.722                                                       & 1.804                                                                     & 2.98                                                                  & 0                    & 0                    & 0                    & 0                    \\ \hline
        \multicolumn{1}{|c|}{\textbf{mem}}        & 149                                    & 146                                                                      & 52           & 3                                                               & 44.627                                                                                                    & 59.867                                                      & 59.94                                                                     & 78.466                                                                & 23808                & 25088                & 23808                & 25088                \\ \hline
        \multicolumn{1}{|c|}{\textbf{ddv}}        & 152                                    & 56                                                                       & 105          & 96                                                              & 260.283                                                                                                   & 11.609                                                      & 122.03                                                                    & 122.938                                                               & 7936                 & 7936                 & 7936                 & 7936                 \\ \hline
        \multicolumn{1}{|c|}{\textbf{token}}      & 54                                     & 54                                                                       & 20           & 0                                                               & 962.277                                                                                                   & 150.977                                                     & 150.977                                                                   & 998.568                                                               & 15616                & 13568                & 15616                & 13568                \\ \hline
        \multicolumn{1}{|c|}{\textbf{disk}}       & 79                                     & 62                                                                       & 72           & 17                                                              & 8194.963                                                                                                  & 241.268                                                     & 638.209                                                                   & 8151.212                                                              & 9472                 & 38912                & 9472                 & 38912                \\ \hline
    \end{tabular}%
}
\end{sidewaystable}

\begin{table}[]
\centering
\caption{Verification time of \hifrog using different combinations of interpolation algorithms.}
\label{table:euf_time_itps}
\begin{tabular}{c|c|c|c|c|}
\cline{2-5}
                                          & \textbf{\McM+\Itps} & \textbf{\McM+\Itpw} & \textbf{\McP+\Itps} & \textbf{\McP+\Itpw} \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{floppy1}}    & 37.314              & 36.255              & \textbf{34.739}     & 39.186              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{floppy2}}    & 138.579             & 144.93              & \textbf{122.544}    & 139.768             \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{kbfiltr1}}   & 1.489               & 1.469               & \textbf{1.304}      & 1.464               \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{diskperf1}}  & 68.17               & \textbf{58.597}     & 67.78               & 64.015              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{floppy3}}    & 43.783              & 47.744              & \textbf{43.737}     & 44.988              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{kbfiltr2}}   & \textbf{2.961}      & 3.082               & 3.107               & 3.001               \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{floppy4}}    & 152.531             & \textbf{142.452}    & 144.073             & 154.35              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{kbfiltr3}}   & \textbf{14.626}     & 15.379              & 14.563              & 15.2                \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{tcas\_asrt}} & 100.467             & 99.95               & \textbf{99.869}     & 100.473             \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{cafe}}       & 14.608              & 14.665              & \textbf{14.582}     & 14.718              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{s3}}         & 2.98                & 2.98                & \textbf{2.859}      & 2.903               \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{mem}}        & 78.728              & \textbf{78.466}     & 79.325              & 78.727              \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{ddv}}        & \textbf{122.487}    & 122.938             & 122.884             & 123.005             \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{token}}      & 999.015             & 998.568             & 1000.373            & \textbf{998.166}    \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{disk}}       & \textbf{8147.576}   & 8151.212            & 8150.354            & 8156.35             \\ \hline
                                          \multicolumn{1}{|c|}{\textbf{TOTAL}}      & 9925.314            & 9918.687            & \textbf{9902.093}   & 9936.314            \\ \hline
                                          \end{tabular}
                                          \end{table}

