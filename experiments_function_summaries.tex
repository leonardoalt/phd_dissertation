\subsection{Interpolants as Function Summaries}
\label{section:experiments_function_summaries}
\funfrog and \evolcheck are SAT-based model checkers for C implement the
approach presented in Chapter~\ref{subsec:fsum}, using interpolants to
represent summaries of program functions. \funfrog is based on the incremental
checking approach, whereas \evolcheck relies on the upgrade checking technique.

\iffalse
\subsubsection{Incremental Verification with Function Summarization}
\label{Sec:FunFrog}
\funfrog is a SAT-based bounded-model-checker for C designed to
incrementally check different assertions. The checker works by
unwinding a program up to some predefined bound and encoding the unwound
program together with the negation of each assertion 
to a \emph{BMC} formula which is then passed to a SAT solver.
%
If the result is unsatisfiable, \funfrog reports that the
program is safe with respect to the provided assertion.  Otherwise, it
returns a counter-example produced from the model of the BMC formula.

Craig interpolation is applied in \funfrog to extract
\emph{function summaries} (relations over input and output parameters of
a function that over-approximate its behavior) to be reused
between checks of different assertions with the goal of improving
overall verification efficiency.
%A summary is constructed from the
%proof of unsatisfiability of the BMC formula, treating different parts
%of the formula as $A$ and $B$ with respect to the function call which
%it is produced for.
%Next we provide some details on the summary extraction
%and its use to facilitate the understanding of the experimental data.
Given a program $P$, and an assertion $\pi$, let $\phi_{P,\pi}$
denote the corresponding BMC formula.  If $\phi_{P,\pi}$ is
unsatisfiable, \funfrog uses Craig Interpolation to extract function
summaries.  This is an iterative procedure for each function call $f$
in $P$.  Given $f$, the formula $\phi_{P,\pi}$ is partitioned as
$\phi_{P,\pi} \equiv A_f\wedge B_\pi$, where $A_f$ encodes $f$ and its
nested calls, $B_\pi$ the rest of the program and the negated assertion
$\pi$. \funfrog then calls \opensmt to compute
an interpolant $I^{f,\pi}$ for the
function $f$ and assertion $\pi$.

%Notably, $I_f$ is a sound over-approximation of $f$ in the sense that $I_{f,\pi} \wedge
%B_\pi \rightarrow \bot$.

While checking the program with respect to another assertion $\pi'$,
\funfrog constructs the new BMC formula $\phi_{P,\pi'}$, 
$\equiv I^{f,\pi} \wedge B_{\pi'}$; where $I^{f,\pi}$ is used to 
over-approximate $f$.   If $\phi_{P,\pi'}$ is unsatisfiable then the
over-approximation was accurate enough to prove that $\pi'$ holds in
$P$.  On the other hand, satisfiability of $\phi_{P,\pi'}$ could be
caused by an overly weak over-approximation of $I^{f,\pi}$.  To check
this hypothesis, $\phi_{P,\pi'}$ is refined to $\phi^{\refnd}_{P,\pi'}$,
in which $I^{f,\pi}$ is replaced by the precise encoding of $f$
and the updated formula is solved again.  If
$\phi^{\refnd}_{P,\pi'}$ is satisfiable, the error is real.  Otherwise,
the unsatisfiable formula $\phi^{\refnd}_{P,\pi'}$ is used to create new
function summaries in a similar manner as described above.

In previous works~\cite{SFS_ATVA12,Rollini2013}
\funfrog chooses the interpolation algorithm from the set
$\{\McM, \Pud, \McP\}$ and uses it to create summaries for all function
calls in the program.
%
In this work, we have added the algorithms $\PS$, $\PS_{w}$ and $\PS_{s}$ to
the portfolio of the interpolation algorithms and show that in
particular the use of $\PS$ and $\PS_{s}$ improves quality of function
summaries in \funfrog and therefore makes overall model checking
procedure more efficient.
\fi

%For each function call $f$, the procedure of creating $I_{f,\pi'}$ is
%similar to the one to create $I_{f,\pi}$ from the formula
%$\phi^{ref}_{P,\pi'} \equiv A_f\wedge B_\pi'$.
\iffalse
Notably, in our
experimentation that used the flexible labeling mechanism, for each
function call $f$, and the corresponding partitioning of formula
$\phi^{\refnd}_{P,\pi'} \equiv A_f \wedge B_{\pi'}$, \funfrog 
chooses a new (most suitable) flexible interpolation algorithm  $\Itpalg{\PS}$.
\fi

\paragraph{Experiments with \funfrog.}
The set of benchmarks consists of 23 C programs with different number of
assertions. Each benchmark contains a complex function call tree with
assertions on different levels of the tree.
%
\funfrog verified the assertions one-by-one incrementally
traversing the program call tree. The main goal of ordering the checks
this way is to maximize the reuse of function summaries and thus to test
how the labeling functions affect the overall verification performance.
To illustrate our setting, consider a program with the chain of nested
function calls
%
\begin{center}
$\main()\{ f()\{ g()\{ h()\{\} \asrt_g\} \asrt_f\} \asrt_{\main}\}$,
\end{center}
%
where $\asrt_F$ represents an assertion in the body of function $F$. In a
successful scenario,
%
    (a) $\asrt_g$ is detected to hold and a summary $I^h$
    for function $h$ is created;
    (b) $\asrt_f$ is efficiently verified by exploiting
    $I^h$, and $I^g$ is then built over $I^h$; and
    (c) finally $\asrt_{\main}$ is checked against $I^g$.
%
In this set of benchmarks, generating good interpolants for the first
assertions can strongly impact the chain verification of the subsequent
assertions, given its complex characteristic.

Fig.~\ref{Fig:funfrog} shows \funfrog's
performance with each interpolation algorithm.  Each curve represents an
interpolation algorithm, and each point on the curve represents one
benchmark run using the corresponding interpolation algorithm, with its
verification time on the vertical axis.  The benchmarks are sorted by
their run time.  The $\Itpalg{\PS}$ and $\Itpalg{\PS_s}$ curves are
mostly lower than those of the other interpolation algorithms,
suggesting they perform better.
%
Table~\ref{Table:funfrog} shows the sum of
\funfrog verification time for all benchmarks and the average
size of all interpolants generated for all benchmarks for each
interpolation algorithm. We also report the relative time and size
increase in percents.  Both $\Itpalg{\PS}$ and $\Itpalg{\PS_s}$ are
indeed competitive for \funfrog, delivering interpolants
smaller than the other interpolation algorithms.
%
Table~\ref{table:funfrogDetailed} shows \funfrog verification time 
for each benchmark (row) and labeling function,
together with the average size of the interpolants used to prove that benchmark.
In each row, the smallest verification time and interpolant size are highlighted.
The labeling functions $\PS_s$ and $\PS$ have the most wins, 7 and 5 respectively.
Notice that in most of the cases the labeling function that had the smallest
size of interpolants had either the smallest verification time or a verification
time close to the smallest. The latter happened in several benchmarks for $\PS$.

\begin{figure}[]
\begin{centering}
\includegraphics[scale=1.0]{figures/algorithms_plot_time_funfrog}
\caption{Overall verification time of \funfrog using different interpolation algorithms.}
\label{Fig:funfrog}
\end{centering}
\end{figure}

\begin{table}[]
\centering
\caption{Sum of overall verification time and average interpolants size for \funfrog using the
applicable labeling functions.}
\label{Table:funfrog}
\begin{tabular}{lrrrrrrr}
\toprule
& \multicolumn{7}{c}{\funfrog} \\
& $\McM$ & $\Pud$ & $\McP$ & $\PS$ & $\PS_w$ & $\PS_s$ & \Dmin \\
\cmidrule{1-1} \cmidrule(l){2-8}
\textbf{Time (s)} & 2333& 3047 & 3207 & 2272 & 3345 & {\bf 2193} & 3811\\ 
\textbf{increase \%} & 6 & 39 & 46 & 3 & 52 &  0 & 74 \\
\cmidrule{1-1} \cmidrule(l){2-8}

\textbf{Avg size} & 48101& 79089& 86831 & 43781 & 95423 & {\bf 40172} & 119306 \\
\textbf{increase \%} & 20 & 97 & 116 & 9 & 137 & 0 & 197 \\
\bottomrule
\end{tabular}
\end{table}

\begin{sidewaystable}[]
    \centering
    \caption{Detailed information about the benchmarks ran with \funfrog.}
    \label{table:funfrogDetailed}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
        \cline{2-15}
        \textbf{}                 & \multicolumn{2}{c|}{\textbf{\McM}}   & \multicolumn{2}{c|}{\textbf{\Pud}}   & \multicolumn{2}{c|}{\textbf{\McP}}  & \multicolumn{2}{c|}{\textbf{\PS}}  & \multicolumn{2}{c|}{\textbf{\PSw}} & \multicolumn{2}{c|}{\textbf{\PSs}} & \multicolumn{2}{c|}{\textbf{\Dmin}} \\ \cline{2-15} 
                                  & \textbf{Time(s)} & \textbf{Size}  & \textbf{Time(s)} & \textbf{Size}  & \textbf{Time(s)} & \textbf{Size}  & \textbf{Time(s)} & \textbf{Size}  & \textbf{Time(s)}  & \textbf{Size} & \textbf{Time(s)} & \textbf{Size}  & \textbf{Time(s)}  & \textbf{Size}  \\ \hline
        \multicolumn{1}{|c|}{B1}  & 74.9             & 37971          & 139.3            & 63254          & 206.1            & 187719         & \textbf{70.8}    & \textbf{30707} & 133.1             & 62945.7       & 71.5             & 31016          & 102               & 53444          \\ \hline
        \multicolumn{1}{|c|}{B2}  & 138.5            & 122576         & 104.3            & 44989          & 104.8            & 40816          & 117              & 39132          & 562.1             & 548079        & \textbf{86.8}    & \textbf{35578} & 100.5             & 46755          \\ \hline
        \multicolumn{1}{|c|}{B3}  & 16.5             & 9907           & 19.5             & 9584           & 18.5             & 8546           & 15.8             & \textbf{6734}  & 20.8              & 15082         & \textbf{14.1}    & 7212           & 16.8              & 7204           \\ \hline
        \multicolumn{1}{|c|}{B4}  & 23.2             & 6624           & 23.3             & 6527           & 21.3             & 4493           & \textbf{19.4}    & \textbf{3232}  & 23.1              & 4837          & 20.6             & 4477           & 20.3              & 3858           \\ \hline
        \multicolumn{1}{|c|}{B5}  & 47.6             & 5212           & 56.3             & 9102           & 46               & 4797           & 45.8             & \textbf{4738}  & 45.6              & 4812          & 48.2             & 5093           & \textbf{44.7}     & 4815           \\ \hline
        \multicolumn{1}{|c|}{B6}  & 111.8            & 16533          & 193.4            & 91678          & 95.7             & 15749          & 95.4             & \textbf{14819} & \textbf{74.7}     & 14943         & 86.5             & 22315          & 89.1              & 15745          \\ \hline
        \multicolumn{1}{|c|}{B7}  & 223.1            & 144065         & 222.4            & 178879         & 245.4            & \textbf{92290} & 210.5            & 121826         & 258.7             & 95589         & \textbf{193.6}   & 93154          & 883.8             & 674142         \\ \hline
        \multicolumn{1}{|c|}{B8}  & 107.7            & 30219          & \textbf{104.5}   & \textbf{23108} & 199              & 113230         & 108.8            & 25691          & 108.8             & 26788         & 106.3            & 25811          & 107.4             & 26571          \\ \hline
        \multicolumn{1}{|c|}{B9}  & 121.1            & 62334          & \textbf{80.1}    & \textbf{27965} & 159.3            & 109700         & 94.6             & 42685.625      & 152               & 101364        & 97.7             & 28572          & 160.4             & 130782         \\ \hline
        \multicolumn{1}{|c|}{B10} & 141.2            & 69821          & 105.5            & 86536          & 166.4            & 56268          & \textbf{105.4}   & 82694.5        & 128.5             & 84897         & 126.5            & \textbf{56207} & 107.6             & 83719          \\ \hline
        \multicolumn{1}{|c|}{B11} & \textbf{78.7}    & \textbf{26970} & 82.7             & 30391          & 131              & 90565          & 91.3             & 43326          & 175.4             & 83770         & 107.6            & 46880          & 183.9             & 82335          \\ \hline
        \multicolumn{1}{|c|}{B12} & 169.1            & 86978          & 236.3            & 235021         & \textbf{166.5}   & 160913         & 172.5            & \textbf{76335} & 315.8             & 343754        & 211.6            & 160716         & 626.3             & 874305         \\ \hline
        \multicolumn{1}{|c|}{B13} & 97.9             & 56230          & 225              & 104427         & 386.8            & 282949         & 98.8             & \textbf{39545} & 115.5             & 43162         & \textbf{84.1}    & 40424          & 188.5             & 86785.625      \\ \hline
        \multicolumn{1}{|c|}{B14} & 52.1             & 16928          & 48.7             & 11805          & 53.7             & 12549          & 73.9             & 49699          & 261.5             & 312589        & \textbf{48.6}    & \textbf{11229} & 49.2              & 11851          \\ \hline
        \multicolumn{1}{|c|}{B15} & 58.2             & 34815          & 61               & 23060          & 59.2             & 29235          & \textbf{56.1}    & \textbf{21756} & 84.9              & 57362         & 59.4             & 29967          & 64.6              & 24567          \\ \hline
        \multicolumn{1}{|c|}{B16} & 74.9             & 23852          & 109              & 104525         & 71.9             & \textbf{19554} & 72               & 19632          & \textbf{71.5}     & 19585         & 74               & 20350          & 72.1              & 19657          \\ \hline
        \multicolumn{1}{|c|}{B17} & 100.5            & 54864          & 110              & 43687          & \textbf{92.6}    & 43923          & 93.4             & \textbf{37804} & 128.6             & 137225        & 111.5            & 63365          & 104.9             & 46823          \\ \hline
        \multicolumn{1}{|c|}{B18} & \textbf{145.7}   & 53491          & 151.8            & 49373          & 159.8            & 60094          & 158.3            & \textbf{47654} & 155.6             & 48620         & 148.6            & 48272          & 148.7             & 50842          \\ \hline
        \multicolumn{1}{|c|}{B19} & 136.6            & 91684          & 450.1            & 370456         & 227.7            & 288637         & 139.9            & \textbf{80770} & 145               & 82786         & \textbf{127.7}   & 81274          & 147.8             & 81937          \\ \hline
        \multicolumn{1}{|c|}{B20} & 33.5             & 7602           & 33.9             & 6806           & \textbf{29.8}    & 6079           & 29.9             & \textbf{6021}  & 32.9              & 7884          & 32.3             & 6330           & 36.5              & 11843          \\ \hline
        \multicolumn{1}{|c|}{B21} & 41.6             & 9696           & 43.5             & 10378          & 56.8             & 22996          & 59.6             & 23954          & 81.3              & 39746         & \textbf{40.1}    & \textbf{7921}  & 40.9              & 8342           \\ \hline
        \multicolumn{1}{|c|}{B22} & 80.9             & 13120          & 82.4             & 16125          & \textbf{67.9}    & \textbf{10799} & 73.6             & 14428          & 89.5              & 31998         & 84.1             & 17383          & 102.1             & 46530          \\ \hline
        \multicolumn{1}{|c|}{B23} & 116.7            & 41730          & 117.2            & 40410          & 118.6            & 39881          & \textbf{111.3}   & \textbf{39804} & 123.8             & 47511         & 136.             & 41431          & 118.5             & 41096          \\ \hline
    \end{tabular}
}
\end{sidewaystable}

\iffalse
\subsubsection{Upgrade Checking using Function Summarization}
\evolcheck is an Upgrade Checker for C, built on top of
\funfrog.  It takes as an input an original program $S$ and its
upgrade $T$ sharing the set of functions calls $\{f\}$.
%
\evolcheck uses the interpolation-based function summaries
$\{I^{S,f}\}$, constructed for $S$ as shown in Sect.~\ref{Sec:FunFrog}
to perform upgrade checking. In particular, it
verifies whether for each function call $f$ the summary $I^{S,f}$
over-approximates the precise behavior of $T$.
This local check is turned into showing unsatisfiability of $\neg
I^{S,f} \land A_{T,f}$, where $A_{T,f}$ encodes $f$ and its nested calls
in $T$.
%
If proven unsatisfiable, \evolcheck applies Craig Interpolation
to refine the function summary with respect to $T$.
\fi

\paragraph{Experiments with \evolcheck.}

The benchmarks consist of the ones used in the \funfrog 
experiments and their upgrades.
%
We only experiment with $\Itpalg{\McM}$, $\Itpalg{\Pud}$ and
$\Itpalg{\PS_s}$ since \evolcheck requires algorithms at least
as strong as $\Itpalg{\Pud}$.
%
Fig.~\ref{Fig:evolcheck} demonstrates that
$\Itpalg{\PS_s}$, represented by the lower curve, outperforms the other
algorithms also for this task.
%
Table~\ref{Table:evolcheck} shows the total time
\evolcheck requires to check the upgraded versions of all
benchmarks and average interpolant size for each of the three
interpolation algorithms.
%
Also for upgrade checking, the interpolation algorithm $\Itpalg{\PS_s}$
results in smaller interpolants and lower run times
compared to the other studied interpolation algorithms.
%
Table~\ref{table:evolcheckDetailed} shows detailed information for each 
benchmark.  Each row shows the verification
time for \evolcheck using each of the labeling functions, and the average
size of the interpolants used to prove that benchmark, with the best of each
highlighted. The performance of $\PS_s$ is even better in this experiment, being the 
winner in 16 out of 23 benchmarks. Even when $\PS_s$ does not lead to the best
verification time, it is close to the best.

\begin{figure}[]
\begin{centering}
\includegraphics[scale=1.0]{figures/algorithms_plot_time_evolcheck}
\caption{Overall verification time of \evolcheck using different interpolation algorithms.}
\label{Fig:evolcheck}
\end{centering}
\end{figure}

\begin{table}[]
\centering
\caption{Sum of overall verification time and average interpolants size for \evolcheck
    using the applicable labeling functions.}
\label{Table:evolcheck}
\begin{tabular}{lrrr}
\toprule
& \multicolumn{3}{c}{\evolcheck} \\
& $\McM$ & $\PS_s$ & \Pud \\
\cmidrule{1-1} \cmidrule(l){2-4}
\textbf{Time (s)} & 4867 & {\bf 4422} & 5081 \\
\textbf{increase \%} & 10 & 0 & 16 \\
\cmidrule{1-1} \cmidrule(l){2-4}

\textbf{Avg size} & 246883 & {\bf 196716} & 259078 \\
\textbf{increase \%} & 26 & 0 & 32 \\
\bottomrule
\end{tabular}
\end{table}


\begin{table}[]
    \centering
    \caption{Detailed information for the benchmarks run with \evolcheck}
    \label{table:evolcheckDetailed}
    \begin{tabular}{l|l|l|l|l|l|l|}
        \cline{2-7}
        \multicolumn{1}{c|}{\textbf{}}     & \multicolumn{2}{c|}{\textbf{M}}                                            & \multicolumn{2}{c|}{\textbf{P}}                                            & \multicolumn{2}{c|}{\textbf{PSs}}                                            \\ \cline{2-7} 
        \multicolumn{1}{c|}{\textbf{}}     & \multicolumn{1}{c|}{\textbf{Time(s)}} & \multicolumn{1}{c|}{\textbf{Size}} & \multicolumn{1}{c|}{\textbf{Time(s)}} & \multicolumn{1}{c|}{\textbf{Size}} & \multicolumn{1}{c|}{\textbf{Time(s)}} & \multicolumn{1}{c|}{\textbf{Size}}   \\ \hline
        \multicolumn{1}{|c|}{\textbf{B1}}  & \multicolumn{1}{c|}{117.3}            & \multicolumn{1}{c|}{181954}        & \multicolumn{1}{c|}{165.5}            & \multicolumn{1}{c|}{319241}        & \multicolumn{1}{c|}{\textbf{104.6}}   & \multicolumn{1}{c|}{\textbf{155759}} \\ \hline
        \multicolumn{1}{|c|}{\textbf{B2}}  & \multicolumn{1}{c|}{27.2}             & \multicolumn{1}{c|}{82183}         & \multicolumn{1}{c|}{20.8}             & \multicolumn{1}{c|}{41203}         & \multicolumn{1}{c|}{\textbf{20.4}}    & \multicolumn{1}{c|}{\textbf{34608}}  \\ \hline
        \multicolumn{1}{|c|}{\textbf{B3}}  & \multicolumn{1}{c|}{66.4}             & \multicolumn{1}{c|}{89123}         & \multicolumn{1}{c|}{64.2}             & \multicolumn{1}{c|}{85081}         & \multicolumn{1}{c|}{\textbf{61.9}}    & \multicolumn{1}{c|}{\textbf{64025}}  \\ \hline
        \multicolumn{1}{|c|}{\textbf{B4}}  & \multicolumn{1}{c|}{46.2}             & \multicolumn{1}{c|}{37877}         & \multicolumn{1}{c|}{58.8}             & \multicolumn{1}{c|}{66023}         & \multicolumn{1}{c|}{\textbf{41.8}}    & \multicolumn{1}{c|}{\textbf{22568}}  \\ \hline
        \multicolumn{1}{|c|}{\textbf{B5}}  & \multicolumn{1}{c|}{237.3}            & \multicolumn{1}{c|}{32822}         & \multicolumn{1}{c|}{246.8}            & \multicolumn{1}{c|}{48865}         & \multicolumn{1}{c|}{\textbf{236.6}}   & \multicolumn{1}{c|}{\textbf{32385}}  \\ \hline
        \multicolumn{1}{|c|}{\textbf{B6}}  & \multicolumn{1}{c|}{73.1}             & \multicolumn{1}{c|}{180422}        & \multicolumn{1}{c|}{73.4}             & \multicolumn{1}{c|}{155281}        & \multicolumn{1}{c|}{\textbf{70.9}}    & \multicolumn{1}{c|}{\textbf{141749}} \\ \hline
        \multicolumn{1}{|c|}{\textbf{B7}}  & \multicolumn{1}{c|}{6.6}              & \multicolumn{1}{c|}{817}           & \multicolumn{1}{c|}{\textbf{6.0}}     & \multicolumn{1}{c|}{\textbf{619}}  & \multicolumn{1}{c|}{6.3}              & \multicolumn{1}{c|}{745}             \\ \hline
        \multicolumn{1}{|c|}{\textbf{B8}}  & \multicolumn{1}{c|}{\textbf{364.8}}   & \multicolumn{1}{c|}{498117}        & \multicolumn{1}{c|}{446.2}            & \multicolumn{1}{c|}{682687}        & \multicolumn{1}{c|}{365.1}            & \multicolumn{1}{c|}{\textbf{454416}} \\ \hline
        \multicolumn{1}{|c|}{\textbf{B9}}  & \multicolumn{1}{c|}{139.5}            & \multicolumn{1}{c|}{136516}        & \multicolumn{1}{c|}{149.3}            & \multicolumn{1}{c|}{192747}        & \multicolumn{1}{c|}{\textbf{112.9}}   & \multicolumn{1}{c|}{\textbf{118989}} \\ \hline
        \multicolumn{1}{|c|}{\textbf{B10}} & \multicolumn{1}{c|}{193.2}            & \multicolumn{1}{c|}{169636}        & \multicolumn{1}{c|}{203.3}            & \multicolumn{1}{c|}{156927}        & \multicolumn{1}{c|}{\textbf{169.4}}   & \multicolumn{1}{c|}{\textbf{107000}} \\ \hline
        \multicolumn{1}{|l|}{\textbf{B11}} & \textbf{212.0}                        & 398903                             & 224.6                                 & 298583                             & 216.1                                 & \textbf{254480}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B12}} & \textbf{337.1}                        & 332379                             & 354.3                                 & \textbf{307082}                    & 347.6                                 & 326373                               \\ \hline
        \multicolumn{1}{|l|}{\textbf{B13}} & 354.5                                 & 353376                             & 444.7                                 & 525151                             & \textbf{345.5}                        & \textbf{300324}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B14}} & 316.3                                 & 683982                             & 368.2                                 & 842993                             & \textbf{278.8}                        & \textbf{589807}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B15}} & 133.3                                 & \textbf{157633}                    & \textbf{132.2}                        & 163467                             & 138.5                                 & 194351                               \\ \hline
        \multicolumn{1}{|l|}{\textbf{B16}} & 305.6                                 & 493254                             & 285.8                                 & 237692                             & \textbf{247.7}                        & \textbf{196568}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B17}} & \textbf{29.5}                         & \textbf{799}                       & 54.0                                  & 180849                             & 40.1                                  & 29723                                \\ \hline
        \multicolumn{1}{|l|}{\textbf{B18}} & 225.5                                 & 210548                             & \textbf{213.1}                        & \textbf{165406}                    & 215.2                                 & 167407                               \\ \hline
        \multicolumn{1}{|l|}{\textbf{B19}} & 20.1                                  & 70870                              & 16.3                                  & 39637                              & \textbf{15.0}                         & \textbf{32528}                       \\ \hline
        \multicolumn{1}{|l|}{\textbf{B20}} & 52.4                                  & \textbf{56261}                     & 55.8                                  & 66972                              & \textbf{51.0}                         & 65388                                \\ \hline
        \multicolumn{1}{|l|}{\textbf{B21}} & 1082.2                                & 1148482                            & 998.8                                 & 1099093                            & \textbf{871.8}                        & \textbf{846222}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B22}} & 198.2                                 & 287381                             & 175.7                                 & 212919                             & \textbf{158.5}                        & \textbf{194488}                      \\ \hline
        \multicolumn{1}{|l|}{\textbf{B23}} & 126.3                                 & 167410                             & 136.7                                 & 207696                             & \textbf{122.8}                        & \textbf{155301}                      \\ \hline
        %\multicolumn{1}{|l|}{\textbf{B24}} & \textbf{40.9}                         & \textbf{52180}                     & 44.2                                  & 95947                              & 42.2                                  & 95824                                \\ \hline
        %\multicolumn{1}{|l|}{\textbf{B25}} & 162.2                                 & 349152                             & 142.6                                 & \textbf{284800}                    & \textbf{141.8}                        & 336881                               \\ \hline
    \end{tabular}
\end{table}

