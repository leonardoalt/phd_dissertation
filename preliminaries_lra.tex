\section{LRA Preliminaries}
\label{preliminaries_lra}

This section introduces the quantifier-free theory of Linear Real Arithmetic and the most
common decision procedure for conjunctions of inequalities.  We
summarize~\cite{Kroening2008} and~\cite{Dutertre2006} with respect to the basics to understand the
theory of LRA and the algorithm \emph{general simplex}, used by most SMT solvers to
decide satisfiability of LRA formulas.

\begin{definition} [c.f.~\cite{Kroening2008}]
\label{definition:lra}
    The syntax of a formula in \emph{linear arithmetic} is defined
    by the following rules:
    \begin{align*}
        formula &:\ formula\ \wedge\ formula\ \vert\ (formula)\ \vert\ atom  \\
        atom &:\ sum\ op\ sum \\
        sum &:\ term\ \vert\ sum\ +\ term \\
        op &:\ =\ \vert\ <\ \vert\ \le \\
        term &:\ identifier\ \vert\ constant\ \vert\ constant\ identifier
    \end{align*}
\end{definition}

Even though the syntax may seem restrictive, some transformations can be done
to achieve the common use of arithmetic. For example, subtraction is not used
in Def.~\ref{definition:lra}, but $x_1 - x_2$ can be read as $x_1 + (- 1x_2)$. Also
the relation operators $>$ and $\ge$ can be replaced by $<$ and $\le$ by
negating the coefficients.
%
This definition of linear arithmetic is valid for multiple domains, but in this
thesis we are interested in the domain of rational numbers.

The \emph{simplex} method is a widely used algorithm that aims at maximizing an
objective function if a set of constraints is satisfiable.
%
Here we interested in the decision problem of whether a set of constraints is
satisfiable, rather than the maximization problem.
%
For that, we use the \emph{general simplex} algorithm. The general simplex
algorithm allows two types of constraints as input: (i) equalities of the
form
\begin{equation}
    a_1x_1 + a_2x_2 + \ldots + a_nx_n = 0,
\end{equation}
and (ii) bounds for the variables:
\begin{equation}
    l_i \le x_i \le u_i,
\end{equation}
where $l_i$ and $u_i$ are real constants.

This representation of an input formula is called the \emph{general form}.
Representing input in this way does not affect the power of the algorithm,
since it is possible to transform any linear constraint $L \bowtie R$ with
$\bowtie \in \{=, \le ,\ge\}$ into
the form described above in the following manner. Let $m$ be the number of
constraints. For each $i$-th constraint, where $1 \le i \le m$:\\
(i) Move all addends in $R$ to the left-hand side obtaining $L' \bowtie b$,
where $b$ is a constant.\\
(ii) Introduce a new variable $s_i$, and add the constraints $L' - s_i = 0$
and $s_i \bowtie b$. If $\bowtie$ is $=$, replace $s_i = b$ by $s_i \le b$ and $s_i \ge b$.

\begin{example}
    \label{example:constraints_sat_1}
    Consider the following conjunction of constraints:
    \begin{align*}
        x_1 + x_2 & \ge 3 \\
        x_1 - 2x_2 & \le 0
    \end{align*}

The constraints are rewritten into general form:
\begin{align*}
    x_1 + x_2 - s_1 & = 0 \\
    x_1 - 2x_2 - s_2 & = 0 \\
    s_1 & \ge 3 \\
    s_2 & \le 0
\end{align*}

\end{example}

The $m$ new variables $s_1$ and $s_2$ are called \emph{additional variables}.
The $n$ variables that occur in the original constraints are called
\emph{problem variables}.

It is common to represent the input constraints as a $m$-by-$(n+m)$ matrix $A$,
where each row represents a constraint, each column represents a variable, and
the cells are the coefficients. The $n$ problem variables and $m$ additional
variables are represented by the elements of a vector $x$ of length $n+m$.
Using this notation, the problem is equivalent to deciding whether there is a
vector $x$ such that
\begin{equation}
    Ax = 0 \text{ and } \bigwedge_{i=1}^m l_i \le s_i \le u_i,
\end{equation}
where $l_i$ and $u_i$ are constants that bound the additional variables $s_i$.

\begin{example}
    \label{example:constraints_sat_2}
    Using the variable ordering $x_1$, $x_2$, $s_1$, $s_2$, the matrix representation for 
    the constraints given in Example~\ref{example:constraints_sat_1} is
    \begin{equation}
    \begin{pmatrix}
        1 & 1 & -1 & 0 \\
        1 & -2 & 0 & -1
    \end{pmatrix}
\end{equation}
\end{example}
Notice that the part of the matrix corresponding to the additional variables is
an $m$-by-$m$ diagonal matrix, where the coefficients are -1. This is a direct
consequence of applying the general form transformations on the original
constraints. The matrix $A$ changes throughout the computation of the
algorithm, but the number of columns of this kind is always the same. The set
of $m$ variables corresponding to these columns are called \emph{basic
variables} and denoted by $\mathcal{B}$. The nonbasic variables are denoted by
$\mathcal{N}$.

It is also convenient to use a \emph{tableau} to manipulate matrix $A$. This
tableau is matrix $A$ without the diagonal matrix with -1
coefficients.  The tableau is an $m$-by-$n$ matrix, where the columns represent
the nonbasic variables, and each row now represents the basic variable that has
the -1 entry at that row in the diagonal sub-matrix, since this basic variable
depends on the nonbasic ones.


\begin{example}
    \label{example:constraints_sat_3}
    Continuing Example~\ref{example:constraints_sat_2}, the tableau and the bounds so far are:
\begin{minipage}{.5\textwidth}
    \centering
    \begin{tabular}{lll}
                & $x_1$ &  $x_2$ \\
          $s_1$ & 1   & 1  \\
          $s_2$ & 1   & -2
    \end{tabular}
\end{minipage}
\begin{minipage}{.5\textwidth}
    \centering
    \begin{align*}
        3 & \le s_1 \\
        s_2 & \le 0
    \end{align*}
\end{minipage}
\end{example}

The tableau is just a different way to represent matrix $A$, since $Ax = 0$ can be
rewritten into
\begin{equation}
    \label{equation:basic}
    \bigwedge_{x_i \in \mathcal{B}} \left( x_i = \sum_{x_j \in \mathcal{N}} a_{ij}x_j \right).
\end{equation}

The algorithm uses additionally an assignment $\alpha : \mathcal{B} \cup \mathcal{N} \rightarrow \mathbb{Q}$.
The initialization process of the algorithm works as follows: (i) the set of additional variables is
assigned to the set of basic variables $\mathcal{B}$; (ii) the set of problem variables is assigned
to the set of nonbasic variables $\mathcal{N}$; (iii) $\alpha(x_i) = 0$ and $\alpha(s_j) = 0$, where
$1 \le i \le n$ and $1 \le j \le m$.

\begin{algorithm}[tb]
\caption{General Simplex}
\label{algorithm:simplex}
\begin{algorithmic}[1]
\Procedure{GeneralSimplex}{Set of constraints $\vars{S}$}
\State{Transform the linear system into the general form.}
\State{$\mathcal{B} \leftarrow $ set of additional variables $s_1, \ldots, s_m$.}
\State{Construct the tableau.}
\State{Decide a fixed order for the variables.}
\While{True}
\If{every basic variable satisfies its bounds}
\Return{SAT}
\Else
\State{$x_i \leftarrow$ first basic variable in the order that
does not satisfy its bounds.}
\State{$x_j \leftarrow$ first suitable nonbasic variable in the order.}
\If{$x_j$ could not be set}
\Return{UNSAT}
\EndIf
\EndIf
\State{Apply the Pivot operation on $x_i$ and $x_j$.}
\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

Algorithm~\ref{algorithm:simplex} summarizes all the necessary steps for the decision.
The main loop of the algorithm checks if all the bounds are satisfied. If that is the
case, the problem is satisfiable and the algorithm terminates,
and $\alpha$ represents an assignment satisfying the constraints.

Suppose now that basic variable $x_i$ violates one if its bounds (according to $\alpha$),
and therefore its value needs to be adjusted. From Eq.~\ref{equation:basic}, we know that
\begin{equation}
    x_i = \sum_{x_j \in \mathcal{N}} a_{ij}x_j.
\end{equation}

Assume, without loss of generality, that $x_i$ violated its upper bound.
We can reduce the value of $x_i$ by decreasing the value of a nonbasic variable
$x_j$ such that $a_{ij} > 0$ and $\alpha(x_j) > l_j$, or by increasing the
value of a nonbasic variable $x_j$ such that $a_{ij} < 0$ and $\alpha(x_j) < u_j$.
A nonbasic variable that satisfies at least one of these conditions is called \emph{suitable}.
If no suitable variable exists at this point of the algorithm, the set of constraints
is unsatisfiable and the algorithm terminates.

After finding a suitable $x_j$, we have to adjust its value so that $x_i$
satisfies its bounds $l_i$ and $u_i$.  Let $\theta$ be this adjustment value:
\begin{equation}
    \theta = \frac{u_i - \alpha(x_i)}{a_{ij}}.
\end{equation}

By adjusting $x_j$, $x_i$ now satisfies its bounds, but it may be the case that
$x_j$ does not satisfy its bounds anymore. Therefore, we need to swap $x_i$ and
$x_j$ in the tableau. As a result, $x_i$ becomes nonbasic and $x_j$ becomes
basic. This transformation is called the \emph{pivot operation}.
%
Notice that an analogous process can be applied if $x_i$ violates its lower bound instead
of its upper bound (substituting $u_i$ by $l_i$).

\begin{definition} [c.f. \cite{Kroening2008}]
    Given two variables $x_i$ and $x_j$, the coefficient $a_{ij}$ is called the \emph{pivot element}.
    The column of $x_j$ is called the \emph{pivot column}. The row $i$ is called the \emph{pivot row}.
\end{definition}
The pivot element must be nonzero in order to apply the swapping.

The pivot operation is done by following the steps: (i) solve row $i$ for $x_j$; and
(ii) for all rows $l \neq i$, eliminate $x_j$ by using the equality for $x_j$ obtained from row $i$.

\begin{example}
    \label{example:constraints_sat_4}
We move on from Example~\ref{example:constraints_sat_3}. As part of the
initialization of the algorithm, we have that $\alpha(x_i) = 0$ and
$\alpha(s_i) = 0$ for every problem and additional variable.  Recall the
tableau and the bounds:

\begin{minipage}{.5\textwidth}
    \centering
    \begin{tabular}{lll}
            & $x_1$ &  $x_2$ \\
      $s_1$ & 1   & 1  \\
      $s_2$ & 1   & -2
    \end{tabular}
\end{minipage}
\begin{minipage}{.5\textwidth}
\begin{align*}
    3 & \le s_1 \\
    s_2 & \le 0
\end{align*}
\end{minipage}

The lower bound of $s_1$ is 3, which is violated since $\alpha(s_1) = 0$.
The next nonbasic variable in the ordering is $x_1$, which has a positive
coefficient and no upper bound, therefore it is suitable for pivoting.
%
Variable $s_1$ has to be increased by 3, which means that also $x_1$ has
to be increased by 3 ($\theta = 3$).
This changes the assignment such that
$\alpha = \{ x_1 \mapsto 3, x_2 \mapsto 0, s_1 \mapsto 3, s_2 \mapsto 3 \}$.
%
As described before, step (i) of pivoting is solving $s_1$'s row for $x_1$:
\begin{equation}
    s_1 = x_1 + x_2 \Leftrightarrow x_1 = s_1 - x_2.
\end{equation}
Now we can use this equality to replace $x_1$ in the other row:
\begin{equation}
    s_2 = (s_1 - x_2) - 2x_2 \Leftrightarrow s_2 = s_1 - 3x_2.
\end{equation}

The result of the pivot operation is:

\begin{minipage}{1.0\textwidth}
    \centering
    \begin{tabular}{lll}
            & $s_1$ &  $x_2$ \\
      $x_1$ & 1   & -1  \\
      $s_2$ & 1   & -3
    \end{tabular}
\end{minipage}


Now the upper bound of $s_2$ is violated. The next suitable
variable for pivoting is $x_2$. Variable $s_2$ needs to be
reduced by 3 to meet its requirements, which means that
$x_2$ needs to be increased by 1 ($\theta = 1$).
This changes the assignment such that
$\alpha = \{ x_1 \mapsto 2, x_2 \mapsto 1, s_1 \mapsto 3, s_2 \mapsto 0 \}$.

%
Solving row 2 for $x_2$:
\begin{equation}
    s_2 = s_1 -3x_2 \Leftrightarrow x_2 = \frac{s_1 - s_2}{3}.
\end{equation}

Substituting in row 1:
\begin{equation}
    x_1 = s_1 - \frac{s_1 - s_2}{3} \Leftrightarrow x_1 = \frac{2s_1 - s_2}{3}.
\end{equation}

The result of the pivot operation is:

\begin{minipage}{1.0\textwidth}
    \centering
    \begin{tabular}{lll}
            & $s_1$ &  $s_2$ \\
        $x_1$ & $\frac{2}{3}$  & $\frac{1}{3}$   \\
        $x_2$ & $\frac{1}{3}$  & $-\frac{1}{3}$
    \end{tabular}
\end{minipage}

Since all the basic variables satisfy their bounds, the problem is satisfiable
with $\alpha$ containing a satisfying assignment.
\end{example}

\begin{example}
\label{example:constraints_unsat}
We now modify the set of constraints from Example~\ref{example:constraints_sat_1}
such that it is unsatisfiable. We add the constraint $x_2 \le 0$.
    The new set of constraints is:
    \begin{align*}
        x_1 + x_2 & \ge 3 \\
        x_1 - 2x_2 & \le 0 \\
        x_2 & \le 0
    \end{align*}

The constraints are rewritten into the general form:
\begin{align*}
    x_1 + x_2 - s_1 & = 0 \\
    x_1 - 2x_2 - s_2 & = 0 \\
    x_2 & \le 0 \\
    s_1 & \ge 3 \\
    s_2 & \le 0
\end{align*}

In this run, the algorithm general simplex starts in the same way as in
Example~\ref{example:constraints_sat_4}, fixing the assignment for $s_1$.
Recall from Example~\ref{example:constraints_sat_4} that the assignment at this
point was $\alpha = \{ x_1 \mapsto 3, x_2 \mapsto 0, s_1 \mapsto 3, s_2 \mapsto
3 \}$ and the tableau was

\begin{minipage}{1.0\textwidth}
    \centering
    \begin{tabular}{lll}
            & $s_1$ &  $x_2$ \\
      $x_1$ & 1   & -1  \\
      $s_2$ & 1   & -3
    \end{tabular}
\end{minipage}

The upper bound of $s_2$ was violated. To fix the violation, we chose
$x_2$ since $s_1$ was not suitable. In the current run $x_2$ is also not
suitable because of its upper bound. Therefore the problem is unsatisfiable.

\end{example}

When the general simplex algorithm is used as the LRA solver in an SMT solver,
it needs to create an explanation for the unsatisfiability of the set of
constraints, which works as a proof of unsatisfiability.
%
Let $x_i$ be the variable that violated its bounds and could not be fixed,
$l_i$ its lower bound, and $u_i$ its upper bound.
%
Let $\mathcal{N}^+ = \{ x_j \in \mathcal{N} \ \vert\  a_{ij} > 0 \}$ and
$\mathcal{N}^- = \{ x_j \in \mathcal{N} \ \vert\  a_{ij} < 0 \}$.
%
Eq.~\ref{equation:exp_lower} and Eq.~\ref{equation:exp_upper} show how the
explanation $\Gamma$ is constructed, respectively, as $\Gamma^l$ for a lower bound violation
and as $\Gamma^u$ for an upper bound violation, according to~\cite{Dutertre2006}. 

\begin{equation}
    \label{equation:exp_lower}
    \Gamma^l = \{ x_j \le u_j \ \vert\  x_j \in \mathcal{N}^+ \} \cup \{ x_j \ge l_j \ \vert\  x_j \in \mathcal{N}^- \} \cup \{ x_i \ge l_i \}.
\end{equation}

\begin{equation}
    \label{equation:exp_upper}
    \Gamma^u = \{ x_j \ge l_j \ \vert\  x_j \in \mathcal{N}^+ \} \cup \{ x_j \le u_j \ \vert\  x_j \in \mathcal{N}^- \} \cup \{ x_i \le u_i \}.
\end{equation}

In the case of Example~\ref{example:constraints_unsat}, we have an
upper bound violation for variable $s_2$, so we use Eq.~\ref{equation:exp_upper}.
We have that $\mathcal{N}^+ = \{ s_1 \}$ and $\mathcal{N}^- = \{ x_2 \}$, so
$\Gamma = \{ s_1 \ge 3 \} \cup \{ x_2 \le 0 \} \cup \{ s_2 \le 0 \}$.
Since $s_1 = x_1 + x_2$ and $s_2 = x_1 - 2x_2$,
$\Gamma = \{ x_1 + x_2 \ge 3 \} \cup \{ x_2 \le 0 \} \cup \{ x_1 - 2x_2 \le 0 \}$.

\subsection{LRA Interpolation}
The explanation is a minimal subset of the original constraints, and can be
used to create a proof of unsatisfiability which leads to an interpolant, as
follows.

A simple approach to create LRA interpolants is presented in
\cite{McMillan2005}.  The idea is to create a tree that represents the proof of
unsatisfiability of the set of constraints and annotate the nodes with partial
interpolants, similar to what is done in propositional logic.
%
A \emph{term} in this system is a linear combination $c_0 + c_1v_1 + \ldots +
c_nv_n$, where $v_1 \ldots v_n$ are distinct variables and $c_0 \ldots c_n$ are
constants.  This proof system accepts constraints of the form $0 \le term$. 
%
Let $x$ be a term $c_0 + c_1v_1 + \ldots + c_nv_n$ and $c$ a constant. The notation $cx$ is equivalent to the term $c*c_0 + c*c_1v_1 + \ldots + c*c_nv_n$.
%
The rules of the proof system from \cite{McMillan2005} are presented in
Table~\ref{table:proof_system_mcmillan05}, where
$x$ and $y$ are terms:\\

\begin{table}[h]
\centering
\caption{LRA proof system from~\cite{McMillan2005}.}
\label{table:proof_system_mcmillan05}
\begin{tabular}{ccccc}
    Hyp & \quad & Comb \\
    \infer[\phi \in \Gamma]{\phi}{\quad&} & \quad & \infer{0 \le x + y}{0 \le x
& 0 \le y} \\
& & \\
    Taut & \quad & Mult \\
    \infer[c \text{ is a constant, }c \ge 0]{0 \le c}{\quad&} & \quad &
\infer[c \text{ is a constant, }c > 0]{0 \le cx}{0 \le c &
0 \le x}
\end{tabular}
\end{table}

\begin{example}
\label{example:proof_unsat_rules}
Using the rules above, we can create a proof tree for
Example~\ref{example:constraints_unsat}. First we have to transform our atoms
into the form accepted by the proof system:

\begin{align*}
    (1)\ 0 \le & -3 + x_1 + x_2 \\
    (2)\ 0 \le & -x_2 \\
    (3)\ 0 \le & -x_1 + 2x_2
\end{align*}

Applying the \emph{Hyp} rule to all the constraints is straightforward.
We can then apply rule \emph{Comb} to combine (1) and (3) and infer (4):
\begin{equation*}
    \infer{(4)\ 0 \le -3 + 3x_2}{0 \le -3 + x_1 + x_2 & 0 \le -x_1 + 2x_2}
\end{equation*}

We can apply rule \emph{Taut} to use the tautology (5) $0 \le 3$ and
then \emph{Mult} to multiply (2) by (3) to infer
(6) $0 \le -3x_2$. Now we can apply \emph{Comb} to combine (4) and (5)
to finally infer (7) $0 \le -3$, a contradiction.

The tree representing this proof is the following:

\begin{center}
\begin{forest}
    [(7) $0 \le -3$,grow=north
            [(6) $0 \le -3x_2$,grow=north
                [(2) $0 \le -x_2$]
                [(5) $0 \le 3$]
            ]
            [(4) $0 \le -3 + 3x_2$,grow=north
                [(3) $0 \le -x_1 + 2x_2$]
                [(1) $0 \le -3 + x_1 + x_2$]
            ]
        ]
    ]
]
\end{forest}
\end{center}

\end{example}

The main idea in the interpolation procedure presented in \cite{McMillan2005}
is to use the contribution from the constraints from $A$ to the sum that leads
to the contradiction, where the contribution from $A$ is effectively an
interpolant.  This contribution fulfills the requirements of an interpolant,
because (i) it is clearly implied by $A$; (ii) when summed with $B$ leads to
the contradiction; (iii) when all the constraints from $A$ are summed the local
symbols from $A$ are removed, therefore only common symbols from $A$ and $B$
remain.

The interpolation rules from \cite{McMillan2005} are given in
Table~\ref{table:interpolation_system_mcmillan05}, where $x$, $y$, $x'$ and
$y'$ are terms, and $[\phi]$ is the annotated term such that $0 \le \phi$ is
the partial interpolant for that node:

\begin{table}[h]
\centering
\caption{Interpolation system from~\cite{McMillan2005}.}
\label{table:interpolation_system_mcmillan05}
\begin{tabular}{cc}
    Hyp-A & Hyp-B \\
    \infer[(0 \le x) \in A]{0 \le x [x]}{\quad&} & \infer[(0 \le x) \in B]{0 \le x [0]}{\quad&} \\
                                              & \\
    Comb & Mult \\
    \infer{0 \le x + y [x' + y']}{0 \le x [x'] & 0 \le y [y']} & \infer{0 \le cx [cx']}{0 \le c & 0 \le x [x']}
\end{tabular}
\end{table}

\begin{example}
\label{example:interpolation_lra}
Suppose $A = \{ 0 \le -3 + x_1 + x_2, 0 \le -x_1 + 2x_2 \}$ and $B = \{ 0 \le -x_2 \}$.
We have shown in the previous examples that the conjunction of these sets of constraints
is unsatisfiable, and how the rules of the proof system from \cite{McMillan2005} are used
to create a proof of unsatisfiability. Now we use the extended interpolation rules to
generate an interpolant for $A$.

Using the $Hyp$-$A$ and $Hyp$-$B$ rules we can infer the following partial interpolants:
\begin{align*}
    \infer[(0 \le -3 + x_1 + x_2) \in A]{(1)\ 0 \le -3 + x_1 + x_2 [-3 + x_1 + x_2]}{\quad&}
\end{align*}

\begin{align*}
    \infer[(0 \le -x_1 + 2x_2) \in A]{(3)\ 0 \le -x_1 + 2x_2 [-x_1 + 2x_2]}{\quad&}
\end{align*}

\begin{align*}
    \infer[(0 \le -x_2) \in B]{(2)\ 0 \le -x_2 [0]}{\quad&}
\end{align*}

Following the proof from Example~\ref{example:proof_unsat_rules}, we apply the \emph{Comb}
rule on (3) and (1) to infer (4):
\begin{equation*}
    \infer{(4)\ 0 \le -3 + 3x_2 [-3 + 3x_2]}{0 \le -3 + x_1 + x_2 & 0 \le -x_1 + 2x_2}
\end{equation*}

We now apply the \emph{Taut} rule to use the tautology (5) $0 \le 3$ and
then \emph{Mult} rule on (5) and (2) to infer
(5) $0 \le -3x_2 [0]$. By applying \emph{Comb} on (4) and (5) we infer
(6) $0 \le -3 [-3 + 3x_2]$.

We have the following annotated proof of unsatisfiability:

\begin{center}
\begin{forest}
    for tree={
            align=center
        }
    [(7) $0 \le -3$\\${[}-3 + 3x_2{]}$,grow=north
            [(6) $0 \le -3x_2$\\${[}0{]}$,grow=north
                [(5) $0 \le 3$]
                [(2) $0 \le -x_2$\\${[}0{]}$]
            ]
            [(4) $0 \le -3 + 3x_2$\\${[}-3 + 3x_2{]}$,grow=north
                [(3) $0 \le -x_1 + 2x_2$\\${[}-x_1 + 2x_2{]}$]
                [(1) $0 \le -3 + x_1 + x_2$\\${[}-3 + x_1 + x_2{]}$]
            ]
        ]
\end{forest}
\end{center}

The final interpolant for $A$ is $0 \le -3 + 3x_2$. Notice that it contains
only symbols that are common between $A$ and $B$.  The proof of
unsatisfiability shows that it is the result of the sum of the constraints from
$A$, which means it is implied by $A$. The proof also shows that when conjoined
with $B$, the interpolant leads to a contradiction.

\end{example}

We use this interpolation system and the duality of interpolants to create a new
interpolation system for LRA, described in Chapter~\ref{chapter:lra_interpolation}.
